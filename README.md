# config
all variation config

ide-helpers.php - для подсказок IDE 

# Components
helpers for formater and handle TimeZoneDb and VinCode (check is Russia manufacturer)
```php
Formatter.php
TimeZoneDbComponent.php
VinCodeComponent.php
```

# Репозиторий с наработками по Yii2 

```php
CreateAction.php
DeleteAction.php
IndexAction.php
UpdateAction.php
```
use in Controller: 
```php
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'modelClass' => SystemEventSearch::class,
            ],
            'create' => [
                'class' => CreateAction::class,
                'modelClass' => SystemEvent::class,
            ],
            'update' => [
                'class' => UpdateAction::class,
                'modelClass' => SystemEvent::class,
            ]
        ];
    }
```
# GRID
helpers for DataColumn for GridView widgets
```php
NumberColumn.php
PercentColumn.php
```
# HELPERS
helpers for all type data
```php
PhoneHelper.php
```

# Validators
all helper for validation form data
```php
DateRangeValidator.php
PhoneNumberValidator.php
```
