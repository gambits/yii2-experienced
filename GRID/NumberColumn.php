<?php

namespace app\components\grid;

use Closure;
use yii\grid\DataColumn;
use yii\helpers\Html;

/**
 * @property int $totalValue
 */
class NumberColumn extends DataColumn
{
    public $format = 'integer';

    protected $_total = 0;
    protected $_idx = [];

    /**
     * @var array|Closure
     */
    public $footerOptions = [];

    public function getDataCellValue($model, $key, $index)
    {
        $value = parent::getDataCellValue($model, $key, $index);

        if (!array_key_exists($index, $this->_idx)) {
            $this->_total += (float)$value;
            $this->_idx[] = $index;
        }

        return $value;
    }

    public function renderDataCell($model, $key, $index)
    {
        if ($this->contentOptions instanceof Closure) {
            $options = call_user_func($this->contentOptions, $model, $key, $index, $this);
        } else {
            $options = $this->contentOptions;
        }

        $options['class'] =  isset($options['class']) ? $options['class'] . ' text-right' : 'text-right';

        return Html::tag('td', $this->renderDataCellContent($model, $key, $index), $options);
    }

    protected function renderFooterCellContent()
    {
        return $this->grid->formatter->format($this->totalValue, $this->format);
    }

    public function renderFooterCell()
    {
        if ($this->footerOptions instanceof Closure) {
            $options = call_user_func($this->footerOptions, $this);
        } else {
            $options = $this->footerOptions;
        }

        $options['class'] =  isset($options['class']) ? $options['class'] . ' text-right' : 'text-right';

        return Html::tag('td', $this->renderFooterCellContent(), $options);
    }

    public function getTotalValue()
    {
        return $this->_total;
    }
}
