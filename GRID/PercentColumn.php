<?php

namespace app\components\grid;

class PercentColumn extends NumberColumn
{
    public $format = ['decimal', 1];

    public function getTotalValue()
    {
        return $this->_idx ? $this->_total / count($this->_idx) : 0;
    }
}
