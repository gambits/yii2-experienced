<?php

namespace app\components\validators;

use yii\base\InvalidConfigException;
use yii\validators\Validator;

class DateRangeValidator extends Validator
{
    public $attributeFrom;
    public $message = 'Дата "с" должна быть больше даты "по".';

    public function init()
    {
        parent::init();
        if ($this->attributeFrom === null) {
            throw new InvalidConfigException('"attributeFrom" must be specified.');
        }
    }

    public function validateAttribute($model, $attribute)
    {
        $from = $to = null;

        if ($model->{$this->attributeFrom} && $model->$attribute) {
            $from = date_create($model->{$this->attributeFrom});
            $to = date_create($model->$attribute);
        }

        if ($from && $to && $from > $to) {
            $this->addError($model, $this->attributeFrom, $this->message);
            $this->addError($model, $attribute, $this->message);
        }
    }
}
