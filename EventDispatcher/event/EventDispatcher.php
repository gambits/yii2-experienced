<?php

namespace app\components\event;

use app\components\interfaces\EventDispatcherInterface;
use yii\base\Component;
use yii\base\Event;

/**
 * Class EventDispatcher
 */
class EventDispatcher extends Component implements EventDispatcherInterface
{
    /**
     * @param Event       $event
     * @param string|null $name
     *
     * @return void
     */
    public function dispatch(Event $event, $name = null): void
    {
        $this->trigger($name ?? get_class($event), $event);
    }
}
