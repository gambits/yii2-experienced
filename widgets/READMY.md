# Использование виджета OnClickFormField
- задача виджета подмена строкового значения на поле для редактирования, например,
```php
<span>Статус: Новый</span> // до клика

// после клика

<select>
    <option>Новый</option>
    <option>Исполнен</option>
    <option>В обработке</option>
</select>
```

# --------------------------------------------

```php

    <?= \app\widgets\OnClickFormField::widget([
        'id' => 'js-show-hide-field',
        'value' => $model->companyStatus->name,
        'fieldBeforeClick' => $formCorpClientInfo->field($model, 'company_status_id'),
        'fieldAfterClick' => $formCorpClientInfo->field($model, 'company_status_id')->dropDownList(\app\models\CompanyStatus::getList())->label(false),
        'idFieldBeforeClick' => 'js-company_status_id-1',
        'idFieldAfterClick' => 'js-company_status_id-2',
    ]) ?>


```