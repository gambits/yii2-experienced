<?php

namespace app\widgets;

use yii\bootstrap\Widget;
use yii\web\View;

/**
 * Class OnClickFormField
 */
class OnClickFormField extends Widget
{
    public $idFieldBeforeClick;
    public $idFieldAfterClick;
    /**
     * @var string
     */
    public $label,
        $value;
    /**
     * @var ActiveField
     */
    public $fieldBeforeClick;
    /**
     * @var ActiveField
     */
    public $fieldAfterClick;

    public function registerClientEvents()
    {
        return $this->view->registerJs(
            '$(function(){
                    $("#' . $this->idFieldBeforeClick . '").on("click", function(){
                        $(this).hide();
                        $("#' . $this->idFieldAfterClick . '").show();
                    });
                });',
            View::POS_END
        );
    }

    public function run()
    {
        $this->registerClientEvents();

        return $this->render('_field', [
            'fieldBeforeClick' => $this->fieldBeforeClick,
            'fieldAfterClick' => $this->fieldAfterClick,
            'label' => $this->label,
            'value' => $this->value,
            'idFieldBeforeClick' => $this->idFieldBeforeClick,
            'idFieldAfterClick' => $this->idFieldAfterClick,
        ]);
    }
}
