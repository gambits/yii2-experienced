<?php

namespace app\modules\customerLoyalty;

use app\helpers\apphelp;
use Yii;
use yii\filters\AccessControl;

/**
 * Class Module
 */
class Module extends \yii\base\Module
{
    public $navbar = [];
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\customerLoyalty\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->navbar = Yii::$app->getModule('admin')->navbar;
        Yii::$app->view->params['catMenu'] = apphelp::getCatList();
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
}
