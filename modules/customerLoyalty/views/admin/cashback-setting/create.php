<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\customerLoyalty\models\CashbackSetting */

$this->title = Yii::t('app', 'Создать');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Настройки кешбэка'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cashback-setting-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
