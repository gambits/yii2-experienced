<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\customerLoyalty\models\CashbackSetting */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cashback-setting-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'expired')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'max_amount')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
