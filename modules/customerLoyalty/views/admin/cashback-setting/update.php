<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\customerLoyalty\models\CashbackSetting */

$this->title = Yii::t('app', 'Обновить', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Настройки кешбэка'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="cashback-setting-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
