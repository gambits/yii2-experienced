<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\customerLoyalty\models\CashbackSettingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Настройки кешбэка');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cashback-setting-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Обновить/Создать'), ['update'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            [
                'attribute' => 'id',
                'filter'    => false,
            ],
            [
                'attribute' => 'expired',
                'filter'    => false,
            ],
            [
                'attribute' => 'max_amount',
                'label' => 'Максимально бонусов на заказ',
                'filter'    => false,
            ],
            [
                'attribute' => 'created_at',
                'filter'    => false,
            ],
            [
                'attribute' => 'updated_at',
                'filter'    => false,
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
