<?php

namespace app\modules\customerLoyalty\controllers;

use yii\web\Controller;

/**
 * Default controller for the `customerLoyalty` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     *
     * @return string
     */
    public function actionApplyCashbackToOrder($id)
    {

        return $this->render('index');
    }

}
