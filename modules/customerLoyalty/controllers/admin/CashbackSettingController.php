<?php

namespace app\modules\customerLoyalty\controllers\admin;

use app\modules\customerLoyalty\models\CashbackSetting;
use app\modules\customerLoyalty\models\CashbackSettingSearch;
use mdm\admin\controllers\BaseAdminController;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * CashbackSettingController implements the CRUD actions for CashbackSetting model.
 */
class CashbackSettingController extends BaseAdminController
{
    /**
     * @var CashbackSetting
     */
    private $model;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CashbackSetting models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CashbackSettingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Updates an existing CashbackSetting model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate()
    {
        $this->loadModel();

        if ($this->model->load(Yii::$app->request->post()) && $this->model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $this->model,
        ]);
    }

    /**
     * Finds the CashbackSetting model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return CashbackSetting the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function loadModel()
    {
        $this->model = CashbackSetting::findOne(1) ?? new CashbackSetting();

        return $this->model;
    }
}
