<?php

namespace app\modules\customerLoyalty\helpers;

use app\modules\customerLoyalty\models\Cashback;
use Yii;

/**
 * Class CustomerLoyaltyHelper
 */
class CustomerLoyaltyHelper
{
    public const ACTION_CASHBACK = 'cashback';

    /**
     * Strategy cashback
     *
     * @return array
     */
    public static function getAllowStrategy(): array
    {
        return [
            Cashback::STRATEGY_FIX     => Yii::t('app', 'Фиксированная сумма'),
            Cashback::STRATEGY_PERCENT => Yii::t('app', 'Процент'),
        ];
    }
}