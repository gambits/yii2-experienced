<?php


namespace app\modules\customerLoyalty\models\form;


use app\modules\customerLoyalty\helpers\CustomerLoyaltyHelper;
use app\modules\customerLoyalty\models\Cashback;
use Yii;
use yii\base\Model;

/**
 * Class CashbackForm
 */
class CashbackForm extends Model
{
    public const MODE_BY_PRODUCT = 'product';
    public const MODE_BY_CATEGORY = 'category';

    /**
     * @example 1,2,3,4
     * @var string
     */
    public $products;
    /**
     * @example 1,2,3,4
     * @var string
     */
    public $category;
    /**
     * @var int
     */
    public $strategy = Cashback::STRATEGY_FIX;
    /**
     * @var int|float
     */
    public $value;

    public function rules()
    {
        return [
            [['products'], 'required', 'on' => self::MODE_BY_PRODUCT],
            [['category'], 'required', 'on' => self::MODE_BY_CATEGORY],
            ['value', 'number'],
            ['value', 'required'],
            ['strategy', 'in', 'range' => array_keys(CustomerLoyaltyHelper::getAllowStrategy())],
        ];
    }

    public function attributeLabels()
    {
        return [
            'value'    => Yii::t('app', 'Значение начисляемого кешбека'),
            'strategy' => Yii::t('app', 'Механизм расчета кешбэка'),
            'products' => Yii::t('app', 'Товары'),
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios[self::MODE_BY_PRODUCT] = ['products', 'value', 'strategy'];
        $scenarios[self::MODE_BY_CATEGORY] = ['category', 'value', 'strategy'];

        return $scenarios;
    }


    public function beforeValidate()
    {
        if (!$this->value) {
            $this->strategy = 0;
        }

        return parent::beforeValidate();
    }

    /**
     * @param array $data
     *
     * @return CashbackForm
     */
    public static function createFromData(array $data): self
    {
        return new self($data);
    }
}