<?php


namespace app\modules\customerLoyalty\models\form;


use yii\base\Model;

/**
 * Class ApplyCashbackForm
 */
class ApplyCashbackForm extends Model
{
    public const SCENARIO_SUM = 'scenario_sum';
    public const SCENARIO_CODE = 'scenario_code';

    public $scenario = self::SCENARIO_SUM;
    /**
     * @var int
     */
    public $sum;
    /**
     * @var int
     */
    public $code;

    public function rules()
    {
        return [
            [['code', 'sum'], 'safe'],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CODE] = ['code'];
        $scenarios[self::SCENARIO_SUM] = ['sum'];

        return $scenarios;
    }
}