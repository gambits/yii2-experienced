<?php

namespace app\modules\customerLoyalty\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\customerLoyalty\models\CashbackSetting;

/**
 * CashbackSettingSearch represents the model behind the search form of `app\modules\customerLoyalty\models\CashbackSetting`.
 */
class CashbackSettingSearch extends CashbackSetting
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['expired', 'max_amount', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CashbackSetting::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'expired', $this->expired])
            ->andFilterWhere(['like', 'max_amount', $this->max_amount]);

        return $dataProvider;
    }
}
