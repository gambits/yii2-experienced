<?php

namespace app\modules\customerLoyalty\models;

use app\models\Products;
use app\models\ProductsItem;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%cashback_product}}".
 *
 * @property int          $id
 * @property int          $product_id  Product
 * @property int          $sum         Cashback Sum Product
 * @property int          $cashback_id Cashback Type
 * @property string       $created_at
 * @property string       $updated_at
 * @property Cashback     $cashback
 * @property ProductsItem $product
 */
class CashbackProduct extends BaseModel
{
    const KEY_CACHE_PRODUCTS_WITH_CASHBACK = 'productsWithCashback';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%cashback_product}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'cashback_id'], 'required'],
            [['product_id', 'sum', 'cashback_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['cashback_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cashback::class, 'targetAttribute' => ['cashback_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::class, 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('app', 'ID'),
            'product_id'  => Yii::t('app', 'Product'),
            'sum'         => Yii::t('app', 'Кешбэк'),
            'cashback_id' => Yii::t('app', 'Способ расчета кешбека'),
            'created_at'  => Yii::t('app', 'Created At'),
            'updated_at'  => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCashback()
    {
        return $this->hasOne(Cashback::class, ['id' => 'cashback_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(ProductsItem::class, ['id' => 'product_id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        CashbackProduct::invalidateCache();
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @param int $id - ProductItem::id
     *
     * @return CashbackProduct|null
     */
    public static function getByProduct($id)
    {
        return CashbackProduct::findOne(['product_id' => $id]);
    }

    /**
     * @param string $id
     *
     * @return string
     */
    public static function getKeyCache(string $id): string
    {
        return md5($id);
    }

    public static function invalidateCache(): void
    {
        foreach (self::getCacheKeys() as $cacheKey) {
            if (Yii::$app->cache->get($cacheKey)) {
                Yii::$app->cache->delete($cacheKey);
            }
        }
    }

    /**
     * @return array
     */
    public static function getCacheKeys(): array
    {
        return array_map(function ($itemKey) {
            return self::getKeyCache($itemKey);
        }, [
            self::KEY_CACHE_PRODUCTS_WITH_CASHBACK,
        ]);
    }
}
