<?php

namespace app\modules\customerLoyalty\models;

use Yii;

/**
 * This is the model class for table "{{%cashback_setting}}".
 *
 * @property int $id
 * @property string $expired Срок жизни бонусов
 * @property string $max_amount Максимальное количество бонусов на один заказ
 * @property string $created_at
 * @property string $updated_at
 */
class CashbackSetting extends \app\modules\customerLoyalty\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%cashback_setting}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['expired', 'max_amount'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'expired' => Yii::t('app', 'Срок жизни бонусов'),
            'max_amount' => Yii::t('app', 'Максимальное количество бонусов на один заказ'),
            'created_at' => Yii::t('app', 'Создано'),
            'updated_at' => Yii::t('app', 'Обновлено'),
        ];
    }
}
