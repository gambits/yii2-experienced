<?php

namespace app\modules\customerLoyalty\models;

use app\models\User;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%cashback_user}}".
 *
 * @property int    $id
 * @property int    $user_id User
 * @property double $sum_reserved Cashback sum reserved
 * @property double $sum Cashback sum
 * @property string $created_at
 * @property string $updated_at
 * @property User   $user
 */
class CashbackUser extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%cashback_user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['sum', 'sum_reserved'], 'number'],
            [['sum', 'sum_reserved'], 'default', 'value' => 0],
            [['created_at', 'updated_at'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'           => Yii::t('app', 'ID'),
            'user_id'      => Yii::t('app', 'User'),
            'sum'          => Yii::t('app', 'Cashback sum'),
            'sum_reserved' => Yii::t('app', 'Cashback status'),
            'created_at'   => Yii::t('app', 'Created At'),
            'updated_at'   => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
