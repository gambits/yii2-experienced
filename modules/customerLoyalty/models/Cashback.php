<?php

namespace app\modules\customerLoyalty\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%cashback}}".
 *
 * @property int               $id
 * @property int               $strategy Strategy cashback calculate, 1 - fix, 2 - %
 * @property string            $title
 * @property string            $created_at
 * @property string            $updated_at
 * @property CashbackProduct[] $cashbackProducts
 */
class Cashback extends BaseModel
{
    public const STRATEGY_FIX = 1;
    public const STRATEGY_PERCENT = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%cashback}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'strategy'], 'required'],
            [['title'], 'integer'],
            [['strategy'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'strategy'   => Yii::t('app', 'Способ расчета кешбека'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCashbackProducts()
    {
        return $this->hasMany(CashbackProduct::class, ['cashback_id' => 'id']);
    }
}
