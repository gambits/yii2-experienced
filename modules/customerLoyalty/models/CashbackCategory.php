<?php

namespace app\modules\customerLoyalty\models;

use app\models\ProductsCategory;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%cashback_category}}".
 *
 * @property int              $id
 * @property int              $category_id Category
 * @property int              $sum Cashback Sum Category
 * @property int              $cashback_id Cashback Type
 * @property string           $created_at
 * @property string           $updated_at
 * @property ProductsCategory $category
 * @property Cashback         $cashback
 */
class CashbackCategory extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%cashback_category}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'cashback_id'], 'required'],
            [['category_id', 'sum', 'cashback_id'], 'integer'],
            [['created_at', 'updated_at', 'category_id'], 'safe'],
            [['category_id'], 'unique'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductsCategory::class, 'targetAttribute' => ['category_id' => 'id']],
            [['cashback_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cashback::class, 'targetAttribute' => ['cashback_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Product'),
            'sum'         => Yii::t('app', 'Кешбэк'),
            'cashback_id' => Yii::t('app', 'Способ расчета кешбека'),
            'created_at'  => Yii::t('app', 'Created At'),
            'updated_at'  => Yii::t('app', 'Updated At'),
        ];
    }

    public static function getByCategory($id)
    {
        return self::findOne(['category_id' => $id]);
    }

    /**
     * @return ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ProductsCategory::class, ['id' => 'category_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCashback()
    {
        return $this->hasOne(Cashback::class, ['id' => 'cashback_id']);
    }
}
