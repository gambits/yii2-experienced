<?php

namespace app\modules\customerLoyalty\models;

use app\models\Orders;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%cacsback_history}}".
 *
 * @property int      $id
 * @property int      $order_id Order
 * @property int      $cashback_id Cachback
 * @property string   $datetime_apply Data apply
 * @property string   $created_at
 * @property string   $updated_at
 * @property Cashback $cashback
 * @property Orders   $order
 */
class CachbackHistory extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%cacsback_history}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'cashback_id'], 'required'],
            [['order_id', 'cashback_id'], 'integer'],
            [['datetime_apply', 'created_at', 'updated_at'], 'safe'],
            [['cashback_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cashback::class, 'targetAttribute' => ['cashback_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::class, 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'             => Yii::t('app', 'ID'),
            'order_id'       => Yii::t('app', 'Order'),
            'cashback_id'    => Yii::t('app', 'Cachback'),
            'datetime_apply' => Yii::t('app', 'Data apply'),
            'created_at'     => Yii::t('app', 'Created At'),
            'updated_at'     => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCashback()
    {
        return $this->hasOne(Cashback::class, ['id' => 'cashback_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::class, ['id' => 'order_id']);
    }
}
