<?php

namespace app\modules\customerLoyalty\DTO;

use app\modules\customerLoyalty\models\Cashback;

/**
 * Class DtoCashback
 */
class ProductCashbackDto
{
    /**
     * @var array
     */
    public $products;
    /**
     * @var int
     */
    public $strategy = Cashback::STRATEGY_FIX;
    /**
     * @var int
     */
    public $value;

    /**
     * ProductCashbackDto constructor.
     *
     * @param array $products
     * @param int   $strategy
     * @param int   $value
     */
    public function __construct(array $products, int $strategy, ?int $value = 0)
    {
        $this->products = $products;
        $this->strategy = $strategy;
        $this->value = $value;
    }
}