<?php


namespace app\modules\customerLoyalty\DTO;


use app\models\Orders;
use app\modules\customerLoyalty\components\interfaces\CashbackOrderInterface;

/**
 * Class OrderCashbackDto
 */
class OrderCashbackDto
{
    /**
     * @var CashbackOrderInterface|Orders
     */
    public $order;

    /**
     * OrderCashbackDto constructor.
     *
     * @param CashbackOrderInterface|Orders $order
     */
    public function __construct(CashbackOrderInterface $order)
    {
        $this->order = $order;
    }
}