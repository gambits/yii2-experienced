<?php

namespace app\modules\customerLoyalty\DTO;

use app\modules\customerLoyalty\models\Cashback;

/**
 * Class CategoryCashbackDto
 */
class CategoryCashbackDto
{
    /**
     * @var array
     */
    public $category;
    /**
     * @var int
     */
    public $strategy = Cashback::STRATEGY_FIX;
    /**
     * @var int
     */
    public $value;

    /**
     * CategoryCashbackDto constructor.
     *
     * @param array $categories
     * @param int   $strategy
     * @param int   $value
     */
    public function __construct(array $categories, int $strategy, int $value)
    {
        $this->category = $categories;
        $this->strategy = $strategy;
        $this->value = $value;
    }
}