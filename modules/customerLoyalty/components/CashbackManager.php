<?php


namespace app\modules\customerLoyalty\components;

use app\models\{
    Products,
    ProductsCategory,
    ProductsItem
};
use app\modules\customerLoyalty\components\interfaces\{
    CashbackCalculateInterface,
    CashbackManagerInterface
};
use app\modules\customerLoyalty\DTO\{
    CategoryCashbackDto,
    OrderCashbackDto,
    ProductCashbackDto
};
use app\modules\customerLoyalty\exceptions\InvalidCalculateStrategy;
use app\modules\customerLoyalty\models\{
    CashbackCategory,
    CashbackProduct,
    CashbackUser,
    form\CashbackForm
};
use dektrium\user\models\User as DektriumUser;
use Yii;
use yii\base\{
    Component,
    Model
};
use yii\caching\CacheInterface;
use yii\web\User;

/**
 * Class CashbackManager
 */
class CashbackManager extends Component implements CashbackManagerInterface
{
    /**
     * @var CacheInterface
     */
    private $cache;
    /**
     * @var CashbackCalculateInterface|CashbackCalculator
     */
    private $calculator;
    /**
     * @var CashbackFormatter
     */
    private $formatter;
    /**
     * @var User
     */
    private $user;

    /**
     * CashbackManager constructor.
     *
     * @param array                      $config
     * @param CashbackCalculateInterface $calculator
     */
    public function __construct($config = [], CashbackCalculateInterface $calculator)
    {
        $this->calculator = $calculator;
        $this->formatter = Yii::$app->cashbackFormatter;
        $this->cache = Yii::$app->cache;
        $this->user = Yii::$app->user;

        parent::__construct($config);
    }

    /**
     * Применение кешбека к товарам
     *
     * @param array $items
     *
     * @return bool
     */
    public function applyByProducts(ProductCashbackDto $cashback): bool
    {
        $products = ProductsItem::findAll(['id' => $cashback->products]);
        $counter = 0;
        /** @var Products $product */
        foreach ($products as $product) {
            if (!$cashbackProduct = CashbackProduct::getByProduct($product->id)) {
                $cashbackProduct = new CashbackProduct();
            }
            $cashbackProduct->setAttributes([
                'product_id'  => $product->id,
                'cashback_id' => $cashback->strategy,
                'sum'         => $cashback->value,
            ]);
            if ($cashbackProduct->save(false)) {
                $counter++;
            }
        }

        return (bool)$counter;
    }

    /**
     * Применение кешбека к категориям
     *
     * @param CategoryCashbackDto $cashback
     *
     * @return bool
     */
    public function applyByCategory(CategoryCashbackDto $cashback): bool
    {
        $categories = ProductsCategory::findAll(['id' => $cashback->category]);
        $counter = 0;
        /** @var ProductsCategory $category */
        foreach ($categories as $category) {
            if (!$casbackCategory = CashbackCategory::getByCategory($category->id)) {
                $casbackCategory = new CashbackCategory();
            }
            $casbackCategory->setAttributes([
                'category_id' => $category->id,
                'cashback_id' => $cashback->strategy,
                'sum'         => $cashback->value,
            ]);
            if ($casbackCategory->save(false)) {
                $counter++;
            }
        }

        return (bool)$counter;
    }

    /**
     * Товары с кешбэком в формате [{id товара} => {sum кешбека по этому товару}]
     *
     * @return CashbackProduct[]
     */
    public function getCashbackProducts(): array
    {
        $items = [];
        /** @var CashbackProduct $product */
        foreach (CashbackProduct::find()->all() as $cashbackProduct) {
            if ($sum = $cashbackProduct->product->getCashbackSize()) {
                $items[$cashbackProduct->product->product_id] = [
                    'sum'    => $sum,
                    'type'   => $cashbackProduct->cashback->strategy,
                    'symbol' => $this->formatter->symbolByTypeStrategy($cashbackProduct->cashback->strategy),
                ]; // тут учитывается откуда брать кешбэк (из товара или категории)
            }
        }

        return $items;
    }

    /**
     * @param OrderCashbackDto $cashback
     *
     * @return mixed|void
     * @throws InvalidCalculateStrategy
     */
    public function reserve(OrderCashbackDto $cashback): bool
    {
        if ($sum = $this->calculateCashback($cashback)) {
            $cashbackUser = $this->getOrCreateCashbackUser($this->user->id);
            $cashbackUser->setAttributes([
                'user_id'      => $this->user->id,
                'sum_reserved' => $sum,
            ]);

            return $cashbackUser->save(false);
        }

        return false;
    }

    /**
     * @param OrderCashbackDto $cashback
     *
     * @return bool
     */
    public function applyReservedCashback(OrderCashbackDto $cashback): bool
    {
        /** @var DektriumUser $user */
        $cashbackUser = $this->getOrCreateCashbackUser($cashback->order->user_id);
        $cashbackUser->sum += $user->getCashbackReserved();

        return $cashbackUser->save(false);
    }

    /**
     * @param OrderCashbackDto $cashback
     *
     * @return bool
     */
    public function rollbackCashback(OrderCashbackDto $cashback)
    {
        /** @var DektriumUser $user */
        $cashbackUser = $this->getOrCreateCashbackUser($cashback->order->user_id);

        $orderCashbackSum = $cashback->order->getCashbackSum();
        if ($cashbackUser->sum < $orderCashbackSum) {
            $orderCashbackSum = $cashbackUser->sum;
        }
        $cashbackUser->sum -= $orderCashbackSum;

        return $cashbackUser->save(false);
    }

    /**
     * @param OrderCashbackDto $cashback
     *
     * @return int
     * @throws InvalidCalculateStrategy
     */
    public function calculateCashback(OrderCashbackDto $cashback): int
    {
        $sum = 0;
        foreach ($cashback->order->ordersItem as $product) {
            $sum += $this->calculator->calculate($product->productsItem);
        }

        return $sum;
    }

    /**
     * @param CashbackForm|Model $cashbackForm
     *
     * @return int
     */
    public function resetBy(Model $cashbackForm): int
    {
        $countRow = 0;
        if ($cashbackForm->category) {
            $countRow += CashbackCategory::deleteAll(['category_id' => $cashbackForm->category]);
        }
        if ($cashbackForm->products) {
            $countRow += CashbackProduct::deleteAll(['product_id' => $cashbackForm->products]);
        }

        return $countRow;
    }

    /**
     * @param int  $productId
     * @param bool $withSymbol
     *
     * @return string
     */
    public function getSizeCashback($productId, $withSymbol = true)
    {
        $cashbackItem = ProductsItem::findOne($productId);
        $size = $cashbackItem->getCashbackSize();
        if (!$size) {
            return 0;
        }

        if ($withSymbol) {
            return $size . ' ' . $this->formatter->symbolByTypeStrategy($cashbackItem->getTypeCashback());
        }

        return $size . ' ' . $this->formatter->symbolByTypeStrategy($cashbackItem->getTypeCashback());
    }

    /**
     * @return CashbackUser|null
     */
    private function getOrCreateCashbackUser($id)
    {
        if (!$cashbackUser = CashbackUser::findOne(['user_id' => $id])) {
            $cashbackUser = new CashbackUser();
        }

        return $cashbackUser;
    }
}