<?php


namespace app\modules\customerLoyalty\components\calculateStrategy;

use app\modules\customerLoyalty\components\interfaces\{
    CashbackCalculateInterface,
    CashbackItemInterface,
    CashbackSupportedInterface
};
use app\modules\customerLoyalty\models\Cashback;


/**
 * Class FixStrategy
 */
class FixCalculateStrategy implements CashbackCalculateInterface, CashbackSupportedInterface
{
    /**
     * @param CashbackItemInterface $data
     *
     * @return int
     */
    public function calculate(CashbackItemInterface $data): int
    {
        return (int) $data->getCashbackSize();
    }

    /**
     * @param CashbackItemInterface $item
     *
     * @return bool
     */
    public function support(CashbackItemInterface $item): bool
    {
        return $item->getTypeCashback() === Cashback::STRATEGY_FIX;
    }
}
