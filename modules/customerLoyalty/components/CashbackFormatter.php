<?php


namespace app\modules\customerLoyalty\components;

use app\modules\customerLoyalty\models\Cashback;
use Yii;
use yii\i18n\Formatter;

/**
 * Class CashbackFormatter
 */
class CashbackFormatter extends Formatter
{
    /**
     * @param int $strategy
     *
     * @return string
     */
    public function symbolByTypeStrategy($strategy)
    {
        return Cashback::STRATEGY_FIX === $strategy ? 'Р' : '%';
    }

    public function formatMaxSumSetting($sum)
    {
        return $sum . Yii::t('app', 'Баллов');
    }
}