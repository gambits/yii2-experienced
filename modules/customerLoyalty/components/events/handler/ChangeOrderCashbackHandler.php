<?php

namespace app\modules\customerLoyalty\components\events\handler;

use app\models\Orders;
use app\models\OrdersLogic;
use app\modules\customerLoyalty\components\CashbackManager;
use app\modules\customerLoyalty\DTO\OrderCashbackDto;
use app\modules\customerLoyalty\exceptions\InvalidCalculateStrategy;
use Yii;
use yii\base\Event;

/**
 * Class ChangeOrderHandler
 */
class ChangeOrderCashbackHandler
{
    /**
     * Orders:EVENT_RESERVED_CASHBACK_ORDER
     *
     * @param Event $event
     */
    public function handleReserved(Event $event)
    {
        $model = $event->sender;
        $cashbackManager = Yii::$app->cashbackManager;
        $orderCashbackDto = new OrderCashbackDto($model);
        $cashbackManager->reserve($orderCashbackDto);
    }

    /**
     * Orders:EVENT_ROLLBACK_CASHBACK_ORDER
     *
     * @param Event $event
     */
    public function handleRollbackCashback(Event $event)
    {
        $model = $event->sender;
        $orderCashbackDto = new OrderCashbackDto($model);
        $cashbackManager = Yii::$app->cashbackManager;

        $cashbackManager->applyReservedCashback($orderCashbackDto);
    }

    /**
     * Orders:EVENT_APPLY_RESERVED_CASHBACK_ORDER
     *
     * @param Event $event
     */
    public function handleCalculateCashback(Event $event)
    {
        $model = $event->sender;
        $orderCashbackDto = new OrderCashbackDto($model);
        $cashbackManager = Yii::$app->cashbackManager;
        $cashbackManager->rollbackCashback($orderCashbackDto);
    }
}