<?php


namespace app\modules\customerLoyalty\components\interfaces;


interface CashbackItemInterface
{
    /**
     * @return int
     */
    public function getTypeCashback(): int;

    /**
     * @return float|int
     */
    public function getCashbackPrice(): int;

    /**
     * @return int
     */
    public function getCashbackSize(): int;
}