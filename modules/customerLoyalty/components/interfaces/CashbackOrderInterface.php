<?php


namespace app\modules\customerLoyalty\components\interfaces;

/**
 * Interface CashbackOrderInterface
 */
interface CashbackOrderInterface
{
    /**
     * @return int
     */
    public function getCashbackSum(): int;
}