<?php


namespace app\modules\customerLoyalty\components\interfaces;

use app\modules\customerLoyalty\DTO\CategoryCashbackDto;
use app\modules\customerLoyalty\DTO\OrderCashbackDto;
use app\modules\customerLoyalty\DTO\ProductCashbackDto;
use yii\base\Model;

/**
 * Interface CashbackManagerInterface
 */
interface CashbackManagerInterface
{
    /**
     * @param ProductCashbackDto $cashback
     *
     * @return bool
     */
    public function applyByProducts(ProductCashbackDto $cashback): bool;

    /**
     * @param CategoryCashbackDto $cashback
     *
     * @return bool
     */
    public function applyByCategory(CategoryCashbackDto $cashback): bool;

    /**
     * @return array
     */
    public function getCashbackProducts(): array;

    /**
     * @param OrderCashbackDto $cashback
     *
     * @return mixed
     */
    public function reserve(OrderCashbackDto $cashback);

    /**
     * @param OrderCashbackDto $cashback
     *
     * @return mixed
     */
    public function applyReservedCashback(OrderCashbackDto $cashback);

    /**
     * @param OrderCashbackDto $cashback
     *
     * @return int
     */
    public function calculateCashback(OrderCashbackDto $cashback): int;

    /**
     * @param Model $cashback
     *
     * @return mixed
     */
    public function resetBy(Model $cashback);
}