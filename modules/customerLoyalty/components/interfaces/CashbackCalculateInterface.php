<?php


namespace app\modules\customerLoyalty\components\interfaces;

/**
 * Interface CashbackCalculateInterface
 */
interface CashbackCalculateInterface
{
    /**
     * @param mixed $data
     *
     * @return int
     */
    public function calculate(CashbackItemInterface $data): int;
}