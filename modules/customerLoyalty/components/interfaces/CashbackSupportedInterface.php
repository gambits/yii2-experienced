<?php


namespace app\modules\customerLoyalty\components\interfaces;

/**
 * Interface CashbackSupported
 */
interface CashbackSupportedInterface
{
    /**
     * @param mixed $data
     *
     * @return bool
     */
    public function support(CashbackItemInterface $data): bool;
}