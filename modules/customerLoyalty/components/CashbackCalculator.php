<?php

namespace app\modules\customerLoyalty\components;

use app\modules\customerLoyalty\components\interfaces\{
    CashbackCalculateInterface,
    CashbackItemInterface,
    CashbackSupportedInterface
};
use app\modules\customerLoyalty\exceptions\InvalidCalculateStrategy;
use Yii;

/**
 * Class CashbackCalculate
 */
class CashbackCalculator implements CashbackCalculateInterface
{
    /**
     * Strategy calculate
     *
     * @var array
     */
    private $strategy = [];

    /**
     * CashbackCalculate constructor.
     *
     * @param array $strategy
     */
    public function __construct(array $strategy)
    {
        $this->strategy = $strategy;
    }

    /**
     * @throws InvalidCalculateStrategy
     */
    public function calculate(CashbackItemInterface $item): int
    {
        $sum = 0;
        foreach ($this->strategy as $strategy) {
            if (!is_subclass_of($strategy, CashbackCalculateInterface::class)) {
                throw new InvalidCalculateStrategy(Yii::t('exception', 'Invalid strategy {strategy}', ['{strategy}' => get_class($strategy)]));
            }
            /** @var CashbackCalculateInterface|CashbackSupportedInterface $strategyObject */
            $strategyObject = new $strategy();
            if ($strategyObject->support($item)) {
                $sum += $strategyObject->calculate($item);
            }
        }

        return $sum;
    }
}