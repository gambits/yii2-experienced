<?php


namespace app\modules\customerLoyalty\components;


use app\modules\customerLoyalty\models\CashbackSetting as ModelCashbackSetting;
use yii\i18n\Formatter;

/**
 * Class CashbackSetting
 */
class CashbackSetting extends Formatter
{
    /**
     * @var ModelCashbackSetting
     */
    private $model;

    public function init()
    {
        $this->loadModel();
        parent::init();
    }

    /**
     * @return int
     */
    public function getMaxSum(): int
    {
        return $this->model->max_amount ?? 0;
    }

    /**
     * @return int
     */
    public function getExpired(): int
    {
        return $this->model->expired ?? 0;
    }

    /**
     * @return ModelCashbackSetting|null
     */
    private function loadModel()
    {
        $this->model = ModelCashbackSetting::findOne(1);
    }
}