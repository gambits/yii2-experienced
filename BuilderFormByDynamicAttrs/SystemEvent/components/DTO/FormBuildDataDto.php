<?php

namespace app\modules\SystemEvent\components\DTO;

use app\models\Task;
use app\models\Workflow;
use app\modules\SystemEvent\components\EventHandleManager;
use app\modules\SystemEvent\components\interfaces\BuildDataInterface;
use app\modules\SystemEvent\components\interfaces\FormSettingInterface;
use app\modules\SystemEvent\helpers\SystemEventHelper;
use app\modules\SystemEvent\models\form\SystemEventForm;
use app\modules\SystemEvent\models\SystemEventType;
use app\modules\SystemEvent\models\SystemEventTypeSetting;
use app\widgets\ActiveForm;
use yii\base\Model;

/**
 * Class FormBuildDataDto
 */
class FormBuildDataDto implements BuildDataInterface
{
    /**
     * @var ActiveForm
     */
    public $form;
    /**
     * @var Model|SystemEventForm
     */
    public $model;
    /**
     * @var SystemEventTypeSetting
     */
    public $systemEventTypeSettingItem;
    /**
     * @var array|null
     * @example
     * 'dropdown' => [
     *           'type_handle' => [SystemEventHelper::class, 'getListSystemEventType'],
     *           'handle' => [EventHandleManager::class, 'getHandlerLabels'],
     *           'workflow' => [Workflow::class, 'getList'],
     *           'stage' => [],
     *           'personal_user_by_workflow' => [Task::class, 'getRolesList'],
     *           'recipients' => [Task::class, 'getRolesList'],
     * ]
     * 'template' => [
     *         'default' => '{label}{input}{error}',
     *         'stage' => '{input}{error}'
     * ]
     */
    public $dataFields = [
        'dropdown' => [
            'handler' => [Component::class, 'getHandler'],
        ],
        'template' => [
        ],
    ];

    /**
     * FormBuildDataDto constructor.
     *
     * @param ActiveForm             $form
     * @param Model                  $model
     * @param SystemEventTypeSetting $systemEventTypeSettingItem
     * @param array|null             $dataFields
     */
    public function __construct(
        ActiveForm $form,
        Model $model,
        SystemEventTypeSetting $systemEventTypeSettingItem,
        ?array $dataFields = null
    ) {
        $this->form = $form;
        $this->model = $model;
        $this->systemEventTypeSettingItem = $systemEventTypeSettingItem;
        if ($dataFields) {
            $this->dataFields = $dataFields;
        }
    }

    /**
     * @return ActiveForm
     */
    public function getForm(): ActiveForm
    {
        return $this->form;
    }

    /**
     * @return SystemEventForm|Model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @return SystemEventTypeSetting
     */
    public function getSystemEventTypeSettingItem(): SystemEventTypeSetting
    {
        return $this->systemEventTypeSettingItem;
    }

    /**
     * @return array|null
     */
    public function getDataFields(): ?array
    {
        return $this->dataFields;
    }

    /**
     * @return string
     */
    public function getFieldName(): string
    {
        return $this->systemEventTypeSettingItem->slug;
    }

    public function getSettingsFrom(): FormSettingInterface
    {
        return $this->systemEventTypeSettingItem->systemEventTypeSettgForm;
    }
}
