<?php

namespace app\modules\SystemEvent\components\DTO;

class ModelDto
{
    /**
     * @var string
     */
    public $key;
    /**
     * @var mixed
     */
    public $data;

    /**
     * ModelDto constructor.
     *
     * @param string $key
     * @param        $data
     */
    public function __construct($key, $data)
    {
        $this->key = $key;
        $this->data = $data;
    }
}
