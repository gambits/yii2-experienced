<?php

namespace app\modules\SystemEvent\components\DTO;

class EmailDto
{
    /**
     * @var string
     */
    public $from;
    /**
     * @var array|string
     */
    public $to;
    /**
     * @var string
     */
    public $subject;
    /**
     * @var string
     */
    public $message;

    public function __construct($from, $to, $subject, $message)
    {
        $this->from = $from;
        $this->to = $to;
        $this->subject = $subject;
        $this->message = $message;
    }
}
