<?php

namespace app\modules\SystemEvent\components;

use app\components\queue\SendEmailJob;
use app\modules\SystemEvent\components\DTO\EmailDto;
use app\modules\SystemEvent\components\interfaces\NotifyInterface;
use yii\base\Component;

/**
 * Class Notifier
 */
class Notifier extends Component implements NotifyInterface
{
    public $providers = [];

    /**
     * @param string $transport
     * @param mixed  $data
     *
     * @return string|null
     */
    public function send($transport, $data)
    {
        /** @var EmailNotifier $provider */
        $provider = $this->getProvider($transport);

        return $provider->send($data);
    }

    /**
     * @param $transport
     *
     * @return mixed|string
     */
    private function getProvider($transport)
    {
        return $this->providers[$transport];
    }
}
