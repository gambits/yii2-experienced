<?php

namespace app\modules\SystemEvent\components;

use app\models\TaskStage;
use app\models\Workflow;
use app\modules\SystemEvent\models\SystemEvent;
use app\modules\SystemEvent\models\SystemEventMailSetting;
use app\modules\SystemEvent\models\SystemEventModelPlaceholder;
use app\modules\SystemEvent\models\SystemEventType;
use app\modules\SystemEvent\models\SystemEventTypeSetting;
use app\modules\SystemEvent\models\SystemEventTypeSettingForm;
use yii\helpers\Json;

/**
 * @todo: Remove after deploy to master
 * Class DataLoader
 */
class DataLoader
{
    public const DATETIME_FORMAT = 'Y-m-d H:i:s';

    /**
     * Load firts data for SystemEvent
     */
    public function load()
    {
        $filePath = \Yii::getAlias(\Yii::$app->getModule('systemEvent')->dataForLoad);
        $items = json_decode(file_get_contents($filePath), true);
        if (!$items) {
            throw new \Exception(\Yii::t('app', 'Invalid file'));
        }
        foreach ($items as $componentName => $item) {
            switch ($componentName) {
                case 'types':
                    $this->loadTypes($item);
                    break;
                case 'typeSettings':
                    $this->loadTypeSettings($item);
                    break;
                case 'modelVariable':
                    $this->loadModelPlaceholder($item);
                    break;
                case 'emails':
                    $this->createEvent($item);
                    break;
            }
        }
    }

    public function loadTypes($items)
    {
        foreach ($items as $item) {
            if (!$model = SystemEventType::findOne(['title' => $item['title']])) {
                $model = new SystemEventType();
            }
            $model->load(
                [
                    'title' => $item['title'],
                    'description' => $item['description'],
                    'template_format' => $item['template_format'],
                    'entity' => Json::encode($item['entity'] ?? []),
                    'active' => 1,
                    'created_at' => date(self::DATETIME_FORMAT),
                ],
                ''
            );
            if (!$model->save()) {
                var_dump($model->errors);
            }
        }
    }

    /**
     * @param array $items
     */
    public function loadTypeSettings($items)
    {
        foreach ($items as $item) {
            if (!$model = SystemEventTypeSetting::findOne(['type_id' => $item['type_id'], 'title' => $item['title']])) {
                $model = new SystemEventTypeSetting();
            }
            $model->load(
                [
                    'type_id' => $item['type_id'],
                    'title' => $item['title'],
                    'system_description' => $item['system_description'],
                    'is_flag' => $item['is_flag'] ?? false,
                ],
                ''
            );
            if (!$model->save()) {
                var_dump($model->errors);
                die;
            }
            if (isset($item['settingsForm'])) {
                $settingForm = $item['settingsForm'];
                $this->loadSettingTypeForm($model->id, $settingForm);
            }
        }
    }

    public function loadSettingTypeForm($settingId, $settingForm)
    {
        if (!$modelSetting = SystemEventTypeSettingForm::findOne(['setting_id' => $settingId])) {
            $modelSetting = new SystemEventTypeSettingForm();
        }
        $modelSetting->load(
            [
                'setting_id' => $settingId,
                'is_required' => $settingForm['is_required'],
                'is_multiple' => $settingForm['is_multiple'],
                'field_type' => $settingForm['field_type'],
                'data_field' => $settingForm['data_field'] ?? json_encode('[]'),
                'type_init_data' => $settingForm['type_init_data'] ?? null,
            ],
            ''
        );
        $modelSetting->save(false);
    }

    public function loadModelPlaceholder($items)
    {
        foreach ($items as $entityName => $item) {
            foreach ($item as $placeholderData) {
                if (!$model = SystemEventModelPlaceholder::findOne(['placeholder' => $placeholderData['placeholder'], 'entity' => $entityName])) {
                    $model = new SystemEventModelPlaceholder();
                }
                $model->load($placeholderData, '');
                $model->entity = $entityName;
                $model->save(false);
            }
        }
    }

    public function createEvent($items)
    {
        foreach ($items as $item) {
            $data = [
                'type_id' => $item['type_id'],
                'event_name' => $item['eventName'],
            ];
            $workflow = isset($item['workflow']) ? $item['workflow'] : [];
            foreach ($workflow as $workflowItem) {
                $data['event_name'] .= '; workflow: ' . $workflowItem['name'];
                $model = new SystemEvent($data);
                if ($model->save(false)) {
                    $stages = isset($item['stages']) ? $item['stages'] : [];
                    if ($stages) {
                        $taskStageSlug = array_map(
                            function ($it) {
                                $taskStage = TaskStage::findOne($it['id']);
                                if ($taskStage) {
                                    return $taskStage->getSlug();
                                } elseif ($it['isUnknow'] ?? false) {
                                    return $it['id'];
                                } else {
                                    return isset($it['id']) ? 'system_' . $it['id'] : $it;
                                }
                            },
                            $stages
                        );
                    }
                    if (
                        !$systemEventMailSettings = SystemEventMailSetting::findOne(
                            [
                            'system_event_id' => $model->id,
                            'type_handle' => $item['type_id'],
                            ]
                        )
                    ) {
                        $systemEventMailSettings = new SystemEventMailSetting();
                    }
                    if ($wFlow = Workflow::findOne($workflowItem['id'])) {
                        $systemEventMailSettings->load(
                            [
                                'system_event_id' => $model->id,
                                'type_handle' => $item['type_id'],
                                'stage' => Json::encode($taskStageSlug),
                                'workflow' => $wFlow->id,
                                'list_email' => Json::encode([]),
                                'role' => isset($item['role']) ? Json::encode($item['role']) : '[]',
                                'recipients' => isset($item['recipients']) ? Json::encode($item['recipients']) : null,
                                'personal_user_by_workflow' => isset($item['personal_user_by_workflow']) ? Json::encode(
                                    $item['personal_user_by_workflow']
                                ) : null,
                                'subject_mail' => $item['mail']['subject'] ?? null,
                                'text_mail' => $item['mail']['message'] ?? null,
                                'text_for_informer' => $item['mail']['message'] ?? null,
                            ],
                            ''
                        );
                    }
                }
                $systemEventMailSettings->save(false);
            }
        }
    }
}
