<?php

namespace app\modules\SystemEvent\components;

use app\components\queue\SendEmailJob;
use app\modules\SystemEvent\components\DTO\EmailDto;

class EmailNotifier
{
    /**
     * @param EmailDto $emailDto
     *
     * @return string|null
     */
    public function send(EmailDto $emailDto)
    {
        return \Yii::$app->queue->push(
            new SendEmailJob(
                [
                    'from' => $emailDto->from ?? \Yii::$app->params['no-reply-email'],
                    'subject' => $emailDto->subject,
                    'html' => $emailDto->message,
                    'emails' => $emailDto->to,
                ]
            )
        );
    }
}
