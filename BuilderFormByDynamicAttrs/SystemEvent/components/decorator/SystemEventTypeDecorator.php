<?php

namespace app\modules\SystemEvent\components\decorator;

use app\modules\SystemEvent\components\interfaces\ConfiguratorActiveFormFieldInterface;
use app\modules\SystemEvent\components\interfaces\FormSettingInterface;
use yii\widgets\ActiveField;

/**
 * Class SystemEventTypeDecorator
 */
class SystemEventTypeDecorator extends MainFieldDecorator implements ConfiguratorActiveFormFieldInterface
{
    public const SUPPORT_PROPERTY = 'type_handle';

    /**
     * @param string $slug
     *
     * @return bool
     */
    public function support($slug): bool
    {
        return $slug === self::SUPPORT_PROPERTY;
    }

    public function configure(string $property, FormSettingInterface $formSettings): ActiveField
    {
        $this->activeField->inputOptions['disabled'] = $formSettings->isDisabled($property);

        return $this->activeField;
    }
}
