<?php

namespace app\modules\SystemEvent\components\decorator;

use app\modules\SystemEvent\components\DTO\FormBuildDataDto;
use app\modules\SystemEvent\components\interfaces\ConfiguratorActiveFormFieldInterface;
use app\modules\SystemEvent\components\interfaces\FormSettingInterface;
use yii\widgets\ActiveField;

/**
 * Decorator ActiveForm field
 * Class MainDecorator
 */
abstract class MainFieldDecorator
{
    /**
     * @var ActiveField
     */
    protected $activeField;
    /**
     * @var FormBuildDataDto
     */
    protected $data;

    /**
     * MainFieldDecorator constructor.
     *
     * @param ActiveField      $activeField - AcitveForm field
     * @param FormBuildDataDto $data - for logic decorator
     */
    public function __construct(ActiveField $activeField, FormBuildDataDto $data)
    {
        $this->activeField = $activeField;
        $this->data = $data;
    }

    /**
     * @param string               $property
     * @param FormSettingInterface $formSettings
     *
     * @return ActiveField
     */
    public function configure(string $property, FormSettingInterface $formSettings)
    {
        $this->activeField->inputOptions = array_merge(
            $this->activeField->inputOptions,
            [
                'id' => $property,
                'class' => 'form-control js-workflow',
                'required' => $formSettings->isRequired(),
            ]
        );
    }
}
