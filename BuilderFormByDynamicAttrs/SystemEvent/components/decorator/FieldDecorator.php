<?php

namespace app\modules\SystemEvent\components\decorator;

use app\modules\SystemEvent\components\DTO\FormBuildDataDto;
use app\modules\SystemEvent\components\interfaces\ConfiguratorActiveFormFieldInterface;
use app\modules\SystemEvent\components\interfaces\FormSettingInterface;
use yii\widgets\ActiveField;

/**
 * Common field decorator
 * Class FieldDecorator
 *
 * @property ActiveField      $activeField
 * @property FormBuildDataDto $data
 */
class FieldDecorator extends MainFieldDecorator implements ConfiguratorActiveFormFieldInterface
{
    /**
     * @param string               $property
     * @param FormSettingInterface $formSettings
     *
     * @return ActiveField
     */
    public function configure(string $property, FormSettingInterface $formSettings): ActiveField
    {
        parent::configure($property, $formSettings);

        $value = $formSettings->getCurrentValue($this->data->model) ?? $formSettings->getDefaultValue();
        if ('dropdown' === $formSettings->getFieldType()) {
            $this->activeField->inputOptions['multiple'] = $formSettings->isMultiple();
        }
        $this->activeField->inputOptions['value'] = $value;
        if (is_array($value) && 'input' === $formSettings->getFieldType()) {
            $this->activeField->inputOptions['value'] = implode(', ', $value);
        }

        return $this->activeField->label($formSettings->getLabel($property));
    }

    public function support($slug): bool
    {
        return true;
    }
}
