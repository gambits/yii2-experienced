<?php

namespace app\modules\SystemEvent\components;

use app\components\interfaces\CrudOperationInterface;
use app\modules\SystemEvent\components\interfaces\InformerInterface;
use yii\base\Component;
use yii\base\Model;

/**
 * Class InformerManager
 */
class InformerManager extends Component implements InformerInterface, CrudOperationInterface
{
    public function markRead()
    {
    }

    public function isRead(): bool
    {
    }

    public function markUnRead()
    {
    }

    /**
     * @return int
     */
    public function getTotalCount(): int
    {
    }

    /**
     * @return int
     */
    public function getList(): int
    {
    }

    public function getItem($id)
    {
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
    }

    /**
     * @param Model $form
     *
     * @return mixed|void
     */
    public function create(Model $form)
    {
    }

    /**
     * @param Model $form
     *
     * @return mixed|void
     */
    public function update(Model $form)
    {
    }

    /**
     * @param int $id
     *
     * @return mixed|void
     */
    public function delete($id)
    {
    }
}
