<?php

namespace app\modules\SystemEvent\components;

use Yii;
use app\modules\SystemEvent\components\interfaces\PlaceholderInterface;
use app\modules\SystemEvent\components\interfaces\PlaceholderResolverInterface;
use app\modules\SystemEvent\models\SystemEventModelPlaceholder;
use yii\base\Model;
use yii\base\UnknownPropertyException;

class PlaceholderResolver implements PlaceholderResolverInterface
{
    /**
     * @todo: совпадение по именам свойств в переданных моделях.
     * @param Model[]|array        $models
     * @param PlaceholderInterface $placeholder
     *
     * @return bool|float|int|string|null
     */
    public function resolve($models, PlaceholderInterface $placeholder)
    {
        if (!$placeholder->getPathRelation()) {
            return null;
        }
        $value = null;
        $buffer = [];
        if (strpos($placeholder->getPathRelation(), '.') !== false) {
            $parts = explode('.', $placeholder->getPathRelation());
            foreach ($models as $model) {
                /** @var SystemEventModelPlaceholder $placeholder */
                foreach ($parts as $part) {
                    if (array_key_exists($part, $buffer)) {
                        continue;
                    }
                    try {
                        $model = $model->{$part};
                        $buffer[$part] = $part;
                    } catch (UnknownPropertyException $ex) {
                    }
                }
                if (is_scalar($model)) {
                    $value = $model;
                    break;
                }
            }
            $value = $value ?? null;
        } else {
            $buffer = [];
            foreach ($models as $model) {
                if (
                    array_key_exists($placeholder->relation_value, $buffer) ||
                    !property_exists($model, $placeholder->relation_value)
                ) {
                    continue;
                }
                /** @var SystemEventModelPlaceholder $placeholder */
                $value = $model->{$placeholder->relation_value};
                if ($value) {
                    break;
                }
            }
        }

        return $value;
    }
}
