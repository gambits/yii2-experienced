<?php

namespace app\modules\SystemEvent\components\eventDispatcher\handlers;

use app\components\queue\SendEmailJob;
use app\modules\DealerPoll\models\DealerPoll;
use app\modules\SystemEvent\components\DTO\EmailDto;
use app\modules\SystemEvent\components\interfaces\EventMailSettingInterface;
use app\modules\SystemEvent\components\interfaces\NotifyInterface;
use app\modules\SystemEvent\components\interfaces\SystemEventInterface;
use app\modules\SystemEvent\models\SystemEvent;
use yii\base\Event;
use yii\helpers\Html;
use yii\helpers\Url;
use \Yii;

/**
 * class EmailNotifyChangeActiveToForDealerPoll
 */
class EmailNotifyChangeActiveToForDealerPoll extends BaseHandler
{
    public const TYPE = 'email-notify-change-active-to-for-dealer-poll';
    public const LABEL = 'E-mail уведомление исполнителю ЛО о продлении срока проведения обследования';
    public const EVENT_TYPE_ID = 4;

    /**
     * @param Event                $event
     * @param SystemEventInterface $systemEvent
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function execute(Event $event, SystemEventInterface $systemEvent): bool
    {
        /** @var DealerPoll $dealerPoll */
        $dealerPoll = $event->sender;
        $link = Html::a(
            $this->labelForLink ?? Yii::t('app', 'Листы оценки'),
            Url::to(
                [
                    '/dealer-poll/default/survey',
                    'id' => $dealerPoll->id,
                ],
                true
            )
        );
        /** @var NotifyInterface $notifier */
        $notifier = Yii::$container->get(NotifyInterface::class);
        /* Message to Informer */
        $mailSettings = $systemEvent->getEmailSettings();
        $message = strtr(
            $mailSettings->getTextEmail(),
            $mailSettings->getPlaceholderAndValue([$dealerPoll], ['{link}' => $link])
        );

        return $notifier->send(
            'email',
            new EmailDto(
                \Yii::$app->params['no-reply-email'],
                $mailSettings->getTo($dealerPoll),
                $mailSettings->getSubject(),
                $message
            )
        );
    }
}
