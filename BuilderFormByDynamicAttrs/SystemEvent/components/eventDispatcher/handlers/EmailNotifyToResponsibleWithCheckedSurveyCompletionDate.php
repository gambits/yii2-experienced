<?php

namespace app\modules\SystemEvent\components\eventDispatcher\handlers;

use app\modules\SystemEvent\components\DTO\EmailDto;
use app\modules\SystemEvent\components\interfaces\SystemEventInterface;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\DealerPoll\models\DealerPoll;
use app\modules\SystemEvent\components\eventDispatcher\Event\SendMailEvent;
use yii\base\Event;

/**
 * Class EmailNotifyToResponsibleWithCheckedSurveyCompletionDate
 */
class EmailNotifyToResponsibleWithCheckedSurveyCompletionDate extends BaseHandler
{
    public const TYPE = 'email-notify-to-responsible-with-checked-survey-completion-date';
    public const LABEL = 'E-mail исполнителю об окончании срока проведения обследования по ЛО';
    public const EVENT_TYPE_ID = 1;

    /**
     * Если дата не больше текущей и ответственный НЕ указан : Уведомление автору
     *
     * @param SendMailEvent|Event $event
     *
     * @return bool|void
     * @throws \Exception
     */
    public function execute(Event $event, SystemEventInterface $systemEvent): bool
    {
        /** @var DealerPoll $dealerPoll */
        $dealerPoll = $event->sender;
        $email = $dealerPoll->task->responsiblePerson->email;
        $link = Html::a(
            'Листы оценки',
            Url::to(
                [
                    '/dealer-poll/default/survey',
                    'id' => $dealerPoll->id,
                ],
                true
            )
        );
        // --------------------
        $mailSettings = $systemEvent->getEmailSettings();
        $message = strtr(
            $mailSettings->getTextEmail(),
            $mailSettings->getPlaceholderAndValue(
                [$dealerPoll],
                [
                    '{link}' => $link,
                ]
            )
        );
        $this->sendEmail(
            $mailSettings,
            new EmailDto(
                \Yii::$app->params['no-reply-email'],
                array_merge(
                    [$email],
                    $mailSettings->getTo($dealerPoll->dealerPoll)
                ),
                $mailSettings->getSubject(),
                $message
            )
        );

        return true;
    }
}
