<?php

namespace app\modules\SystemEvent\components\eventDispatcher\handlers;

use app\models\Task;
use app\models\User;
use app\modules\DealerPoll\models\DealerPoll;
use app\modules\DealerPoll\models\DealerPollForm;
use app\modules\SystemEvent\components\DTO\EmailDto;
use app\modules\SystemEvent\components\interfaces\SystemEventInterface;
use Yii;
use yii\base\Event;
use yii\helpers\Html;
use yii\helpers\Url;

class EmailNotifyTaskChangeInspector extends BaseHandler
{
    public const TYPE = 'email-notify-task-change-inspector';
    public const LABEL = 'E-mail уведомление сотрудника о смене ответственного задачи ЛО';
    public const EVENT_TYPE_ID = 2;

    public function execute(Event $event, SystemEventInterface $systemEvent): bool
    {
        if ($event->sender instanceof DealerPoll) {
            $userTasks[$event->sender->task->inspector_id] = [$event->sender];
            $dealerPollId = $event->sender->id;
            $models = [
                $event->sender,
                $event->sender->task,
            ];
        } elseif ($event->sender instanceof Task) {
            $userTasks[$event->sender->inspector_id] = [$event->sender];
            $dealerPollId = $event->sender->dealer_poll_id;
            $models = [
                $event->sender->dealerPoll,
                $event->sender,
            ];
        } else {
            return false;
        }
        /** @var Task $task */
        foreach ($userTasks as $userId => $tasks) {
            if (!$user = User::findOne($userId)) {
                continue;
            }
            // --------------------
            $mailSettings = $systemEvent->getEmailSettings();
            $message = strtr(
                $mailSettings->getTextEmail(),
                $mailSettings->getPlaceholderAndValue(
                    $models,
                    [
                        '{link}' => Html::a(Yii::t('app', 'Листы оценки'), Url::to(['/dealer-poll/default/survey', 'id' => $dealerPollId], true)),
                    ]
                )
            );
            $this->sendEmail(
                $mailSettings,
                new EmailDto(
                    \Yii::$app->params['no-reply-email'],
                    array_merge([$user->email], $mailSettings->getTo($event->sender->dealerPoll)),
                    $mailSettings->getSubject(),
                    $message
                )
            );
        }

        return true;
    }
}
