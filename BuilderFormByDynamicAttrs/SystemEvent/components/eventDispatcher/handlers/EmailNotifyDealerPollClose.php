<?php

namespace app\modules\SystemEvent\components\eventDispatcher\handlers;

use app\helpers\Helper;
use app\models\DealerContact;
use app\models\Task;
use app\models\TaskLog;
use app\models\TaskStage;
use app\modules\SystemEvent\components\DTO\EmailDto;
use app\modules\SystemEvent\components\interfaces\SystemEventInterface;
use yii\base\Event;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class EmailNotifyDealerPollClose extends BaseHandler
{
    public const TYPE = 'email-notify-dealer-poll-close';
    public const LABEL = 'E-mail уведомление дилера о закрытии листа оценки';
    public const EVENT_TYPE_ID = 1;

    public $message;

    /**
     * @param $event
     *
     * @return bool Выполнено ли действие полностью
     */
    public function execute(Event $event, SystemEventInterface $systemEvent): bool
    {
        /** @var Task $sender */
        if (
            !($sender = $event->sender) ||
            !$sender instanceof Task ||
            !$sender->dealer_poll_id ||
            ($sender->getTaskStageSlug() !== TaskStage::SYSTEM_CLOSED_SLUG)
        ) {
            return false;
        }
        $emails = array_map(
            function ($item) {
                return $item->value;
            },
            DealerContact::find()
                ->andWhere(
                    [
                        'contact_type_id' => $sender->dealerPoll->poll->getSettingsValueByType(\app\modules\poll\models\PollSettings::TYPE_CONTACT_TYPE),
                        'dealer_id' => $sender->dealerPoll->dealer_id,
                        'status' => DealerContact::STATUS_ACTIVE,
                    ]
                )->all()
        );
        $emails = array_unique($emails);
        $emails = array_filter($emails);
        // --------------------------------------------
        $mailSettings = $systemEvent->getEmailSettings();
        $message = strtr(
            $mailSettings->getTextEmail(),
            $mailSettings->getPlaceholderAndValue([$event->sender->dealerPoll, $event->sender])
        );
        $this->sendEmail(
            $mailSettings,
            new EmailDto(
                \Yii::$app->params['no-reply-email'],
                array_merge($emails, $mailSettings->getTo($sender->dealerPoll)),
                $mailSettings->getSubject(),
                $message
            )
        );

        return true;
    }
}
