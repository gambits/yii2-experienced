<?php

namespace app\modules\SystemEvent\components\eventDispatcher\handlers;

use app\components\queue\SendEmailJob;
use app\models\Task;
use app\models\TaskStage;
use app\models\User;
use app\modules\DealerPoll\models\DealerPoll;
use app\modules\SystemEvent\components\eventDispatcher\Event\SendMailEvent;
use app\modules\SystemEvent\components\interfaces\SystemEventInterface;
use yii\base\Event;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class EmailNotifyToAuthorForDealerPollWithoutResponsible
 */
class EmailNotifyToAuthorForDealerPollWithoutResponsible extends BaseHandler
{
    public const TYPE = 'email-notify-to-author-for-dealer-poll-without-responsible';
    public const LABEL = 'E-mail автору об отсутствии ответственного по листу оценки';
    public const EVENT_TYPE_ID = 2;

    /**
     * Если дата не больше текущей и ответственный НЕ указан : Уведомление автору
     *
     * @param SendMailEvent|Event $event
     *
     * @return bool|void
     * @throws \Exception
     */
    public function execute(Event $event, SystemEventInterface $systemEvent): bool
    {
        /** @var DealerPoll $dealerPoll */
        foreach ($event->dataMail as $email => $dealerPolls) {
            $linkToList = Url::to(
                [
                    '/dealer-poll/default/index',
                    'user_id[]' => implode(
                        ', ',
                        array_map(
                            function ($dealerPoll) {
                                return $dealerPoll->task->author->id;
                            },
                            $dealerPolls
                        )
                    ),
                    'activeFrom' => (new \DateTime())->format('d.m.Y'),
                    'is_taken_to_work' => 0,
                    'responsiblePersonId' => 0,
                    'status' => 0,
                ],
                true
            );
            // --------------------
            $mailSettings = $systemEvent->getEmailSettings();
            $message = strtr(
                $mailSettings->getTextEmail(),
                $mailSettings->getPlaceholderAndValue(
                    [], // ? не известно какие модели (тригерится от (new DealerPoll)->trigger(...)
                    [
                        '{link}' => Html::a(Yii::t('app', 'Листы оценки'), $linkToList),
                    ]
                )
            );
            $this->sendEmail(
                $mailSettings,
                new EmailDto(
                    \Yii::$app->params['no-reply-email'],
                    $mailSettings->getTo($event->sender->dealerPoll),
                    $mailSettings->getSubject(),
                    $message
                )
            );
        }

        return true;
    }
}
