<?php

namespace app\modules\SystemEvent\components\eventDispatcher\handlers;

use app\components\queue\SendEmailJob;
use app\models\Task;
use app\models\User;
use app\modules\DealerPoll\models\DealerPoll;
use app\modules\EventDispatcher\events\DealerPollAddObserversEvent;
use app\modules\SystemEvent\components\DTO\EmailDto;
use app\modules\SystemEvent\components\interfaces\NotifyInterface;
use app\modules\SystemEvent\components\interfaces\SystemEventInterface;
use Yii;
use yii\base\Event;
use Yii\base\InvalidConfigException;
use yii\helpers\Html;

class EmailNotifyDealerPollAddObservers extends BaseHandler
{
    public const TYPE = 'email-notify-dealer-poll-add-observers';
    public const LABEL = 'E-mail уведомление наблюдателю листа оценки';
    public const EVENT_TYPE_ID = 2;

    /**
     * @param DealerPollAddObserversEvent $event
     * @param SystemEventInterface        $systemEvent
     *
     * @return bool
     * @throws InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function execute(Event $event, SystemEventInterface $systemEvent): bool
    {
        /** @var Task $task */
        $task = $event->sender;
        $userIds = $event->userIds;
        $dealerPoll = $task->dealerPoll;
        /** @var NotifyInterface $notifier */
        $notifier = \Yii::$container->get(NotifyInterface::class);
        /*! add Message to Informer */
        $mailSettings = $systemEvent->getEmailSettings();
        $to = $mailSettings->getTo($dealerPoll);
        $subject = $mailSettings->getSubject();
        $message = strtr(
            $mailSettings->getTextEmail(),
            $mailSettings->getPlaceholderAndValue(
                [$dealerPoll, $task],
                [
                    '{link}' => Html::a(
                        $dealerPoll->name,
                        ['/dealer-poll/default/survey', 'id' => $dealerPoll->id],
                        ['target' => '_blank']
                    ),
                ]
            )
        );
        foreach ($userIds as $userId) {
            $user = User::findOne($userId);
            if (!$user || !$user->email) {
                continue;
            }
            $to[] = $user->email;
            $notifier->send(
                'email',
                new EmailDto(
                    \Yii::$app->params['no-reply-email'],
                    $to,
                    $subject,
                    $message
                )
            );
        }

        return true;
    }
}
