<?php

namespace app\modules\SystemEvent\components\eventDispatcher\handlers;

use app\models\DealerContact;
use app\models\Task;
use app\models\User;
use app\modules\DealerPoll\models\DealerPoll;
use app\modules\poll\models\Poll;
use app\modules\poll\models\PollSettings;
use app\modules\SystemEvent\components\DTO\EmailDto;
use app\modules\SystemEvent\components\interfaces\HandlerEventInterface;
use app\modules\SystemEvent\components\interfaces\SystemEventInterface;
use Yii;
use yii\base\Event;
use yii\helpers\Html;

class EmailNotifyTaskAssignee extends BaseHandler
{
    public const TYPE = 'email-notify-task-assignee';
    public const LABEL = 'E-mail уведомление сотрудника о смене статуса задачи';
    public const EVENT_TYPE_ID = 1;

    public function execute(Event $event, SystemEventInterface $systemEvent): bool
    {
        if (
            !($sender = $event->sender) ||
            !$sender instanceof Task ||
            ($sender->workflow_id != $this->workflowId) ||
            !in_array($sender->getTaskStageSlug(), $this->stageId)
        ) {
            return false;
        }
        $link = Yii::$app->formatter->taskEntityLink($sender, true);
        // --------------------
        $mailSettings = $systemEvent->getEmailSettings();
        $message = strtr(
            $mailSettings->getTextEmail(),
            $mailSettings->getPlaceholderAndValue([$event->sender->dealerPoll, $event->sender], ['{link}' => $link])
        );
        $this->sendEmail(
            $mailSettings,
            new EmailDto(
                \Yii::$app->params['no-reply-email'],
                $mailSettings->getTo($event->sender->dealerPoll),
                $mailSettings->getSubject(),
                $message
            )
        );

        return true;
    }

    /**
     * @param      $users
     * @param Task $task
     *
     * @return array
     */
    protected function getAdditionalDealerEmails($users, Task $task)
    {
        /** @var User[] $users * */
        if (!is_array($users)) {
            $users = [$users];
        }
        $entity = $task->getEntity();
        $emails = [];
        if ($entity instanceof DealerPoll) {
            /** @var Poll $poll */
            $poll = $entity->poll;
            $contactTypes = $poll->getSettingsValueByType(PollSettings::TYPE_CONTACT_TYPE);
            if (!$contactTypes) {
                return $emails;
            }
            $dealers = [];
            foreach ($users as $user) {
                if (in_array($user->role, User::getDealerRoles())) {
                    $dealers[] = $user->dealer_id;
                }
            }
            if (!$dealers) {
                return $emails;
            }
            foreach (
                DealerContact::findAll(
                    [
                        'dealer_id' => $dealers,
                        'status' => DealerContact::STATUS_ACTIVE,
                        'contact_type_id' => $contactTypes,
                    ]
                ) as $contact
            ) {
                $emails[] = $contact->value;
            }
        }

        return $emails;
    }
}
