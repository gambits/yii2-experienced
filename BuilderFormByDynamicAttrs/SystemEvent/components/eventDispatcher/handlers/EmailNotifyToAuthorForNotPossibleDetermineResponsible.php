<?php

namespace app\modules\SystemEvent\components\eventDispatcher\handlers;

use app\modules\SystemEvent\components\DTO\EmailDto;
use app\modules\SystemEvent\components\interfaces\SystemEventInterface;
use Yii;
use app\components\queue\SendEmailJob;
use app\models\Task;
use app\models\TaskStage;
use app\models\User;
use app\modules\DealerPoll\models\DealerPoll;
use app\modules\SystemEvent\components\eventDispatcher\Event\SendMailEvent;
use yii\base\Event;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Лист оценки {code-dealer-poll} {name-dealer-poll} для проведения обследования дилерского центра
 * {marketing-name-dealer} не может быть направлен на заполнение из-за отсутствия исполнителя
 * {link-to-list-dealer-poll}
 * Обратитесь к Администратору системы для добавления исполнителя и выполните направление Листа оценки на заполнение вручную
 *
 * Class EmailNotifyToAuthorForDealerPollWithoutResponsible
 */
class EmailNotifyToAuthorForNotPossibleDetermineResponsible extends BaseHandler
{
    public const TYPE = 'email-notify-to-author-for-not-possible-determine-responsible';
    public const LABEL = 'E-mail автору о невозможности определить исполнителя по ЛО';
    public const EVENT_TYPE_ID = 2;

    /**
     * Если дата не больше текущей и исполнитель не может быть опеределен: Уведомление автору
     *
     * @param SendMailEvent|Event $event
     *
     * @return bool|void
     * @throws \Exception
     */
    public function execute(Event $event, SystemEventInterface $systemEvent): bool
    {
        $mailSettings = $systemEvent->getEmailSettings();
        /** @var DealerPoll $dealerPoll */
        foreach ($event->dataMail as $email => $dealerPolls) {
            foreach ($dealerPolls as $dealerPoll) {
                $link = Html::a($this->labelForLink ?? 'Листы оценки', Url::to(['/dealer-poll/default/index'], true));
                // --------------------
                $message = strtr(
                    $mailSettings->getTextEmail(),
                    $mailSettings->getPlaceholderAndValue(
                        [$dealerPoll],
                        [
                            '{link}' => $link,
                        ]
                    )
                );
                $this->sendEmail(
                    $mailSettings,
                    new EmailDto(
                        \Yii::$app->params['no-reply-email'],
                        array_merge(
                            [$email],
                            $mailSettings->getTo($dealerPoll->dealerPoll)
                        ),
                        $mailSettings->getSubject(),
                        $message
                    )
                );
            }
        }

        return true;
    }
}
