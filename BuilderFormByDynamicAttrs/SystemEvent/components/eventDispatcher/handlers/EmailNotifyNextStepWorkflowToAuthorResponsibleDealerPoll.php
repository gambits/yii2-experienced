<?php

namespace app\modules\SystemEvent\components\eventDispatcher\handlers;

use app\components\queue\SendEmailJob;
use app\models\Task;
use app\models\TaskStage;
use app\models\User;
use app\modules\DealerPoll\models\DealerPoll;
use app\modules\SystemEvent\components\DTO\EmailDto;
use app\modules\SystemEvent\components\interfaces\NotifyInterface;
use app\modules\SystemEvent\components\interfaces\SystemEventInterface;
use yii\base\Event;
use Yii;
use yii\helpers\Url;

/**
 * Class EmailNotifyNextStepWorkflowToAuthorResponsibleDealerPoll
 */
class EmailNotifyNextStepWorkflowToAuthorResponsibleDealerPoll extends BaseHandler
{
    public const TYPE = 'email-notify-next-step-workflow-to-author-responsible-dealer-poll';
    public const LABEL = 'E-mail ответственному лицу о начале следующего этапа workflow';
    public const EVENT_TYPE_ID = 1;

    /**
     * @todo: не описано в ТЗ, не хватает переменной для обозначения следующего этапа workflow
     *
     * @param Event                $event
     * @param SystemEventInterface $systemEvent
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function execute(Event $event, SystemEventInterface $systemEvent): bool
    {
        /** @var DealerPoll $dealerPoll */
        $dealerPoll = $event->sender;
        $author = $dealerPoll->task ? $dealerPoll->task->author : null;
        if (!$author) {
            return false;
        }
        $linkToList = Url::to(
            [
                '/dealer-poll/default/index',
                'user_id' => $author->id,
                'activeFrom' => (new \DateTime())->add(new \DateInterval('P1D'))->format('d.m.Y'),
                'status' => TaskStage::SYSTEM_NEW_SLUG,
            ],
            true
        );
        $stage = $dealerPoll->task->taskStage;
        $slug = $stage instanceof TaskStage ? $stage->getSlug() : $stage->type . '_' . $stage->id;
        $allowStage = TaskStage::getStageList($dealerPoll->task->workflow_id, Task::getUserRoles($dealerPoll->task, $dealerPoll->user_id));
        if (!$nextStageSlug = $this->getNextStage($allowStage, $slug)) {
            $nextStageSlug = $allowStage[$slug];
        }
        $message = strtr(
            $this->message,
            [
                '{Дата начала}' => $dealerPoll->active_from,
                '{id}' => $dealerPoll->id,
                '{poll-name}' => $dealerPoll->name,
                '{dealer-name-implode}' => $dealerPoll->dealer->marketing_name,
                '{workflow-name}' => $allowStage[$nextStageSlug] ?? $nextStageSlug,
                '{link-to-list-dealer-poll}' => '<a href="' . $linkToList . '">' . Yii::t('app', $this->labelForLink) . '</a>',
            ]
        );
        $email = $author->email;
        \Yii::$app->mailer->compose()
            ->setFrom(\Yii::$app->params['no-reply-email'])
            ->setTo($email)
            ->setSubject($this->subject)
            ->setHtmlBody($message)
            ->send();
        \Yii::getLogger()->log('Send email to: ' . $author->email . PHP_EOL, 'debug', 'mailer');
        \Yii::getLogger()->log('Email data: ' . implode(', ', [$this->subject, $message]), 'debug', 'mailer');
        if ($responsibleUser && ($author->id !== $responsibleUser->id)) {
            $email = $responsibleUser->email;
            // send email to responsibleUser
            \Yii::$app->mailer->compose()
                ->setFrom(\Yii::$app->params['no-reply-email'])
                ->setTo($email)
                ->setSubject($this->subject)
                ->setHtmlBody($message)
                ->send();
            Yii::debug('Send email to: ' . $responsibleUser->email . PHP_EOL, 'mailer');
            Yii::debug('Email data: ' . implode(', ', [$this->subject, $message]) . PHP_EOL, 'mailer');
        }
        // ----------------------------------------
        /** @var NotifyInterface $notifier */
        $notifier = Yii::$container->get(NotifyInterface::class);
        /* Message to Informer */
        $mailSettings = $systemEvent->getEmailSettings();
        $message = strtr(
            $mailSettings->getTextEmail(),
            $mailSettings->getPlaceholderAndValue([$dealerPoll], ['{link}' => $link])
        );

        return $notifier->send(
            'email',
            new EmailDto(
                \Yii::$app->params['no-reply-email'],
                $mailSettings->getTo($dealerPoll),
                $mailSettings->getSubject(),
                $message
            )
        );
        // ----------------------------------------



        return true;
    }

    /**
     * @param array   $array
     * @param integer $key
     *
     * @return string|bool
     */
    private function getNextStage($array, $key)
    {
        if (array_key_exists($key, $array)) {
            $arrayKeys = array_keys($array);
            $currentIndex = array_search($key, $arrayKeys);
            $currentIndex++;
            if (isset($arrayKeys[$currentIndex])) {
                return $arrayKeys[$currentIndex];
            }
        }

        return false;
    }
}
