<?php

namespace app\modules\SystemEvent\components\eventDispatcher\handlers;

use app\components\queue\SendEmailJob;
use app\models\Task;
use app\models\TaskStage;
use app\models\User;
use app\modules\DealerPoll\models\DealerPoll;
use app\modules\SystemEvent\components\DTO\EmailDto;
use app\modules\SystemEvent\components\eventDispatcher\Event\SendMailEvent;
use app\modules\SystemEvent\components\interfaces\EventMailSettingInterface;
use app\modules\SystemEvent\components\interfaces\SystemEventInterface;
use yii\base\Event;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * {user_full_name}, вы назначены исполнителем по заполнению листа оценки {code} {name-dealer-poll}
 * для обследования дилерского центра в период с {dateFrom} по {dateTo} {link-to-take-of-work}
 *
 * Class EmailNotifyToAuthorForDealerPollWithoutResponsible
 */
class EmailNotifyToAuthorForDealerPollWithResponsible extends BaseHandler
{
    public const TYPE = 'email-notify-to-author-for-dealer-poll-with-responsible';
    public const LABEL = 'E-mail пользователю о его назначении исполнителем по конкретному листу оценки';
    public const EVENT_TYPE_ID = 2;

    /**
     * Если дата не больше текущей и ответственный НЕ указан : Уведомление автору
     *
     * @param SendMailEvent|Event $event
     *
     * @return bool|void
     * @throws \Exception
     */
    public function execute(Event $event, SystemEventInterface $systemEvent): bool
    {
        $mailSettings = $systemEvent->getEmailSettings();
        if ($event instanceof SendMailEvent) {
            /** @var DealerPoll $dealerPoll */
            foreach ($event->dataMail as $email => $dealerPolls) {
                foreach ($dealerPolls as $dealerPoll) {
                    $link = Html::a($this->labelForLink, Url::to(['/dealer-poll/default/take-on-work', 'id' => $dealerPoll->id], true));
                    // --------------------
                    $message = strtr(
                        $mailSettings->getTextEmail(),
                        $mailSettings->getPlaceholderAndValue(
                            [$dealerPoll],
                            [
                                '{link}' => Html::a(Yii::t('app', 'Листы оценки'), $link),
                            ]
                        )
                    );
                    $this->sendEmail(
                        $mailSettings,
                        new EmailDto(
                            \Yii::$app->params['no-reply-email'],
                            $mailSettings->getTo($event->sender->dealerPoll),
                            $mailSettings->getSubject(),
                            $message
                        )
                    );
                }
            }
        } else {
            /** @var DealerPoll $dealerPoll */
            $dealerPoll = $event->sender;
            if (($dealerPoll && $dealerPoll->task && $dealerPoll->task->responsiblePerson) && ($email = $dealerPoll->task->responsiblePerson->email)) {
                $this->sendEmailToDealerPollResponsible($dealerPoll, $mailSettings);
            }
        }

        return true;
    }

    /**
     * @param DealerPoll $dealerPoll
     */
    public function sendEmailToDealerPollResponsible(DealerPoll $dealerPoll, EventMailSettingInterface $mailSettings)
    {
        $email = $dealerPoll->task->responsiblePerson->email;
        $link = Html::a(
            Yii::t('app', 'Листы оценки'),
            Url::to(['/dealer-poll/default/take-on-work', 'id' => $dealerPoll->id], true)
        );
        // --------------------
        $message = strtr(
            $mailSettings->getTextEmail(),
            $mailSettings->getPlaceholderAndValue(
                [$dealerPoll],
                [
                    '{link}' => $link,
                ]
            )
        );
        $this->sendEmail(
            $mailSettings,
            new EmailDto(
                \Yii::$app->params['no-reply-email'],
                array_merge(
                    [$email],
                    $mailSettings->getTo($dealerPoll->dealerPoll)
                ),
                $mailSettings->getSubject(),
                $message
            )
        );
    }
}
