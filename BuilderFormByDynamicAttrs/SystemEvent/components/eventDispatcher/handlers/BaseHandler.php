<?php

namespace app\modules\SystemEvent\components\eventDispatcher\handlers;

use app\modules\SystemEvent\components\DTO\EmailDto;
use Yii;
use app\modules\SystemEvent\components\interfaces\{EventMailSettingInterface, HandlerEventAttributeInterface, HandlerEventInterface, NotifyInterface};

/**
 * Class BaseHandler
 */
abstract class BaseHandler implements HandlerEventInterface, HandlerEventAttributeInterface
{
    /**
     * @param string $eventName
     *
     * @return bool
     */
    public function support($eventName): bool
    {
        return $this->getType() === $eventName;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return Yii::t('app', static::LABEL);
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return static::TYPE;
    }

    /**
     * @return int
     */
    public function getTypeEvent(): int
    {
        return static::EVENT_TYPE_ID;
    }

    public function sendEmail(EventMailSettingInterface $mailSettings, EmailDto $emailDto)
    {
        /** @var NotifyInterface $notifier */
        $notifier = \Yii::$container->get(NotifyInterface::class);

        /* Message to Informer */

        return $notifier->send(
            'email',
            $emailDto
        );
    }
}
