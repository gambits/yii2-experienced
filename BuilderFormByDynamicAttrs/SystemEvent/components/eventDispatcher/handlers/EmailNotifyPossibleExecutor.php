<?php

namespace app\modules\SystemEvent\components\eventDispatcher\handlers;

use app\models\events\PossibleExecutorEvent;
use app\modules\SystemEvent\components\DTO\EmailDto;
use app\modules\SystemEvent\components\interfaces\SystemEventInterface;
use Yii;
use yii\base\Event;
use yii\base\InvalidArgumentException;
use yii\helpers\Html;
use yii\helpers\Url;

class EmailNotifyPossibleExecutor extends BaseHandler
{
    public const TYPE = 'email-notify-possible-executor';
    public const LABEL = 'E-mail уведомление сотрудника о назначении исполнителем по заполнению ЛО';
    public const EVENT_TYPE_ID = 2;

    /**
     * @param PossibleExecutorEvent|Event                $event
     * @param SystemEventInterface $systemEvent
     *
     * @return bool
     */
    public function execute(Event $event, SystemEventInterface $systemEvent): bool
    {
        if (!$event instanceof PossibleExecutorEvent) {
            throw new InvalidArgumentException();
        }
        $link = Html::a(Yii::t('app', 'Листы оценки'), Url::to(['/dealer-poll/default/take-on-work', 'id' => $event->task->dealer_poll_id], true));
        $emails = [$event->user->email];
        // --------------------
        $mailSettings = $systemEvent->getEmailSettings();
        $message = strtr(
            $mailSettings->getTextEmail(),
            $mailSettings->getPlaceholderAndValue([$event->sender->dealerPoll, $event->sender], ['{link}' => $link])
        );
        $this->sendEmail(
            $mailSettings,
            new EmailDto(
                \Yii::$app->params['no-reply-email'],
                array_merge($emails, $mailSettings->getTo($event->sender->dealerPoll)),
                $mailSettings->getSubject(),
                $message
            )
        );

        return true;
    }
}
