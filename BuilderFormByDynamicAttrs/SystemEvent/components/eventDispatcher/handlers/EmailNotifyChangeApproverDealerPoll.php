<?php

namespace app\modules\SystemEvent\components\eventDispatcher\handlers;

use app\components\queue\SendEmailJob;
use app\models\Task;
use app\models\User;
use app\modules\DealerPoll\models\DealerPoll;
use app\modules\SystemEvent\components\DTO\EmailDto;
use app\modules\SystemEvent\components\interfaces\NotifyInterface;
use app\modules\SystemEvent\components\interfaces\SystemEventInterface;
use yii\base\Event;
use yii\helpers\Html;
use app\modules\SystemEvent\models\SystemEvent;

/**
 * class EmailNotifyChangeApproverDealerPoll
 */
class EmailNotifyChangeApproverDealerPoll extends BaseHandler
{
    public const TYPE = 'email-notify-dealer-poll-change-approve';
    public const LABEL = 'E-mail уведомление дилера о назначении/изменении согласующего листа оценки';
    public const EVENT_TYPE_ID = 2;

    public $subject;
    public $message;

    public function execute(Event $event, SystemEventInterface $systemEvent): bool
    {
        /** @var Task $task */
        $task = $event->sender;
        /** @var DealerPoll $dealerPoll */
        $dealerPoll = $task->dealerPoll;
        /** @var NotifyInterface $notifier */
        $notifier = \Yii::$container->get(NotifyInterface::class);
        /* Message to Informer */
        $mailSettings = $systemEvent->getEmailSettings();
        $message = strtr(
            $mailSettings->getTextEmail(),
            $mailSettings->getPlaceholderAndValue(
                [$dealerPoll, $task],
                [
                    '{link}' => Html::a(
                        $dealerPoll->name,
                        \Yii::$app->urlManager->createAbsoluteUrl(
                            [
                                '/dealer-poll/default/survey',
                                'id' => $dealerPoll->id,
                            ]
                        ),
                        ['target' => '_blank']
                    ),
                ]
            )
        );

        return $notifier->send(
            'email',
            new EmailDto(
                \Yii::$app->params['no-reply-email'],
                $mailSettings->getTo($dealerPoll),
                $mailSettings->getSubject(),
                $message
            )
        );
    }
}
