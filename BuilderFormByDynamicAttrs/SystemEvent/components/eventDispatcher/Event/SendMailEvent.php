<?php

namespace app\modules\SystemEvent\components\eventDispatcher\Event;

use yii\base\Event;

/**
 * Class SendMailEvent
 */
class SendMailEvent extends Event
{
    public $dataMail;
}
