<?php

namespace app\modules\SystemEvent\components\EventDispatcher\Event;

use app\modules\SystemEvent\components\DTO\FormBuildDataDto;
use yii\base\Event;

/**
 * Class BuildFormEvent
 */
class BuildFormEvent extends Event
{
    /**
     * @var FormBuildDataDto
     */
    public $formBuildData;
}
