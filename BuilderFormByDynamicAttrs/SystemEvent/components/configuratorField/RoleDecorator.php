<?php

namespace app\modules\SystemEvent\components\configuratorField;

use app\helpers\RoleHelper;
use app\modules\SystemEvent\components\DTO\FormBuildDataDto;
use app\modules\SystemEvent\components\interfaces\FormSettingInterface;
use yii\base\InvalidArgumentException;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\widgets\ActiveField;

/**
 * Role field decorator
 * Class RoleDecorator
 *
 * @property ActiveField      $activeField
 * @property FormBuildDataDto $data
 */
class RoleDecorator extends MainFieldDecorator
{
    public const SUPPROT_FIELD_NAME = [
        'role',
    ];

    /**
     * @param string               $property
     * @param FormSettingInterface $formSetting
     *
     * @return ActiveField
     */
    public function configure(string $property, FormSettingInterface $formSetting): ActiveField
    {
        parent::configure($property, $formSetting);
        try {
            $itemsRole = Json::decode($this->data->getModel()->role);
        } catch (InvalidArgumentException $exception) {
            $itemsRole = null;
        }
        $this->data->dataFields['dropdown'][$property] = ArrayHelper::map(RoleHelper::getRoles($itemsRole), 'name', 'description');

        return $this->activeField->label($formSetting->getLabel($property));
    }

    public function support($slug): bool
    {
        return in_array($slug, self::SUPPROT_FIELD_NAME, true);
    }
}
