<?php

namespace app\modules\SystemEvent\components\configuratorField;

use app\modules\SystemEvent\components\DTO\FormBuildDataDto;
use app\modules\SystemEvent\components\interfaces\FormSettingInterface;
use yii\base\InvalidArgumentException;
use yii\helpers\Json;
use yii\widgets\ActiveField;

/**
 * Common field decorator
 * Class FieldDecorator
 *
 * @property ActiveField      $activeField
 * @property FormBuildDataDto $data
 */
class FieldDecorator extends MainFieldDecorator
{
    public function __construct(ActiveField $activeField = null, FormBuildDataDto $data = null)
    {
        parent::__construct($activeField, $data);
    }

    /**
     * @param string               $property
     * @param FormSettingInterface $formSetting
     *
     * @return ActiveField
     */
    public function configure(string $property, FormSettingInterface $formSettings): ActiveField
    {
        parent::configure($property, $formSettings);
        $value = $this->data->getModel()->{$property};
        $this->activeField->inputOptions = array_merge(
            $this->activeField->inputOptions,
            [
                'id' => $property,
                'class' => 'form-control js-workflow',
                'required' => $formSettings->isRequired(),
                'disabled' => $formSettings->isDisabled($property),
            ]
        );
        if ('dropdown' === $formSettings->getFieldType() && $isMultiple = $formSettings->isMultiple()) {
            $this->activeField->inputOptions['multiple'] = $isMultiple;
            if (!is_array($value)) {
                try {
                    $value = Json::decode($value);
                } catch (InvalidArgumentException $exception) {
                    $value = is_array($value) ? implode(', ', $value) : $value;
                }
            }
            $this->activeField->options['selected'] = $this->formingSelected($value);
        } else {
            $this->activeField->inputOptions['value'] = is_array($value) ? implode(',', $value) : $value;
        }

        return $this->activeField->label($formSettings->getLabel($property));
    }

    public function support($slug): bool
    {
        return true;
    }
}
