<?php

namespace app\modules\SystemEvent\components\configuratorField;

use app\helpers\RoleHelper;
use app\modules\SystemEvent\components\DTO\FormBuildDataDto;
use app\modules\SystemEvent\components\interfaces\FormSettingInterface;
use app\modules\SystemEvent\helpers\SystemEventHelper;
use yii\base\InvalidArgumentException;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\widgets\ActiveField;

/**
 * Class HandlerDecorator
 *
 * @property ActiveField      $activeField
 * @property FormBuildDataDto $data
 */
class HandlerDecorator extends MainFieldDecorator
{
    public const SUPPROT_FIELD_NAME = [
        'handler',
    ];

    /**
     * @param string               $property
     * @param FormSettingInterface $formSetting
     *
     * @return ActiveField
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function configure(string $property, FormSettingInterface $formSetting): ActiveField
    {
        parent::configure($property, $formSetting);

        $this->data->dataFields['dropdown'][$property] = SystemEventHelper::getHandlerLabels($this->data->getModel()->type_handle);

        return $this->activeField->label($formSetting->getLabel($property));
    }

    public function support($slug): bool
    {
        return in_array($slug, self::SUPPROT_FIELD_NAME, true);
    }
}
