<?php

namespace app\modules\SystemEvent\components\configuratorField;

use app\modules\SystemEvent\components\DTO\FormBuildDataDto;
use app\modules\SystemEvent\components\interfaces\ConfiguratorFieldInterface;
use app\modules\SystemEvent\components\interfaces\FormSettingInterface;
use yii\widgets\ActiveField;

/**
 * Decorator ActiveForm field
 * Class MainDecorator
 */
abstract class MainFieldDecorator implements ConfiguratorFieldInterface
{
    /**
     * @var ActiveField
     */
    protected $activeField;
    /**
     * @var FormBuildDataDto
     */
    protected $data;
    /**
     * @var string
     */
    protected $fieldTemplate = "{label}\n{input}\n{error}";
    /**
     * @var bool
     */
    protected $enableAjaxValidation = true;
    /**
     * @var bool
     */
    protected $validateOnBlur = true;
    /**
     * @var int
     */
    protected $validationDelay;
    /**
     * @var bool
     */
    protected $validationStateOn = true;
    /**
     * @var array
     */
    protected $partsForTemplate = [];
    /**
     * @var string
     */
    protected $fieldContainerTag = 'div';

    /**
     * MainFieldDecorator constructor.
     *
     * @param ActiveField      $activeField - AcitveForm field
     * @param FormBuildDataDto $data        - for logic decorator
     */
    public function __construct(ActiveField $activeField, FormBuildDataDto $data)
    {
        $this->activeField = $activeField;
        $this->data = $data;
    }

    /**
     * @param string               $property
     * @param FormSettingInterface $formSettings
     *
     * @return ActiveField
     */
    public function configure(string $property, FormSettingInterface $formSettings): ActiveField
    {
        $this->setDefaultOptions();

        return $this->activeField;
    }

    abstract public function support($slug): bool;

    /**
     * @param array $value
     *
     * @return array
     */
    protected function formingSelected($value): array
    {
        if (!$value) {
            return [];
        }
        $selected = [];
        foreach ($value as $item) {
            $selected[$item] = ['selected' => true];
        }

        return $selected;
    }

    /**
     * Default configure
     */
    protected function setDefaultOptions(): void
    {
        $this->data->getForm()->enableAjaxValidation = $this->enableAjaxValidation;
        $this->data->getForm()->options['tag'] = $this->fieldContainerTag;
        $this->activeField->template = $this->fieldTemplate;
        $this->activeField->validateOnBlur = $this->validateOnBlur;
        $this->activeField->parts = $this->partsForTemplate;
        if ($this->validationStateOn) {
            $this->data->getForm()->validationStateOn = $this->validationStateOn;
        }
        if ($this->validationDelay) {
            $this->activeField->validationDelay = $this->validationDelay;
        }
    }
}
