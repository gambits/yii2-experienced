<?php

namespace app\modules\SystemEvent\components;

use app\modules\SystemEvent\components\interfaces\HandlerEventInterface;
use yii\base\Event;
use app\modules\SystemEvent\models\SystemEvent;
use app\modules\SystemEvent\components\interfaces\EventHandleManagerInterface;

/**
 * Class EventHandleManager
 */
class EventHandleManager implements EventHandleManagerInterface
{
    private $handlers = [];

    /**
     * EventHandleManager constructor.
     *
     * @param array|HandlerEventInterface[] $handlers
     */
    public function __construct($handlers)
    {
        $this->handlers = $handlers;
    }

    /**
     * @param Event $event
     *
     * @return int Количество успешно выполненных обработчиков
     */
    public function handle(Event $event)
    {
        $succeeded = 0;
        /** @var SystemEvent $item */
        foreach ($this->handlers as $handler) {
            if (!$handler->support($event->name)) {
                continue;
            }
            foreach (SystemEvent::findAll(['handler' => $event->name, 'active' => 1]) as $systemEvent) {
                $handler->execute($event, $systemEvent);
            }
        }

        return $succeeded;
    }

    public function getHandlers()
    {
        return $this->handlers;
    }
}
