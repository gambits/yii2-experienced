<?php

namespace app\modules\SystemEvent\components\interfaces;

use \app\modules\SystemEvent\components\interfaces\BuildDataInterface;

/**
 * Interface BuildByTypeInterface
 */
interface BuildByTypeInterface
{
    /**
     * @param BuildDataInterface $data
     *
     * @return mixed
     */
    public function build(BuildDataInterface $data);
}
