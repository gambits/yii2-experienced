<?php

namespace app\modules\SystemEvent\components\interfaces;

/**
 * Interface NotifyInterface
 */
interface NotifyInterface
{
    /**
     * @param string $transport
     * @param mixed  $data
     *
     * @return mixed
     */
    public function send($transport, $data);
}
