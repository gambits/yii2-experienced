<?php

namespace app\modules\SystemEvent\components\interfaces;

interface HandlerEventAttributeInterface
{
    /**
     * @return string
     */
    public function getType(): string;

    /**
     * @return string
     */
    public function getLabel(): string;

    /**
     * @return int
     */
    public function getTypeEvent(): int;
}
