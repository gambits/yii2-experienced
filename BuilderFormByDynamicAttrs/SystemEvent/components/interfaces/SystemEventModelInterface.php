<?php

namespace app\modules\SystemEvent\components\interfaces;

use app\modules\SystemEvent\components\DTO\ModelDto;

interface SystemEventModelInterface
{
    /**
     * @param ModelDto $model
     *
     * @return mixed
     */
    public function getEventData(ModelDto $model);
}
