<?php

namespace app\modules\SystemEvent\components\interfaces;

interface HandleChainInterface
{
    /**
     * @return bool
     */
    public function support(): bool;

    /**
     * @return mixed
     */
    public function handle();
}
