<?php

namespace app\modules\SystemEvent\components\interfaces;

use yii\base\Event;

/**
 * Interface EventManagerInterface
 */
interface EventHandleManagerInterface
{
    /**
     * @param Event $event
     *
     * @return mixed
     */
    public function handle(Event $event);

    /**
     * @return array
     */
    public function getHandlers();
}
