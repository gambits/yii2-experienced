<?php

namespace app\modules\SystemEvent\components\interfaces;

use yii\base\Model;

interface PlaceholderResolverInterface
{
    /**
     * @param Model[]|array        $model
     * @param PlaceholderInterface $placeholder
     *
     * @return mixed
     */
    public function resolve($model, PlaceholderInterface $placeholder);
}
