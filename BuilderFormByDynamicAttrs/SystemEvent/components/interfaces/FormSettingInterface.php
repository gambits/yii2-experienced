<?php

namespace app\modules\SystemEvent\components\interfaces;

use yii\base\Model;

/**
 * Interface FormSettingInterface
 */
interface FormSettingInterface
{
    /**
     * @return bool
     */
    public function isMultiple(): bool;

    /**
     * @return bool
     */
    public function isRequired(): bool;

    /**
     * @return bool
     */
    public function isDisabled($property): bool;

    /**
     * @param Model $model
     *
     * @return mixed
     */
    public function getCurrentValue(Model $model);

    /**
     * @return mixed
     */
    public function getDefaultValue();

    /**
     * @return string
     */
    public function getFieldType(): string;

    /**
     * @param string $attribute
     *
     * @return mixed
     */
    public function getLabel($attribute);

    /**
     * @return string|null
     */
    public function getFieldData(): ?string;

    /**
     * @return string|null
     */
    public function getTypeInitData(): ?string;

    /**
     * @param string $property
     * @param string $type
     *
     * @return string|bool|null
     */
    public function getClassWidget($property, $type);
}
