<?php

namespace app\modules\SystemEvent\components\interfaces;

use yii\base\Event;

/**
 * Interface EventManagerInterface
 */
interface EventHandlerInterface
{
    /**
     * @param Event $event
     *
     * @return mixed
     */
    public function handleEvent(Event $event);

    /**
     * @param Event $event
     *
     * @return mixed
     */
    public function dispatchEvent(Event $event);
}
