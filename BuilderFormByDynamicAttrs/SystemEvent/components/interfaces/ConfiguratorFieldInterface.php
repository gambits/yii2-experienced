<?php

namespace app\modules\SystemEvent\components\interfaces;

use yii\base\Model;
use yii\widgets\ActiveField;

/**
 * Interface ConfiguratorFieldInterface
 */
interface ConfiguratorFieldInterface
{
    /**
     * @param $slug
     *
     * @return bool
     */
    public function support($slug): bool;

    /**
     * @param string               $property
     * @param FormSettingInterface $formSettings
     *
     * @return ActiveField
     */
    public function configure(string $property, FormSettingInterface $formSettings): ActiveField;
}
