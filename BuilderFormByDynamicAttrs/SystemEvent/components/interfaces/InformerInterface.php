<?php

namespace app\modules\SystemEvent\components\interfaces;

use app\components\interfaces\CrudOperationInterface;

/**
 * Interface InformerInterface
 */
interface InformerInterface
{
    /**
     * @return mixed
     */
    public function markRead();

    /**
     * @return bool
     */
    public function isRead(): bool;

    /**
     * @return mixed
     */
    public function markUnRead();

    /**
     * @return int
     */
    public function getTotalCount(): int;

    /**
     * @return mixed
     */
    public function getList();

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getItem($id);
}
