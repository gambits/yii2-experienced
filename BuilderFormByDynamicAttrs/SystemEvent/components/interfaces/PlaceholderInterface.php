<?php

namespace app\modules\SystemEvent\components\interfaces;

interface PlaceholderInterface
{
    /**
     * @return string
     */
    public function getPathRelation(): ?string;
}
