<?php

namespace app\modules\SystemEvent\components\interfaces;

use yii\base\Model;

/**
 * Interface EventMailSettingInterface
 */
interface EventMailSettingInterface
{
    /**
     * @param Model[]    $models
     * @param array|null $append
     *
     * @return array
     */
    public function getPlaceholderAndValue(array $models, ?array $append = []): array;

    /**
     * @return string
     */
    public function getTextEmail(): string;

    /**
     * @return string
     */
    public function getSubject(): string;

    /**
     * @param Model|mixed $model
     *
     * @return array
     */
    public function getTo(SystemEventModelInterface $model): array;

    /**
     * @return array
     */
    public function getRoleTo(): array;
}
