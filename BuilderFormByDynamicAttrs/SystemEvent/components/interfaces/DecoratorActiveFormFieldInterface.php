<?php

namespace app\modules\SystemEvent\components\interfaces;

use yii\widgets\ActiveField;

/**
 * Interface DecoratorActiveFormFieldInterface
 */
interface DecoratorActiveFormFieldInterface
{
    /**
     * @param $slug
     *
     * @return bool
     */
    public function support($slug): bool;

    /**
     * @return ActiveField
     */
    public function decorate(): ActiveField;
}
