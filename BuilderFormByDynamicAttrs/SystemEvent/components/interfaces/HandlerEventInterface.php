<?php

namespace app\modules\SystemEvent\components\interfaces;

use yii\base\Event;

/**
 * Interface HandlerEventInterface
 */
interface HandlerEventInterface
{
    /**
     * @param string|mixed $eventName
     *
     * @return bool
     */
    public function support($eventName): bool;

    /**
     * @param Event                $event
     * @param SystemEventInterface $systemEvent
     *
     * @return bool
     */
    public function execute(Event $event, SystemEventInterface $systemEvent): bool;
}
