<?php

namespace app\modules\SystemEvent\components\interfaces;

/**
 * Interface SystemEventInterface
 */
interface SystemEventInterface
{
    public function getEmailSettings(): EventMailSettingInterface;
}
