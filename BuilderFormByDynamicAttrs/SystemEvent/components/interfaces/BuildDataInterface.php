<?php

namespace app\modules\SystemEvent\components\interfaces;

use app\modules\SystemEvent\models\form\SystemEventForm;
use app\modules\SystemEvent\models\SystemEventTypeSetting;
use app\widgets\ActiveForm;
use yii\base\Model;

interface BuildDataInterface
{
    /**
     * @return ActiveForm
     */
    public function getForm(): ActiveForm;

    /**
     * @return SystemEventForm|Model
     */
    public function getModel();

    /**
     * @return SystemEventTypeSetting
     */
    public function getSystemEventTypeSettingItem(): SystemEventTypeSetting;

    /**
     * @return array|null
     */
    public function getDataFields(): ?array;

    /**
     * @return string
     */
    public function getFieldName(): string;

    /**
     * @return FormSettingInterface
     */
    public function getSettingsFrom(): FormSettingInterface;
}
