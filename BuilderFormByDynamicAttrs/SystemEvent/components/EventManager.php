<?php

namespace app\modules\SystemEvent\components;

use Yii;
use app\components\interfaces\CrudOperationInterface;
use app\modules\SystemEvent\models\form\ToggleActiveForm;
use app\modules\SystemEvent\models\form\SystemEventForm;
use app\modules\SystemEvent\models\SystemEvent;
use app\modules\SystemEvent\models\SystemEventMailSetting;
use yii\base\Component;
use yii\db\Transaction;
use yii\web\NotFoundHttpException;
use yii\db\StaleObjectException;

/**
 * Class EventManager
 */
class EventManager extends Component implements CrudOperationInterface
{
    /**
     * @param SystemEventForm $form
     *
     * @return SystemEvent
     */
    public function create($form): SystemEvent
    {
        $transaction = Yii::$app->db->beginTransaction();
        $model = new SystemEvent();
        try {
            $config = [
                'type_id' => $form->type_handle,
                'event_name' => $form->event_name,
                'handler' => $form->handler,
            ];
            if ($model->load($config, '') && $model->save()) {
                $config = $form->attributes;
                unset($config['system_event_active']);
                $modelEmailSettings = new SystemEventMailSetting($config);
                $modelEmailSettings->system_event_id = $model->id;
                $modelEmailSettings->save(false);
                $transaction->commit();
            }
            if ($model->errors || !$model->systemEventMailSettings) {
                $transaction->rollback();
            }
        } catch (\Exception $ex) {
            if ($transaction->isActive) {
                $transaction->rollback();
            }
        }

        return $model;
    }

    /**
     * @param SystemEventForm $form
     *
     * @return SystemEvent
     * @throws NotFoundHttpException
     */
    public function update($form): SystemEvent
    {
        $transaction = Yii::$app->db->beginTransaction();
        $errors = false;
        try {
            $model = $this->loadModel($form->system_event_id);
            $config = [
                'type_id' => (int) $form->type_handle,
                'event_name' => $form->event_name,
                'active' => $form->system_event_active,
                'handler' => $form->handler,
            ];
            if ($model->load($config, '') && $model->save()) {
                $errors = $model->hasErrors();
                /** @var SystemEventMailSetting $modelEmailSettings */
                $modelEmailSettings = $model->systemEventMailSettings;
                if (!$errors && $modelEmailSettings->load($form->attributes, '')) {
                    $modelEmailSettings->system_event_id = $model->id;
                    $modelEmailSettings->save();
                    $errors = $modelEmailSettings->hasErrors();
                }
            }
            if ($errors || !$model->systemEventMailSettings) {
                $this->rollback($transaction);
            } else {
                $transaction->commit();
            }
        } catch (\Exception $ex) {
            $this->rollback($transaction);
        }

        return $model;
    }

    /**
     * @param int $id
     *
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws StaleObjectException
     */
    public function delete($id)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            /** @var SystemEvent $model */
            $model = $this->loadModel($id);
            if ($model->systemEventMailSettings->delete()) {
                $model->delete();
                $transaction->commit();
            }
        } catch (\Exception $ex) {
            $this->rollback($transaction);
        } catch (\Throwable $ex) {
            $this->rollback($transaction);
        }
    }

    /**
     * @param $ids
     *
     * @return mixed
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws \Throwable
     */
    public function toggleActiveItem(ToggleActiveForm $form): SystemEvent
    {
        /** @var SystemEvent $model */
        $model = $this->loadModel($form->id);
        $model->active = $form->active;
        if ($model->update(false, ['active'])) {
            return $model;
        }

        return $model;
    }

    /**
     * @param Transaction $transaction
     */
    private function rollback(Transaction $transaction)
    {
        if ($transaction->isActive) {
            $transaction->rollBack();
        }
    }

    /**
     * @param int $id
     *
     * @return SystemEvent
     * @throws NotFoundHttpException
     */
    public function loadModel($id): SystemEvent
    {
        if (!$model = SystemEvent::findOne($id)) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested model #{id} does not exist.', ['{id}' => $id]));
        }

        return $model;
    }
}
