<?php

namespace app\modules\SystemEvent\components;

use app\modules\SystemEvent\components\DTO\FormBuildDataDto;
use app\modules\SystemEvent\models\SystemEventType;

/**
 * Interface BuildByTypeInterface
 */
interface BuildByTypeInterface
{
    /**
     * @param SystemEventType $type
     *
     * @return mixed
     */
    public function build(FormBuildDataDto $data);
}
