<?php

namespace app\modules\SystemEvent\components;

use app\components\interfaces\EventDispatcherInterface;
use app\modules\SystemEvent\components\DTO\FormBuildDataDto;
use app\modules\SystemEvent\components\eventDispatcher\Event\BuildFormEvent;
use app\modules\SystemEvent\components\interfaces\BuildByTypeInterface;
use app\modules\SystemEvent\components\interfaces\BuildDataInterface;
use app\modules\SystemEvent\components\interfaces\ConfiguratorFieldInterface;
use app\widgets\Select;
use yii\helpers\Json;
use yii\widgets\ActiveField;
use yii\base\InvalidArgumentException;

/**
 * Собирает форму по конфигу
 * Class FormBuilder
 */
class FormBuilder implements BuildByTypeInterface
{
    const TYPE_INIT_DATA_FIELD_CALLABLE = 'callable';
    const TYPE_INIT_DATA_FIELD_AJAX = 'ajax';
    const TYPE_INIT_DATA_FIELD_DEFAULT = 'default';
    public const EVENT_BEFORE_BUILD = 'onBeforeFormBuild';
    /**
     * @var array
     */
    private $configurators;
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;
    /**
     * @var array
     */
    public $defaultSelectClassOptions = [
        'class' => 'form-control',
        'data-style' => 'btn-sm btn-default',
        'data-live-search' => 1,
        'data-size' => 6,
        'encode' => false,
        'prompt' => 'Ничего не выбрано',
    ];

    /**
     * FormBuilder constructor.
     *
     * @param array                    $configurators
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(array $configurators, EventDispatcherInterface $eventDispatcher)
    {
        $this->configurators = $configurators;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param BuildDataInterface|FormBuildDataDto $data
     *
     * @return ActiveField
     * @throws \Exception
     */
    public function build(BuildDataInterface $data): ActiveField
    {
        $this->before($data);
        if (method_exists($data->getModel(), 'beforeBuildForm')) {
            $data->getModel()->beforeBuildForm();
        }
        $method = $this->getMethodActiveForm($data);
        $fieldName = $data->getFieldName();
        $activeFormField = $data->getForm()->field($data->getModel(), $fieldName);
        /** @var ConfiguratorFieldInterface $configurator */
        foreach ($this->configurators as $configurator) {
            if (!is_subclass_of($configurator, ConfiguratorFieldInterface::class)) {
                throw $this->createExceptionWithInvalidConfigurator($fieldName);
            }
            if (!is_object($configurator)) {
                $configurator = new $configurator($activeFormField, $data);
            }
            if (!$configurator->support($fieldName)) {
                continue;
            }
            $configurator->configure(
                $fieldName,
                $data->getSettingsFrom()
            );
        }
        $this->applyMethodWithArgument($activeFormField, $method, $data);

        return $activeFormField;
    }

    /**
     * @param FormBuildDataDto $data
     */
    protected function before(FormBuildDataDto $data)
    {
        $this->eventDispatcher->dispatch(new BuildFormEvent(['formBuildData' => $data]), self::EVENT_BEFORE_BUILD);
    }

    /**
     * @param ActiveField      $activeFormField
     * @param string           $method
     * @param FormBuildDataDto $data
     *
     * @return ActiveField
     * @throws \Exception
     */
    private function applyMethodWithArgument(ActiveField $activeFormField, string $method, FormBuildDataDto $data)
    {
        $firstArgumentMethod = $this->getFirstArgument($data);
        if ($method === 'dropDownList') {
            $firstArgumentMethod = $firstArgumentMethod ?? [];
            if ($widgetClass = $data->getSettingsFrom()->getClassWidget($data->systemEventTypeSettingItem->slug, $method)) {
                $activeFormField->widget(
                    $widgetClass,
                    [
                        'options' => array_merge(
                            $activeFormField->inputOptions,
                            $data->dataFields['dropdown'][$widgetClass][$activeFormField->attribute] ?? $this->defaultSelectClassOptions
                        ),
                        'items' => $firstArgumentMethod,
                    ]
                );
            } else {
                $activeFormField->$method($firstArgumentMethod);
            }
        } else {
            $activeFormField->$method();
        }

        return $activeFormField;
    }

    /**
     * @param FormBuildDataDto $data
     *
     * @return mixed|null
     */
    private function getFirstArgument(FormBuildDataDto $data)
    {
        $firstArgumethMethod = null;
        $fieldType = $data->systemEventTypeSettingItem->systemEventTypeSettgForm->field_type;
        $slug = $data->systemEventTypeSettingItem->slug;
        try {
            $callback = Json::decode($data->getSettingsFrom()->getFieldData());
        } catch (InvalidArgumentException $exception) {
            $callback = null;
        }
        if (isset($data->dataFields[$fieldType]) && array_key_exists($slug, $data->dataFields[$fieldType])) {
            $firstArgumethMethod = $data->dataFields[$fieldType][$slug];
            if (isset($firstArgumethMethod[0]) && isset($firstArgumethMethod[1])) {
                $firstArgumethMethod = call_user_func($firstArgumethMethod);
            }
        } elseif ($callback && $data->getSettingsFrom()->getTypeInitData() === self::TYPE_INIT_DATA_FIELD_CALLABLE) {
            $firstArgumethMethod = call_user_func($callback);
        }

        return $firstArgumethMethod;
    }

    /**
     * @param FormBuildDataDto $data
     *
     * @return string
     */
    private function getMethodActiveForm(FormBuildDataDto $data): string
    {
        $method = 'textInput';
        $typeFiled = $data->systemEventTypeSettingItem->systemEventTypeSettgForm->field_type;
        switch ($typeFiled) {
            case 'textarea':
                $method = 'textarea';
                break;
            case 'dropdown':
                $method = 'dropDownList';
                break;
            case 'checkbox':
                $method = 'checkbox';
                break;
            case 'radio':
                $method = 'radio';
                break;
        }

        return $method;
    }

    /**
     * @return \Exception
     */
    private function createExceptionWithInvalidConfigurator($fieldName)
    {
        return new \Exception(
            \Yii::t(
                'error',
                'Configurator field {field} must by instanceof {class}',
                [
                    '{class}' => ConfiguratorFieldInterface::class,
                    '{field}' => $fieldName,
                ]
            )
        );
    }
}
