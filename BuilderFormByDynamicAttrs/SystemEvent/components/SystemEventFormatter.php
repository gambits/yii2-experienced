<?php

namespace app\modules\SystemEvent\components;

use app\components\Formatter;
use app\models\Task;
use app\models\TaskStage;
use app\models\Workflow;
use app\modules\SystemEvent\models\SystemEvent;
use app\modules\SystemEvent\models\SystemEventMailSetting;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class SystemEventFormatter
 */
class SystemEventFormatter extends Formatter
{
    public function formatSystemEventDescrWithWorkflow(SystemEvent $model)
    {
        $settings = $model->systemEventMailSettings;
        $workflow = $stage = $recipients = '';
        if ($settings->workflow) {
            $workflow = $this->formatWorkflow($settings);
        }
        if ($settings->stage) {
            $stage = $this->formatStageList($settings);
        }
        $recipients = $this->formatRecipients($settings);

        return strtr(
            $model->type->template_format,
            array_merge(
                $settings->attributes,
                [
                    '{stage}' => $stage,
                    '{workflow}' => $workflow,
                    '{recipients}' => $recipients,
                    '{list_email}' => $this->formatListEmail($settings),
                    '{position}' => $recipients,
                ]
            )
        );
    }

    /**
     * @param SystemEventMailSetting $settings
     *
     * @return string
     */
    private function formatWorkflow($settings)
    {
        $works = $settings->workflow;
        if (!is_array($works)) {
            $works = json_decode($settings->workflow, true);
        }

        return implode(
            ', ',
            array_map(
                function (Workflow $item) {
                    return $item->name;
                },
                Workflow::findAll(['id' => $works])
            )
        );
    }

    /**
     * @param SystemEventMailSetting $settings
     *
     * @return string
     */
    public function formatStageList($settings)
    {
        $listStageSlug = $settings->stage;
        if (!is_array($listStageSlug)) {
            $listStageSlug = Json::decode($settings->stage, true);
        }
        if (!$listStageSlug) {
            return '';
        }
        $stageList = array_map(
            function (TaskStage $taskStage) {
                return $taskStage->name;
            },
            TaskStage::findAll(
                [
                    'id' => array_map(
                        function ($it) {
                            $data = explode('_', $it);

                            return $data[1] ?? $data[0];
                        },
                        $listStageSlug
                    ),
                ]
            )
        );

        return implode(', ', $stageList);
    }

    /**
     * @param SystemEventMailSetting $settings
     *
     * @return string
     */
    public function formatRecipients($settings): string
    {
        if (!$settings->recipients) {
            return '';
        }
        if (!is_array($settings->recipients)) {
            return implode(', ', json_decode($settings->recipients, true));
        }

        $data = array_map(function ($item) {
            return Task::getRolesList()[$item];
        }, $settings->recipients);

        return implode(', ', $data);
    }

    /**
     * @param SystemEventMailSetting $settings
     *
     * @return string
     */
    public function formatListEmail($settings): string
    {
        if (!$settings->list_email) {
            return '';
        }
        if (!is_array($settings->list_email)) {
            return implode(', ', json_decode($settings->list_email, true));
        }

        return implode(', ', $settings->list_email);
    }
}
