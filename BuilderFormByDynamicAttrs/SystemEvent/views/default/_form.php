<?php

use yii\web\View;
use yii\helpers\Html;
use app\widgets\Select;
use yii\bootstrap\Modal;
use yii\bootstrap\Alert;
use yii\widgets\ActiveForm;
use app\modules\SystemEvent\models\SystemEvent;
use app\modules\SystemEvent\components\FormBuilder;
use app\modules\SystemEvent\models\SystemEventType;
use app\modules\SystemEvent\helpers\SystemEventHelper;
use app\modules\SystemEvent\models\form\SystemEventForm;

/**
 * @var View            $this
 * @var SystemEvent     $model
 * @var ActiveForm      $form
 * @var FormBuilder     $formBuilder
 * @var SystemEventType $type
 * @var SystemEventForm $modelForm
 */
?>
<?php Modal::begin(
    [
        'closeButton' => false,
        'options' => [
            'class' => 'fade modal',
        ],
        'header' => '<div style="float:left;margin-right: 15px;">' . Yii::t(
            'app',
            $model->isNewRecord ? 'Добавление события\обработчика' : 'Обновление события\обработчика'
        ) . '</div>' . (!$model->isNewRecord ? '<div id="js-checkbox-toggle"></div>' : ''),
        'footer' => '<div class="row">
                        <div class="col-lg-12"></div>
                        <div class="col-lg-12">' .
            Html::button(
                Yii::t('app', 'Закрыть'),
                [
                    'style' => 'margin-right:15px',
                    'class' => 'btn btn-outline btn-outline--green',
                    'data-dismiss' => 'modal',
                ]
            ) .
            Html::button(
                $model->isNewRecord ? Yii::t('app', 'Добавить') : Yii::t('app', 'Сохранить'),
                ['class' => 'btn btn-green js-submit', 'disabled' => $model->isNewRecord]
            )
            . '</div></div>',
    ]
); ?>

<?= Html::errorSummary($type, ['class' => 'alert alert-danger']) ?>
<?php $form = ActiveForm::begin(['options' => ['autocomplete' => 'off']]); ?>
<div class="row">
    <div class="col-md-24">
        <?php if ($model->isNewRecord): ?>
            <?= $form->field($type, 'id')->widget(
                Select::class,
                [
                    'options' => [
                        'class' => 'form-control js-handler',
                        'data-style' => 'btn-sm btn-default',
                        'prompt' => '—',
                    ],
                    'items' => SystemEventHelper::getListSystemEventType(),
                ]
            )->label('Выберите тип обработчика события') ?>
        <?php endif; ?>
        <div class="flash-alerts" style="display:none;background:#fff;width:100%;position:absolute;top:0;left:0;z-index:9999;padding-top: 10px;">
            <div style="width: 100%;" class="alert alert-dismissible attention">
                <div id="message"></div>
            </div>
        </div>
        <input type="hidden" name="SystemEventForm[type_handle]" id="js-type-event" value="<?= $type->id ?>">
        <div class="js-handler-params">
            <?php if (!$model->isNewRecord): ?>
                <input type="hidden" name="SystemEventForm[system_event_id]" id="systemEventId" value="<?= $model->id ?>">
                <input type="hidden" name="SystemEventForm[system_event_active]" id="systemEventActive" value="<?= $model->active ?>">
                <?= $this->render('_form_builded_by_type', ['systemEvent' => $model, 'model' => $modelForm, 'formBuilder' => $formBuilder, 'type' => $type]) ?>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
<?php Modal::end() ?>
