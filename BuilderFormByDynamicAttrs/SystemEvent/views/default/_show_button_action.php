<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;

/**
 * @var array $actions
 */
?>

<?php if ($actions): ?>
    <div class="btn-group" role="group">
        <button id="btn-action-system-event" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                aria-expanded="false" style="width: 190px">
            <?= Yii::t('app', 'Действия') ?>
        </button>
        <div class="dropdown-menu btn-batch-action" aria-labelledby="btn-action-system-event" style="width: 190px">
            <?php foreach ($actions as $action): ?>
                <?= Html::a($action['label'], $action['url'], $action['options']) ?>
            <?php endforeach; ?>
        </div>
    </div>

    <?php Modal::begin(
        [
            'size' => 'modal-md',
            'bodyOptions' => [
                'style' => 'padding:20px',
                'class' => 'modal-body',
            ],
            'options' => [
                'id' => 'modal-change-stage-syst-event',
            ],
            'header' => '<h5 class="modal-title">' . Yii::t('app', '') . '</h5>',
            'footer' =>
                Html::button(Yii::t('app', 'Подтвердить'), ['class' => 'btn btn-green', 'id' => 'js-approve-action'])
                . Html::button(
                    Yii::t('app', 'Отменить'),
                    ['class' => 'btn btn-outline btn-outline--green', 'data-dismiss' => 'modal', 'id' => 'js-dismiss-modal']
                ),
        ]
    ); ?>
    <?php Modal::end(); ?>
<?php endif; ?>
