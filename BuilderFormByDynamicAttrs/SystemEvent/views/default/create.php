<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\SystemEvent\models\SystemEvent */

$this->title = Yii::t('app', 'Create System Event');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'System Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-event-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
