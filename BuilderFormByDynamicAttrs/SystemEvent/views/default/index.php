<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\User;
use app\modules\SystemEvent\models\SystemEvent;

/* @var $this yii\web\View */
/* @var $actions array */
/* @var $searchModel app\modules\SystemEvent\models\search\SystemEventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('app', 'System Events');
$this->params['breadcrumbs'][] = $this->title;
$this->registerJs("RDS.EventList();");
?>

<div class="content--header">
    <h1 class="title"><?= $this->title ?></h1>
    <div class="content--header--toolbar">
        <?php if (Yii::$app->user->can(User::SYSTEM_EVENT_CREATE)): ?>
            <?= Html::a(Yii::t('app', 'Добавить'), ['create'], ['class' => 'btn btn-green js-show-modal']) ?>
        <?php endif ?>
        <?= $this->render('_show_button_action', ['actions' => $actions]) ?>
        <?= Html::a(Yii::t('app', 'Фильтр'), '#', ['class' => 'btn btn-outline btn-outline--green', 'id' => 'js-filter-toggle']) ?>
    </div>
</div>

<?= $this->render('_search', ['model' => $searchModel]) ?>

<?= GridView::widget(
    [
        'tableOptions' => ['class' => 'table table-striped'],
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            return ['class' => 'se-item', 'data-id' => $model->id];
        },
        'columns' => [
            'id',
            [
                'attribute' => 'type.title',
                'label' => Yii::t('app', 'Тип обработчика'),
            ],
            [
                'attribute' => 'event_name',
                'label' => Yii::t('app', 'Событие \ Обработчик'),
            ],
            [
                'attribute' => 'type.description',
                'value' => function (SystemEvent $model) {
                    return Yii::$app->systemEventFormatter->formatSystemEventDescrWithWorkflow($model);
                },
            ],
            [
                'attribute' => 'systemEventMailSettings.text_for_informer',
                'format' => 'raw',
                'value' => function (SystemEvent $model) {
                    return Html::activeCheckbox(
                        $model->systemEventMailSettings,
                        'text_for_informer',
                        ['disabled' => true, 'checked' => (bool) $model->systemEventMailSettings->text_for_informer, 'label' => false]
                    );
                },
            ],
            [
                'attribute' => 'active',
                'label' => Yii::t('app', 'Вкл.\Выкл'),
                'format' => 'raw',
                'value' => function (SystemEvent $model) {
                    return Html::activeCheckbox(
                        $model,
                        'active',
                        ['disabled' => true, 'checked' => (bool) $model->active, 'label' => false]
                    );
                },
            ],
            [
                'header' => Yii::t('app', 'Действия'),
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function ($url) {
                        return Html::a(
                            Yii::t('app', '<span class="svg--icon svg--edit" data-grunticon-embed></span>'),
                            $url,
                            [
                                'title' => Yii::t('app', 'Редактировать'),
                                'class' => 'btn-icon js-show-modal',
                            ]
                        );
                    },
                    'delete' => function ($url) {
                        return Html::a(
                            Yii::t('app', '<span class="svg--icon svg--delete" data-grunticon-embed></span>'),
                            $url,
                            [
                                'title' => Yii::t('app', 'Удалить'),
                                'class' => 'btn-icon js-delete',
                                'data-method' => 'post'
                            ]
                        );
                    },
                ],
            ],
        ],
    ]
); ?>
