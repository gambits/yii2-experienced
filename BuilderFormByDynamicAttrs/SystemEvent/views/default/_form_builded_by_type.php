<?php

use app\widgets\ActiveForm;
use app\modules\SystemEvent\models\{
    SystemEvent,
    SystemEventModelPlaceholder,
    SystemEventType,
    SystemEventTypeSetting,
};
use app\modules\SystemEvent\models\form\SystemEventForm;
use app\modules\SystemEvent\components\FormBuilder;
use app\modules\SystemEvent\components\DTO\FormBuildDataDto;
use app\models\TaskStage;
use yii\helpers\Url;
use yii\bootstrap\Html;

/**
 * @var SystemEventType        $type
 * @var SystemEventTypeSetting $systemEventTypeSettingItem
 * @var FormBuilder            $formBuilder
 * @var SystemEventForm        $model
 * @var SystemEvent            $systemEvent
 */
$form = ActiveForm::begin(
    [
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to('/default/validation-form-build'),
    ]
);
$groupFieldTab = [];
?>
<?= $systemEvent->isNewRecord ? '<hr>' : '' ?>
<?php
foreach ($type->systemEventTypeSettings as $systemEventTypeSettingItem) {
    $builderDto = new FormBuildDataDto($form, $model, $systemEventTypeSettingItem);
    if (!in_array($systemEventTypeSettingItem->slug, ['list_email', 'subject_mail', 'text_mail', 'text_for_informer'])) {
        if ($systemEventTypeSettingItem->slug === 'stage') {
            if (!$systemEvent->isNewRecord) {
                $builderDto->dataFields['dropdown']['stage'] = TaskStage::getStageList($systemEvent->systemEventMailSettings->workflow);
                echo $formBuilder->build($builderDto);
            } else {
                echo $formBuilder->build($builderDto);
            }
        } else {
            echo $formBuilder->build($builderDto);
        }
    } elseif ($systemEventTypeSettingItem->slug === 'text_for_informer') {
        $groupFieldTab['informer'][] = $formBuilder->build($builderDto);
    } else {
        $groupFieldTab['email'][$systemEventTypeSettingItem->slug] = $formBuilder->build($builderDto);
    }
}
?>

<nav id="systemEventNav" class="nav nav-pills nav-fill" role="tablist">
    <a class="nav-item nav-link active" id="pills-email-tab" href="#pills-email" aria-controls="pills-email"
       role="tab" data-toggle="pill" aria-selected="true">
        <?= Yii::t('app', 'Email message') ?>
    </a>
    <a class="nav-item nav-link" id="pills-informer-tab" href="#pills-informer" aria-controls="pills-design" role="tab"
       data-toggle="pill" aria-selected="false">
        <?= Yii::t('app', 'Message for informer') ?>
    </a>
</nav>

<div class="tab-content">
    <div role="tabpanel" aria-labelledby="pills-email-tab" class="tab-pane fade show active" id="pills-email">
        <?= $groupFieldTab['email']['subject_mail'] ?>
        <?= $groupFieldTab['email']['list_email'] ?>
        <?= $groupFieldTab['email']['text_mail'] ?>
    </div>
    <div role="tabpanel" aria-labelledby="pills-informer-tab" class="tab-pane fade" id="pills-informer">
        <?php foreach ($groupFieldTab['informer'] as $informerField): ?>
            <?= $informerField ?>
        <?php endforeach; ?>
    </div>
</div>
<?php ActiveForm::end() ?>
<div class="row">
    <div class="col-lg-24">
        <div class="placeholders">
            <?php if ($entities = $type->placeholders): ?>
                <?= Yii::t('app', 'В текст сообщения можно добавить переменные') . ': ' . implode(
                    ', ',
                    array_map(
                        function (SystemEventModelPlaceholder $item) {
                            return Html::tag(
                                'span',
                                $item->placeholder,
                                [
                                    'title' => $item->description,
                                    'class' => 'badge',
                                ]
                            );
                        },
                        $entities
                    )
                ) ?>
            <?php endif; ?>
        </div>
    </div>
</div>
<br/>
