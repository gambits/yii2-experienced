<?php

use app\modules\SystemEvent\models\search\SystemEventSearch;
use yii\helpers\Html;
use app\widgets\Select;
use yii\widgets\ActiveForm;
use app\modules\SystemEvent\helpers\SystemEventHelper;

/**
 * @var yii\web\View           $this
 * @var SystemEventSearch      $model
 * @var yii\widgets\ActiveForm $form
 */
?>
<div id="js-filter-form" style="display: <?= $model->hasErrors() ? 'block' : 'none' ?>;">
    <?php $form = ActiveForm::begin(
        [
            'action' => ['index'],
            'method' => 'get',
            'options' => ['autocomplete' => 'off'],
            'enableClientValidation' => true,
            'enableAjaxValidation' => true,
        ]
    ) ?>
    <section class="content--filters">
        <h2 class="title"><?= Yii::t('app', 'Фильтр') ?></h2>
        <div class="row">
            <div class="col-sm-12">
                <?= $form->field($model, 'typeId')->widget(
                    Select::class,
                    [
                        'options' => [
                            'class' => 'form-control input-sm',
                            'data-style' => 'btn-sm btn-default',
                            'prompt' => Yii::t('app', 'Не выбрано'),
                        ],
                        'items' => SystemEventHelper::getListSystemEventType(),
                    ]
                )->label(Yii::t('app', 'Тип обработчика')) ?>
            </div>
            <div class="col-sm-12">
                <?= $form->field($model, 'active')->dropDownList(SystemEventHelper::getDropdownActiveEvent())
                    ->label(Yii::t('app', 'Статус'))  ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="" class="control-label">&nbsp;</label>
                    <div class="content--filters--action">
                        <?= Html::submitButton(Yii::t('app', 'Показать'), ['class' => 'btn btn-green']) ?>
                        <?= Html::a(Yii::t('app', 'Сброс'), ['index'], ['class' => 'btn btn-default']) ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php ActiveForm::end(); ?>
</div>
