<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\SystemEvent\models\SystemEventInformer */

$this->title = Yii::t('app', 'Create System Event Informer');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'System Event Informers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-event-informer-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
