<?php

namespace app\modules\SystemEvent;

use yii\base\Event;
use app\components\event\EventDispatcher;
use app\components\interfaces\EventDispatcherInterface;
use app\modules\SystemEvent\components\interfaces\EventHandlerInterface;

/**
 * Class EventHandler
 */
class EventHandler implements EventHandlerInterface
{
    /**
     * @var EventDispatcherInterface | EventDispatcher
     */
    private $eventDispatcher;
    /**
     * @var array
     */
    private $handlers = [];

    /**
     * EventHandler constructor.
     *
     * @param EventDispatcherInterface $eventDispatcher
     * @param array                    $handlers
     */
    public function __construct(EventDispatcherInterface $eventDispatcher, array $handlers)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->handlers = $handlers;
    }

    /**
     * @param Event $event
     *
     * @return mixed|void
     */
    public function handleEvent(Event $event)
    {
    }

    /**
     * @param Event $event
     *
     * @return mixed|void
     */
    public function dispatchEvent(Event $event)
    {
    }
}
