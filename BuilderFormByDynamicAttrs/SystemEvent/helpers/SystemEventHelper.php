<?php

namespace app\modules\SystemEvent\helpers;

use app\models\User;
use app\modules\SystemEvent\components\interfaces\EventHandleManagerInterface;
use app\modules\SystemEvent\components\interfaces\HandlerEventInterface;
use Yii;
use app\modules\SystemEvent\models\SystemEventType;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\User as WebUser;

/**
 * Class SystemEventHelper
 */
class SystemEventHelper
{
    /**
     * Return array list System event type
     * @return array
     */
    public static function getListSystemEventType()
    {
        return ArrayHelper::map(SystemEventType::find()->all(), 'id', 'title');
    }

    /**
     * Return array list System event type
     * @return array
     */
    public static function getDropdownActiveEvent()
    {
        return [
            '' => Yii::t('app', 'Все'),
            0 => Yii::t('app', 'Выкл'),
            1 => Yii::t('app', 'Вкл'),
        ];
    }

    /**
     * @param WebUser|null $user
     *
     * @return array
     */
    public static function getAllowBatchAction(?WebUser $user = null): array
    {
        if (!$user) {
            $user = Yii::$app->user;
        }
        $actionList = [
            'enabled' => [
                'permission' => User::SYSTEM_EVENT_DISPATCHER_EDIT,
                'label' => Yii::t('app', 'Включить обработку'),
            ],
            'disabled' => [
                'permission' => User::SYSTEM_EVENT_DISPATCHER_EDIT,
                'label' => Yii::t('app', 'Выключить обработку'),
            ],
        ];
        $actions = [];
        foreach ($actionList as $action => $data) {
            if ($user->can($data['permission'])) {
                $actions[] = [
                    'options' => [
                        'id' => 'js-se-' . $action,
                        'class' => 'es-action-batch',
                        'style' => 'display:inline:block;font-size:12px;padding:5px 10px',
                        'data-action' => $action,
                    ],
                    'url' => Url::to(
                        [
                            '/systemEvent/default/batch-operation',
                            'action' => $action,
                        ]
                    ),
                    'label' => \Yii::t('app', $data['label']),
                ];
            }
        }

        return $actions;
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public static function getHandlerLabels($type = null)
    {
        /** @var EventHandleManagerInterface $handlerEvent */
        $handlerEvent = \Yii::$container->get(EventHandleManagerInterface::class);
        $items = [];
        $handlers = $handlerEvent->getHandlers();
        if ($type) {
            $handlers = array_filter(
                $handlerEvent->getHandlers(),
                function ($item) use ($type) {
                    return $item->getTypeEvent() == $type;
                }
            );
        }
        /** @var HandlerEventInterface $handler */
        foreach ($handlers as $handler) {
            $items[$handler->getType()] = $handler->getLabel();
        }

        return $items;
    }
}
