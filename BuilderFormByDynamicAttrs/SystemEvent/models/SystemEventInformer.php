<?php

namespace app\modules\SystemEvent\models;

use Yii;

/**
 * This is the model class for table "{{%system_event_informer}}".
 *
 * @property int $id
 * @property int|null $user_id User
 * @property int|null $type_id Type System Event
 * @property string $message Message
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 *
 * @property SystemEventType $type
 */
class SystemEventInformer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%system_event_informer}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'type_id', 'created_by', 'updated_by'], 'integer'],
            [['message'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['message'], 'string', 'max' => 1000],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => SystemEventType::className(), 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User'),
            'type_id' => Yii::t('app', 'Type System Event'),
            'message' => Yii::t('app', 'Message'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(SystemEventType::className(), ['id' => 'type_id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\SystemEvent\models\query\SystemEventInformerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\modules\SystemEvent\models\query\SystemEventInformerQuery(get_called_class());
    }
}
