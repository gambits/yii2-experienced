<?php

namespace app\modules\SystemEvent\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\SystemEvent\models\SystemEvent]].
 *
 * @see \app\modules\SystemEvent\models\SystemEvent
 */
class SystemEventQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere('active=1');
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\SystemEvent\models\SystemEvent[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\SystemEvent\models\SystemEvent|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
