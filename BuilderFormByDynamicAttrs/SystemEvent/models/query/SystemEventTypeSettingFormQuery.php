<?php

namespace app\modules\SystemEvent\models\query;

use app\modules\SystemEvent\models\SystemEventTypeSettingForm;

/**
 * This is the ActiveQuery class for [[SystemEventTypeSettingForm]].
 *
 * @see SystemEventTypeSettingForm
 */
class SystemEventTypeSettingFormQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return SystemEventTypeSettingForm[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return SystemEventTypeSettingForm|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
