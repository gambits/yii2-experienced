<?php

namespace app\modules\SystemEvent\models\query;

use app\modules\SystemEvent\models\SystemEventTypeSetting;

/**
 * This is the ActiveQuery class for [[SystemEventTypeSetting]].
 *
 * @see SystemEventTypeSetting
 */
class SystemEventTypeSettingQuery extends \yii\db\ActiveQuery
{
    public function orderBySort()
    {
        return $this->addOrderBy('sort');
    }

    /**
     * {@inheritdoc}
     * @return SystemEventTypeSetting[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return SystemEventTypeSetting|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
