<?php

namespace app\modules\SystemEvent\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\SystemEvent\models\SystemEventModelPlaceholder]].
 *
 * @see \app\modules\SystemEvent\models\SystemEventModelPlaceholder
 */
class SystemEventModelPlaceholderQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\modules\SystemEvent\models\SystemEventModelPlaceholder[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\SystemEvent\models\SystemEventModelPlaceholder|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
