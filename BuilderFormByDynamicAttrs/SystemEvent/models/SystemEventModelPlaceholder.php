<?php

namespace app\modules\SystemEvent\models;

use Yii;
use app\modules\SystemEvent\components\interfaces\PlaceholderInterface;
use app\modules\SystemEvent\models\query\SystemEventModelPlaceholderQuery;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%system_evt_model_placeholder}}".
 *
 * @property int               $id
 * @property string            $placeholder     Placeholder
 * @property string            $entity          Model Class Name
 * @property string            $relation_value  Relation Model Value
 * @property string|null       $description     Description
 * @property string            $created_at      Created At
 * @property string            $updated_at      Updated At
 * @property int|null          $created_by      Created By
 * @property int|null          $updated_by      Updated By
 *
 * @property SystemEventType[] $systemEventTypes
 */
class SystemEventModelPlaceholder extends ActiveRecord implements PlaceholderInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%system_evt_model_placeholder}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'entity',
                    'placeholder',
                ],
                'required',
            ],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'safe'],
            [['placeholder', 'entity'], 'string', 'max' => 255],
            [['relation_value'], 'string', 'max' => 100],
            [['description'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'placeholder' => Yii::t('app', 'Placeholder'),
            'entity' => Yii::t('app', 'Model Class Name'),
            'relation_value' => Yii::t('app', 'Relation Model Value'),
            'description' => Yii::t('app', 'Description'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    public function behaviors(): array
    {
        return [
            TimestampBehavior::class => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::class => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getSystemEventTypes()
    {
        return $this->hasMany(SystemEventType::class, ['model_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return SystemEventModelPlaceholderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SystemEventModelPlaceholderQuery(get_called_class());
    }

    /**
     * @return string
     */
    public function getPathRelation(): ?string
    {
        return $this->relation_value;
    }
}
