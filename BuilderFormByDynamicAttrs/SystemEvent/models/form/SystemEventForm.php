<?php

namespace app\modules\SystemEvent\models\form;

use yii\base\Model;
use yii\helpers\Json;

/**
 * Class SystemEventForm
 */
class SystemEventForm extends Model
{
    public $id;
    public $system_event_id;
    public $system_event_active;
    public $type_handle;
    public $event_name;
    public $workflow;
    public $stage;
    public $list_email;
    public $subject_mail;
    public $text_mail;
    public $text_for_informer;
    public $is_send_notify_before_change;
    public $count_days_to_blocked = 0;
    public $recipients;
    public $personal_user_by_workflow;
    public $role = 0;
    public $handler;

    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['event_name', 'type_handle', 'handler'], 'required'],
            [
                [
                    'event_name',
                    'subject_mail',
                    'text_mail',
                    'text_mail',
                    'text_for_informer',
                    'handler',
                ],
                'string',
            ],
            [['is_send_notify_before_change'], 'boolean'],
            [['type_handle', 'count_days_to_blocked'], 'integer'],
            [['type_handle', 'count_days_to_blocked'], 'integer'],
            [
                [
                    'id',
                    'system_event_id',
                    'system_event_active',
                    'type_handle',
                    'event_name',
                    'workflow',
                    'stage',
                    'recipients',
                    'list_email',
                    'subject_mail',
                    'text_mail',
                    'text_for_informer',
                    'personal_user_by_workflow',
                    'is_send_notify_before_change',
                    'count_days_to_blocked',
                    'role',
                    'handler',
                ],
                'safe',
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('app', 'ID'),
            'handler' => \Yii::t('app', 'Обработчик'),
            'system_event_id' => \Yii::t('app', 'Системное событие'),
            'system_event_active' => \Yii::t('app', 'Выкл \ Вкл.'),
            'type_handle' => \Yii::t('app', 'Тип обработчика'),
            'event_name' => \Yii::t('app', 'Событие \ Обработчик'),
            'workflow' => \Yii::t('app', 'WorkFlow'),
            'stage' => \Yii::t('app', 'Этапы'),
            'recipients' => \Yii::t('app', 'Адресаты'),
            'list_email' => \Yii::t('app', 'Список E-mail'),
            'subject_mail' => \Yii::t('app', 'Тема письма'),
            'text_mail' => \Yii::t('app', 'Текст письма'),
            'text_for_informer' => \Yii::t('app', 'Текст сообщения для информера событий'),
            'personal_user_by_workflow' => \Yii::t('app', 'Действующее лицо'),
            'is_send_notify_before_change' => \Yii::t('app', 'Направлять уведомление пользователю до изменения'),
            'count_days_to_blocked' => \Yii::t('app', 'Количество дней до блокировки'),
            'role' => \Yii::t('app', 'Роли пользователя'),
        ];
    }

    public function beforeValidate()
    {
        if (!is_array($this->list_email) && is_string($this->list_email)) {
            $this->list_email = explode(',', $this->list_email);
        }
        if (!is_array($this->stage) && is_string($this->stage)) {
            $this->stage = explode(',', $this->stage);
        }
        if (!is_array($this->recipients) && is_string($this->recipients)) {
            $this->recipients = explode(',', $this->recipients);
        }

        return parent::beforeValidate();
    }

    /**
     * Called from app\modules\SystemEvent\components\FormBuilder
     */
    public function beforeBuildForm()
    {
        $this->list_email = is_array($this->list_email) ? $this->list_email : Json::decode($this->list_email);
        $this->stage = is_array($this->stage) ? $this->stage : Json::decode($this->stage);
        $this->recipients = is_array($this->recipients) ? $this->recipients : Json::decode($this->recipients);
        $this->role = is_array($this->role) ? $this->role : Json::decode($this->role);
    }
}
