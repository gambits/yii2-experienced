<?php

namespace app\modules\SystemEvent\models\form;

use yii\base\Model;

/**
 * class ToggleActiveForm
 */
class ToggleActiveForm extends Model
{
    /**
     * @var int|null
     */
    public $id;
    /**
     * @var bool|null
     */
    public $active;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
            ['active', 'boolean'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('app', 'ID'),
            'active' => \Yii::t('app', 'Active'),
        ];
    }
}
