<?php

namespace app\modules\SystemEvent\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\SystemEvent\models\SystemEventTypeSettingForm;

/**
 * SystemEventTypeSettingFormSearch represents the model behind the search form of `app\modules\SystemEvent\models\SystemEventTypeSettingForm`.
 */
class SystemEventTypeSettingFormSearch extends SystemEventTypeSettingForm
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'setting_id', 'is_required', 'created_by', 'updated_by'], 'integer'],
            [['field_type', 'is_multiple', 'default_value', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SystemEventTypeSettingForm::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'setting_id' => $this->setting_id,
            'is_required' => $this->is_required,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'field_type', $this->field_type])
            ->andFilterWhere(['like', 'is_multiple', $this->is_multiple])
            ->andFilterWhere(['like', 'default_value', $this->default_value]);

        return $dataProvider;
    }
}
