<?php

namespace app\modules\SystemEvent\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\SystemEvent\models\SystemEventModelPlaceholder;

/**
 * SystemEventModelPlaceholderSearch represents the model behind the search form of `app\modules\SystemEvent\models\SystemEventModelPlaceholder`.
 */
class SystemEventModelPlaceholderSearch extends SystemEventModelPlaceholder
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by'], 'integer'],
            [['entity', 'placeholder', 'description', 'value', 'relation_value', 'type_init_value', 'handler', 'method', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SystemEventModelPlaceholder::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'entity', $this->entity])
            ->andFilterWhere(['like', 'placeholder', $this->placeholder])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'value', $this->value])
            ->andFilterWhere(['like', 'relation_value', $this->relation_value])
            ->andFilterWhere(['like', 'type_init_value', $this->type_init_value])
            ->andFilterWhere(['like', 'handler', $this->handler])
            ->andFilterWhere(['like', 'method', $this->method]);

        return $dataProvider;
    }
}
