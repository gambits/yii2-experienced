<?php

namespace app\modules\SystemEvent\models\search;

use app\models\User;
use app\modules\SystemEvent\models\SystemEvent;
use app\modules\SystemEvent\models\SystemEventType;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SystemEvent represents the model behind the search form of `app\modules\SystemEvent\models\SystemEvent`.
 */
class SystemEventSearch extends Model
{
    public $id;
    public $userId;
    public $typeId;
    public $handler;
    public $eventName;
    public $createdAt;
    public $updatedAt;
    public $active;
    public $params;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'typeId',
                    'userId',
                    'eventName',
                    'createdAt',
                    'updatedAt',
                    'handler',
                    'active',
                    'params',
                ],
                'safe',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SystemEvent::find();
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
            ]
        );
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andFilterWhere(
            [
                'id' => $this->id,
                'type_id' => $this->typeId,
                'user_id' => $this->userId,
                'event_name' => $this->eventName,
                'created_at' => $this->createdAt,
                'updated_at' => $this->updatedAt,
                'handler' => $this->handler,
                'active' => $this->active,
            ]
        );
        $query->andFilterWhere(['like', 'params', $this->params]);

        return $dataProvider;
    }
}
