<?php

namespace app\modules\SystemEvent\models;

use app\models\User;
use app\modules\SystemEvent\components\interfaces\EventMailSettingInterface;
use app\modules\SystemEvent\components\interfaces\SystemEventInterface;
use app\modules\SystemEvent\models\query\SystemEventQuery;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%system_event}}".
 *
 * @property int                    $id
 * @property string                 $event_name Event Name
 * @property int                    $type_id    Type Id
 * @property int                    $active     Active
 * @property string                 $handler
 * @property string|null            $params
 * @property int|null               $user_id
 * @property string|null            $created_at
 * @property string|null            $updated_at
 *
 * @property array                  $eventTypes
 * @property User                   $user
 * @property SystemEventType        $type
 * @property SystemEventMailSetting $systemEventMailSettings
 */
class SystemEvent extends ActiveRecord implements SystemEventInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%system_event}}';
    }

    public function behaviors(): array
    {
        return [
            TimestampBehavior::class => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::class => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => 'user_id',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_name', 'type_id', 'handler'], 'required'],
            [['type_id', 'user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['event_name'], 'string', 'max' => 300],
            [['handler'], 'string', 'max' => 255],
            [['params'], 'string', 'max' => 1000],
            [['active'], 'boolean'],
            [['active'], 'default', 'value' => true],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => SystemEventType::class, 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'event_name' => Yii::t('app', 'Event Name'),
            'type_id' => Yii::t('app', 'Type Id'),
            'active' => \Yii::t('app', 'Active'),
            'handler' => Yii::t('app', 'Handler'),
            'params' => Yii::t('app', 'Params'),
            'user_id' => Yii::t('app', 'User ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return array
     */
    public function getEventTypes()
    {
        return [
            'email' => 'email',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getSystemEventMailSettings()
    {
        return $this->hasOne(SystemEventMailSetting::class, ['system_event_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(SystemEventType::class, ['id' => 'type_id']);
    }

    /**
     * {@inheritdoc}
     * @return SystemEventQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SystemEventQuery(get_called_class());
    }

    /** implements SystemEventInterface methods - Facade for system event email */
    /**
     * @return EventMailSettingInterface
     */
    public function getEmailSettings(): EventMailSettingInterface
    {
        return $this->systemEventMailSettings;
    }
    /** implements methods END */
}
