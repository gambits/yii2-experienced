<?php

namespace app\modules\EventDispatcher\models\event;

use yii\base\Event;

/**
 * Class SendMailEvent
 */
class SendMailEvent extends Event
{
    public $dataMail;
}
