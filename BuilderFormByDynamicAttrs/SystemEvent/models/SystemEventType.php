<?php

namespace app\modules\SystemEvent\models;

use app\modules\SystemEvent\models\query\SystemEventTypeQuery;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%system_event_type}}".
 *
 * @property int                         $id
 * @property string|null                 $entity           Model Class Name - json
 * @property string                      $type_event       Type
 * @property string                      $title            Title
 * @property string|null                 $description      Description
 * @property string|null                 $template_format  Template format description
 * @property int|null                    $active           Active
 * @property string                      $created_at       Created At
 * @property string                      $updated_at       Updated At
 * @property int|null                    $created_by       Created By
 * @property int|null                    $updated_by       Updated By
 *
 * @property SystemEvent[]               $systemEvents
 * @property SystemEventModelPlaceholder $entities
 * @property SystemEventTypeSetting[]    $systemEventTypeSettings
 */
class SystemEventType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%system_event_type}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['active', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['entity', 'template_format'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1000],
            [['type_event'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'entity' => Yii::t('app', 'List Model Class Name'),
            'title' => Yii::t('app', 'Title'),
            'type_event' => Yii::t('app', 'Type event'),
            'description' => Yii::t('app', 'Description'),
            'template_format' => Yii::t('app', 'Template format description'),
            'active' => Yii::t('app', 'Active'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    public function behaviors(): array
    {
        return [
            TimestampBehavior::class => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::class => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @param int $typeId
     *
     * @return SystemEventType|null
     */
    public static function findById($id)
    {
        return self::findOne(['id' => $id]);
    }

    /**
     * @return ActiveQuery
     */
    public function getSystemEvents()
    {
        return $this->hasMany(SystemEvent::class, ['type_id' => 'id']);
    }

    /**
     * @return SystemEventModelPlaceholder[]|array
     */
    public function getEntities()
    {
        if (!$this->entity) {
            return [];
        }

        return Json::decode($this->entity);
    }

    /**
     * @return SystemEventModelPlaceholder[]|array
     */
    public function getPlaceholders()
    {
        $entityList = $this->getEntities();

        return SystemEventModelPlaceholder::find()
            ->where(['entity' => $entityList])
            ->all();
    }
    /**
     * @return SystemEventModelPlaceholder[]|array
     */
    public function placeholdersToArray()
    {
        $entityList = $this->getEntities();

        return SystemEventModelPlaceholder::find()
            ->where(['entity' => $entityList])
            ->all();
    }
    /**
     * @return ActiveQuery
     */
    public function getSystemEventTypeSettings()
    {
        return $this->hasMany(SystemEventTypeSetting::class, ['type_id' => 'id'])
            ->groupBy('id, type_id, sort');
    }

    /**
     * {@inheritdoc}
     * @return SystemEventTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SystemEventTypeQuery(get_called_class());
    }
}
