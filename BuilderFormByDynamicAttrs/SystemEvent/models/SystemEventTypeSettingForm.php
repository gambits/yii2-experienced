<?php

namespace app\modules\SystemEvent\models;

use app\modules\SystemEvent\components\interfaces\FormSettingInterface;
use app\modules\SystemEvent\models\query\SystemEventTypeSettingFormQuery;
use app\widgets\Select;
use Yii;
use yii\base\Model;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%system_evt_type_settg_form}}".
 *
 * @property int                    $id
 * @property int                    $setting_id        System Event Type
 * @property string                 $field_type        Title
 * @property string                 $widget            Widget Class Name
 * @property string                 $data_field        Data for field
 * @property string                 $type_init_data    Type init data
 * @property int                    $is_required       Is Required
 * @property string                 $is_multiple       Is Multiple
 * @property string                 $default_value     Default value
 * @property string                 $created_at        Created At
 * @property string                 $updated_at        Updated At
 * @property int|null               $created_by        Created By
 * @property int|null               $updated_by        Updated By
 *
 * @property SystemEventTypeSetting $setting
 */
class SystemEventTypeSettingForm extends ActiveRecord implements FormSettingInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%system_evt_type_settg_form}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['setting_id', 'field_type'], 'required'],
            [['setting_id'], 'integer'],
            [['is_required', 'is_multiple'], 'boolean'],
            [['is_required', 'is_multiple'], 'default', 'value' => false],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'safe'],
            [['field_type', 'default_value'], 'string', 'max' => 255],
            [['data_field', 'widget'], 'string'],
            [['type_init_data'], 'string', 'max' => 100],
            [['setting_id'], 'exist', 'skipOnError' => true, 'targetClass' => SystemEventTypeSetting::class, 'targetAttribute' => ['setting_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'setting_id' => Yii::t('app', 'System Event Type'),
            'field_type' => Yii::t('app', 'Title'),
            'data_field' => Yii::t('app', 'Data For Field'),
            'type_init_data' => Yii::t('app', 'Type Init Data For Field'),
            'is_required' => Yii::t('app', 'Is Required'),
            'is_multiple' => Yii::t('app', 'Is Multiple'),
            'default_value' => Yii::t('app', 'Default value'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    public function behaviors(): array
    {
        return [
            TimestampBehavior::class => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::class => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getSetting()
    {
        return $this->hasOne(SystemEventTypeSetting::class, ['id' => 'setting_id']);
    }

    /**
     * {@inheritdoc}
     * @return SystemEventTypeSettingFormQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SystemEventTypeSettingFormQuery(get_called_class());
    }

    /** implemets methods FormSettingInterface */
    /**
     * @return bool
     */
    public function isMultiple(): bool
    {
        return $this->is_multiple;
    }

    /**
     * @return bool
     */
    public function isRequired(): bool
    {
        return $this->is_required;
    }

    /**
     * @param $property
     *
     * @return bool
     */
    public function isDisabled($property): bool
    {
        return $property === 'type_handle';
    }

    /**
     * @param Model $model
     *
     * @return string
     */
    public function getCurrentValue(Model $model)
    {
        $attribute = $this->setting->slug;

        return $model->{$attribute};
    }

    /**
     * @return string|array|callable
     */
    public function getDefaultValue()
    {
        return $this->default_value;
    }

    /**
     * @param string $attribute
     *
     * @return string|array|callable
     */
    public function getLabel($attribute)
    {
        return $this->setting->title;
    }

    /**
     * @return string
     */
    public function getFieldType(): string
    {
        return $this->field_type;
    }

    /**
     * @return string
     */
    public function getFieldData(): string
    {
        return $this->data_field;
    }

    /**
     * @return string|null
     */
    public function getTypeInitData(): ?string
    {
        return $this->type_init_data;
    }

    /**
     * @param string $property
     * @param string $type
     *
     * @return bool|string|null
     */
    public function getClassWidget($property, $type)
    {
        return in_array($type, ['dropdown', 'dropDownList'], true) ? Select::class : false;
    }
    /** End implemets methods FormSettingInterface */
}
