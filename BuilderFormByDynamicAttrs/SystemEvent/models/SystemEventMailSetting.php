<?php

namespace app\modules\SystemEvent\models;

use app\helpers\RoleHelper;
use app\modules\SystemEvent\components\DTO\ModelDto;
use app\modules\SystemEvent\components\interfaces\EventMailSettingInterface;
use app\modules\SystemEvent\components\interfaces\PlaceholderResolverInterface;
use app\modules\SystemEvent\components\interfaces\SystemEventModelInterface;
use Yii;
use app\models\TaskStage;
use app\models\Workflow;
use app\modules\SystemEvent\models\query\SystemEventMailSettingQuery;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%system_event_mail_setting}}".
 *
 * @property int               $id
 * @property int               $system_event_id              System Event Id
 * @property int|null          $type_handle                  Type Handler
 * @property int|null          $handler                      Handler
 * @property string|null       $event_name                   Event Name
 * @property int|null          $workflow                     Workflow
 * @property string|array|null $stage                        Stage      - json
 * @property string|array|null $recipients                   Recipients - json
 * @property string|array|null $personal_user_by_workflow    Personal User By Workflow - json
 * @property string|array|null $role                         User role  - json
 * @property string|array|null $list_email                   List email - json
 * @property string|null       $subject_mail                 Subject
 * @property string|null       $text_mail                    Email message
 * @property string|null       $text_for_informer            Message for informer
 * @property int               $is_send_notify_before_change Is send notify before change
 * @property int|null          $count_days_to_blocked        Count Days To Blocked
 *
 * @property SystemEvent       $systemEvent
 * @property SystemEventType   $typeHandle
 * @property Workflow          $relWorkflow
 */
class SystemEventMailSetting extends ActiveRecord implements EventMailSettingInterface
{
    public const MAIL_RECIPIENTS_SLUG = [
        'recipients',
        'role',
        'personal_user_by_workflow',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%system_event_mail_setting}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['system_event_id'], 'required'],
            [['subject_mail', 'text_mail', 'text_for_informer', 'personal_user_by_workflow', 'handler'], 'string'],
            [['event_name'], 'string', 'max' => 255],
            [['is_send_notify_before_change'], 'default', 'value' => false],
            [['system_event_id'], 'exist', 'skipOnError' => true, 'targetClass' => SystemEvent::class, 'targetAttribute' => ['system_event_id' => 'id']],
            [['type_handle'], 'exist', 'skipOnError' => true, 'targetClass' => SystemEventType::class, 'targetAttribute' => ['type_handle' => 'id']],
            [['stage'], 'validateStage'],
            [['workflow'], 'validateWorkflow'],
            [['list_email'], 'validateListEmail'],
            [['role'], 'validateRole'],
            [
                ['system_event_id', 'type_handle', 'recipients', 'list_email', 'workflow', 'stage', 'is_send_notify_before_change', 'count_days_to_blocked'],
                'safe',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'system_event_id' => Yii::t('app', 'System Event Id'),
            'type_handle' => Yii::t('app', 'Type Handler'),
            'handler' => Yii::t('app', 'Handler'),
            'event_name' => Yii::t('app', 'Event And Handler'),
            'workflow' => Yii::t('app', 'Workflow'),
            'stage' => Yii::t('app', 'Stage'),
            'recipients' => Yii::t('app', 'Recipients'),
            'list_email' => Yii::t('app', 'List email'),
            'subject_mail' => Yii::t('app', 'Subject'),
            'text_mail' => Yii::t('app', 'Email message'),
            'text_for_informer' => Yii::t('app', 'Message for informer'),
            'personal_user_by_workflow' => Yii::t('app', 'Personal User By Workflow'),
            'is_send_notify_before_change' => Yii::t('app', 'Is send notify before change'),
            'count_days_to_blocked' => Yii::t('app', 'Count Days To Blocked'),
        ];
    }

    public function afterFind()
    {
        $this->list_email = Json::decode($this->list_email);
        $this->stage = Json::decode($this->stage);
        $this->recipients = Json::decode($this->recipients);
        $this->role = Json::decode($this->role);
        parent::afterFind();
    }

    public function afterValidate()
    {
        $this->list_email = Json::encode($this->list_email);
        $this->stage = Json::encode($this->stage);
        $this->recipients = Json::encode($this->recipients);
        $this->role = Json::encode($this->role);

        return parent::afterValidate();
    }

    public function validateStage()
    {
        if (!is_array($this->stage)) {
            $this->stage = [];
        }
        foreach ($this->stage as $stageItem) {
            if (strpos($stageItem, 'system') !== false) {
                $stageWithoutPrefix = str_replace('system_', '', $stageItem);
                if (!isset(TaskStage::getSystemStages()[$stageWithoutPrefix])) {
                    $this->addError('stage', Yii::t('app', 'TaskStage {id} не найдено', ['{id}' => $stageItem]));

                    return false;
                }
            } elseif (strpos($stageItem, '_') !== false) {
                $stage = explode('_', $stageItem);
                $stageItemId = $stage[1] ?? null;
                if (!TaskStage::findOne($stageItemId)) {
                    $this->addError('stage', Yii::t('app', 'TaskStage {id} не найдено', ['{id}' => $stageItem]));

                    return false;
                }
            }
        }

        return true;
    }

    public function validateWorkflow()
    {
        if (!Workflow::findOne($this->workflow)) {
            $this->addError('workflow', Yii::t('app', 'Workflow #{id} не найдено', ['{id}' => $this->workflow]));

            return false;
        }

        return true;
    }

    public function validateListEmail()
    {
        if (!is_array($this->list_email)) {
            $this->list_email = [];
        }
        $items = [];
        foreach ($this->list_email as $email) {
            if (!$email) {
                continue;
            }
            $email = trim($email);
            $items[$email] = $email;
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $this->addError('list_email', Yii::t('app', 'Email {email} is no valid', ['{email}' => $email]));

                return false;
            }
        }
        $this->list_email = array_values($items);

        return true;
    }

    public function validateRole()
    {
        if (!is_array($this->role)) {
            $this->role = [];
        }
        $items = [];
        foreach ($this->role as $role) {
            $items[$role] = $role;
            if (!RoleHelper::getRoles($role)) {
                $this->addError('role', Yii::t('app', 'Role {role} is no valid', ['{role}' => $role]));

                return false;
            }
        }
        $this->list_email = array_values($items);
    }

    /**
     * @return ActiveQuery
     */
    public function getRelWorkflow()
    {
        return $this->hasOne(Workflow::class, ['id' => 'workflow']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSystemEvent()
    {
        return $this->hasOne(SystemEvent::class, ['id' => 'system_event_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTypeHandle()
    {
        return $this->hasOne(SystemEventType::class, ['id' => 'type_handle']);
    }

    /**
     * {@inheritdoc}
     * @return SystemEventMailSettingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SystemEventMailSettingQuery(get_called_class());
    }

    /** implements EventMailSettingInterface methods */
    /**
     * @param array      $models
     * @param array|null $append
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function getPlaceholderAndValue(array $models = [], ?array $append = []): array
    {
        /** @var PlaceholderResolverInterface $resolver */
        $resolver = Yii::$container->get(PlaceholderResolverInterface::class);
        $items = [];
        /** @var SystemEventType $typeEvent */
        $typeEvent = $this->systemEvent->type;
        $placeholders = $typeEvent->getPlaceholders();
        foreach ($placeholders as $placeholder) {
            $items[$placeholder->placeholder] = $resolver->resolve($models, $placeholder);
        }

        return $append ? array_merge($items, $append) : $items;
    }

    /**
     * @return string
     */
    public function getTextEmail(): string
    {
        return $this->text_mail;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject_mail;
    }

    /**
     * @param mixed|Model $model
     *
     * @return array
     */
    public function getTo(SystemEventModelInterface $model): array
    {
        return array_merge($this->list_email, $model->getEventData(new ModelDto('email', $this->getRoleTo())));
    }

    /**
     * @return array
     */
    public function getRoleTo(): array
    {
        $list = [];
        /** @var SystemEventTypeSetting $typeSetting */
        foreach ($this->getFieldListTo() as $typeSetting) {
            $propertyRecipient = $typeSetting->slug;
            if ($recipients = $this->{$propertyRecipient}) {
                foreach ($recipients as $recipient) {
                    $list[$recipient] = $recipient;
                }
            }
        }

        return $list;
    }

    /** END implements EventMailSettingInterface methods */
    /**
     * @return array
     */
    public function getFieldListTo(): array
    {
        $type = $this->systemEvent->type;
        $items = array_filter(
            $type->systemEventTypeSettings,
            function ($itemTypeSetting) {
                return $itemTypeSetting && in_array($itemTypeSetting->slug, self::MAIL_RECIPIENTS_SLUG, true);
            }
        );

        return array_values($items);
    }
}
