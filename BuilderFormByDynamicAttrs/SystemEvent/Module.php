<?php

namespace app\modules\SystemEvent;

/**
 * SystemEvent module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\SystemEvent\controllers';
    /**
     * @var string|null
     */
    public $dataForLoad;
}
