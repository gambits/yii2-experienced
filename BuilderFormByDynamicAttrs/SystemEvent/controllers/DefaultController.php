<?php

namespace app\modules\SystemEvent\controllers;

use app\actions\TogglePropertyAction;
use app\modules\SystemEvent\helpers\SystemEventHelper;
use Yii;
use app\actions\IndexAction;
use app\models\User;
use app\modules\SystemEvent\components\interfaces\BuildByTypeInterface;
use app\modules\SystemEvent\models\form\SystemEventForm;
use app\modules\SystemEvent\models\form\ToggleActiveForm;
use app\modules\SystemEvent\models\SystemEventMailSetting;
use app\modules\SystemEvent\models\SystemEventType;
use app\modules\SystemEvent\models\SystemEvent;
use app\modules\SystemEvent\models\search\SystemEventSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * DefaultController implements the CRUD actions for SystemEvent model.
 */
class DefaultController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'create',
                            'update',
                            'delete',
                            'build-form',
                            'toggle-active',
                            'batch-operation',
                        ],
                        'allow' => true,
                        'roles' => [
                            User::SYSTEM_EVENT_DISPATCHER_EDIT,
                        ],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => [
                            User::SYSTEM_EVENT_LIST,
                        ],
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => [
                            User::SYSTEM_EVENT_CREATE,
                        ],
                    ],
                    [
                        'actions' => ['update', 'toggle-active', 'batch-operation'],
                        'allow' => true,
                        'roles' => [
                            User::SYSTEM_EVENT_UPDATE,
                        ],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => [
                            User::SYSTEM_EVENT_DELETE,
                        ],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'modelClass' => SystemEventSearch::class,
                'paramsView' => [
                    'actions' => SystemEventHelper::getAllowBatchAction(),
                ],
            ],
        ];
    }

    public function actionCreate()
    {
        $form = new SystemEventForm();
        $model = new SystemEvent();
        if (Yii::$app->request->isPost && $form->load(\Yii::$app->request->post()) && $form->validate()) {
            if ($model = Yii::$app->systemEventManager->create($form)) {
                return $this->redirect(['index']);
            }
        }
        if (Yii::$app->request->isAjax) {
            return $this->asJson(
                [
                    'form' => $this->renderPartial(
                        '_form',
                        [
                            'formBuilder' => Yii::$container->get(BuildByTypeInterface::class),
                            'type' => new SystemEventType(),
                            'model' => $model,
                            'modelForm' => $form,
                        ]
                    ),
                ]
            );
        }

        return $this->render(
            'create',
            [
                'model' => $model,
            ]
        );
    }


    /**
     * @param int $id
     *
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     * @todo: Сейчас форма инитится из настроек для email уведомлений (событий), далее могут быть другие события
     */
    public function actionUpdate($id)
    {
        /** @var SystemEvent $model */
        $model = $this->findModel($id);
        $form = new SystemEventForm($model->systemEventMailSettings->attributes);
        if (Yii::$app->request->isPost && $form->load(\Yii::$app->request->post()) && $form->validate()) {
            if ($model = Yii::$app->systemEventManager->update($form)) {
                return $this->redirect(['index']);
            }
        }
        $form->event_name = $model->event_name;
        $form->handler = $model->handler;
        if (Yii::$app->request->isAjax) {
            return $this->asJson(
                [
                    'form' => $this->renderPartial(
                        '_form',
                        [
                            'formBuilder' => Yii::$container->get(BuildByTypeInterface::class),
                            'type' => $model->type,
                            'model' => $model,
                            'modelForm' => $form,
                        ]
                    ),
                ]
            );
        }
    }

    /**
     * @param int $id
     *
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionToggleActive($id)
    {
        /** @var SystemEvent $model */
        $model = $this->findModel($id);
        $form = new ToggleActiveForm(['id' => $id]);
        if ($form->load(Yii::$app->request->post(), '') && $form->validate()) {
            if (Yii::$app->systemEventManager->toggleActiveItem($form)) {
                return $this->asJson(['status' => 200]);
            }
        }

        return $this->asJson(['status' => 500, 'errors' => array_merge($form->errors, $model->errors)]);
    }

    /**
     * @param string $action
     * @param array  $ids
     *
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBatchOperation(string $action)
    {
        $ids = Yii::$app->request->post('ids');
        switch ($action) {
            case 'enabled':
            case 'disabled':
                foreach ($ids as $id) {
                    $form = new ToggleActiveForm(['id' => $id, 'active' => $action === 'enabled']);
                    if ($form->load(Yii::$app->request->post(), '') && $form->validate()) {
                        $model = Yii::$app->systemEventManager->toggleActiveItem($form);
                        if ($model->errors) {
                            var_dump($model->errors);
                            die;
                        }
                    }
                }
                break;
        }
    }

    /**
     * @param int $id
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        Yii::$app->systemEventManager->delete($id);

        return $this->redirect(['index']);
    }

    /**
     * @param int $id
     *
     * @return string
     * @throws \Exception
     */
    public function actionBuildForm($id)
    {
        $type = SystemEventType::findOne($id);

        return $this->asJson(
            [
                'form' => $this->renderPartial(
                    '_form_builded_by_type',
                    [
                        'formBuilder' => Yii::$container->get(BuildByTypeInterface::class),
                        'type' => $type,
                        'model' => new SystemEventForm(
                            [
                                'type_handle' => $id,
                            ]
                        ),
                        'systemEvent' => new SystemEvent(),
                    ]
                ),
            ]
        );
    }

    /**
     * @return array
     */
    public function actionValidationFormBuild()
    {
        $model = new SystemEventForm();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }
    }

    /**
     * @param integer $id
     *
     * @return SystemEvent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    private function findModel($id)
    {
        if (($model = SystemEvent::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested model #{id} does not exist.', ['{id}' => $id]));
    }
}
