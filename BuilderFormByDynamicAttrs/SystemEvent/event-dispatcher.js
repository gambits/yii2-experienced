import {bindModal} from "./modal";
import {datepicker, selectpicker} from "./picker";
import {switchTabs} from "./tabs";
import Vue from "vue";
import SwitchButton from "./SwitchButton.vue";
import {isVisible} from "bootstrap-vue/esm/utils/dom";

function eventDispatcherFormInit(_modal) {
    selectpicker(_modal);
    datepicker(_modal);
    switchTabs($("#systemEventNav"));

    _modal.on("shown.bs.modal", function () {
        const switchButton = new Vue({
            el: "#js-checkbox-toggle",
            ...SwitchButton
        });

        switchButton.configure({
            nameInput: "SystemEvent[active]",
            onChange: function (event) {
                const alertMessage = $(".flash-alerts .alert");
                const message = $(".flash-alerts").find("#message");
                const systemEventActive = $("#systemEventActive");
                let active = event.target.checked ? 1 : 0;
                $.post("/systemEvent/default/toggle-active?id=" + $("#systemEventId").val(), {"active": active}, function (response) {
                    if (parseInt(response.status) == 200) {
                        message.text("Обновлено успешно");
                        alertMessage.addClass("success");
                        systemEventActive.val(active);
                    } else {
                        message.text("Ошибка обновления");
                        alertMessage.addClass("error");
                    }
                    alertMessage.parent("div").show();
                    setTimeout(function () {
                        alertMessage.parent("div").hide();
                    }, 2000);
                });
            },
            checked: parseInt($("#systemEventActive").val())
        });
        $("#systemeventtype-id").on("change", function(){
            setTimeout(function () {
                $.get("/taskboard/stage/list", {workflow_id: $("#workflow").val(), multiple: 1}, function (options) {
                    $("select#stage", _modal).html(options);
                    $("select#stage", _modal).selectpicker("refresh");
                });
            }, 1000);
        });
    });

    _modal.on("change", ".js-handler.selectpicker", function (event) {
        let systemEventTypeId = $(event.currentTarget).val();
        if (systemEventTypeId) {
            $.get("/systemEvent/default/build-form", {id: systemEventTypeId}, function (e) {
                let form = $(".js-handler-params", _modal);
                form.html(e.form);
                $(".selectpicker", form).selectpicker();
                $(".js-submit").removeAttr("disabled");
            });
            $("#js-type-event").val(systemEventTypeId);
        } else {
            $(".js-handler-params", _modal).html("");
        }
    });

    _modal.on("change", "select#workflow", function () {
        $.get("/taskboard/stage/list", {workflow_id: $(this).val(), multiple: 1}, function (options) {
            $("select#stage", _modal).html(options);
            $("select#stage", _modal).selectpicker("refresh");
        });
    });
}

export function EventDispatcherForm(selector, options) {
    options = $.extend({
        beforeShow: eventDispatcherFormInit,
        afterClose: function () {
            window.location.reload();
        },
    }, options || {});
    bindModal(selector, options);
}

export function EventList() {
    $("body").on("click", "#js-filter-toggle", function () {
        return $("#js-filter-form").toggle();
    });
    EventDispatcherForm($(".js-show-modal"));
    toggleState();
}

export function toggleState() {
    $(".es-action-batch").on("click", function (e) {
        e.preventDefault();
        const $actionButton = $(e.currentTarget);
        let batchItems = [];
        $(".se-item").each((i, el) => {
            batchItems.push($(el).data("id"));
        });
        const modalApprove = $("#modal-change-stage-syst-event");
        if (batchItems.length) {
            $.ajax({
                "url": $actionButton.attr("href"),
                "data": {"ids": batchItems},
                "type": "POST",
                "success": (data) => {
                    window.location.reload();
                }
            });
        } else {
            modalApprove.modal("show");
            $(modalApprove).on("shown.bs.modal", function (e) {
                $(e.currentTarget).find(".modal-body").html("Список событий пуст, изменение невозможно");
                $("#js-approve-action").hide();
                $("#js-dismiss-modal").text("OK");
            });
        }

        return false;
    });
}


