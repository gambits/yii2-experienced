<?php

use app\components\interfaces\EventDispatcherInterface;
use app\modules\SystemEvent\components\eventDispatcher\handlers\{
    EmailNotifyChangeActiveToForDealerPoll,
    EmailNotifyChangeApproverDealerPoll,
    EmailNotifyDealerPollAddObservers,
    EmailNotifyDealerPollClose,
    EmailNotifyNextStepWorkflowToAuthorResponsibleDealerPoll,
    EmailNotifyPossibleExecutor,
    EmailNotifyTaskAssignee,
    EmailNotifyTaskChangeInspector,
    EmailNotifyToAuthorForDealerPollWithoutResponsible,
    EmailNotifyToAuthorForDealerPollWithResponsible,
    EmailNotifyToAuthorForNotPossibleDetermineResponsible,
    EmailNotifyToResponsibleWithCheckedSurveyCompletionDate,
    EmailNotifyToResponsibleWithEndSurveyCompletionDateAndBlockedDealerPoll
};
use app\modules\SystemEvent\components\interfaces\{
    BuildByTypeInterface,
    NotifyInterface,
    EventHandleManagerInterface,
    PlaceholderResolverInterface
};
use app\modules\SystemEvent\components\{
    PlaceholderResolver,
    FormBuilder,
    Notifier,
    EmailNotifier,
    EventHandleManager
};
use app\components\event\EventDispatcher;
use app\modules\SystemEvent\components\configuratorField\{
    FieldDecorator,
    SystemEventTypeDecorator,
    RoleDecorator,
    HandlerDecorator,
};

Yii::$container->set(EventDispatcherInterface::class, EventDispatcher::class);
Yii::$container->setDefinitions(
    [
        BuildByTypeInterface::class => function () {
            return new FormBuilder(
                [
                    FieldDecorator::class,
                    SystemEventTypeDecorator::class,
                    RoleDecorator::class,
                    HandlerDecorator::class,
                ],
                Yii::$container->get(EventDispatcherInterface::class)
            );
        },
        NotifyInterface::class => function () {
            return new Notifier(
                [
                    'providers' => [
                        'email' => new EmailNotifier(),
                    ],
                ]
            );
        },
        EventHandleManagerInterface::class => function () {
            return new EventHandleManager(
                [
                    new EmailNotifyTaskAssignee(),
                    new EmailNotifyDealerPollClose(),
                    new EmailNotifyTaskChangeInspector(),
                    new EmailNotifyNextStepWorkflowToAuthorResponsibleDealerPoll(),
                    new EmailNotifyToAuthorForDealerPollWithoutResponsible(),
                    new EmailNotifyToAuthorForDealerPollWithResponsible(),
                    new EmailNotifyToAuthorForNotPossibleDetermineResponsible(),
                    new EmailNotifyChangeApproverDealerPoll(),
                    new EmailNotifyToResponsibleWithCheckedSurveyCompletionDate(),
                    new EmailNotifyToResponsibleWithEndSurveyCompletionDateAndBlockedDealerPoll(),
                    new EmailNotifyPossibleExecutor(),
                    new EmailNotifyDealerPollAddObservers(),
                    new EmailNotifyChangeActiveToForDealerPoll(),
                ]
            );
        },
        PlaceholderResolverInterface::class => PlaceholderResolver::class,
    ]
);
