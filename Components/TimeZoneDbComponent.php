<?php

namespace app\components;

use DateTime;
use DateTimeZone;
use IntlDateFormatter;
use yii\base\Component;

class TimeZoneDbComponent extends Component
{
    public function getList(): array
    {
        static $result = null;
        if ($result === null) {
            $result = [];
            $now = new DateTime();
            foreach (DateTimeZone::listIdentifiers() as $timeZoneId) {
                $fmt = datefmt_create(
                    \Yii::$app->language,
                    IntlDateFormatter::FULL,
                    IntlDateFormatter::FULL,
                    $timeZoneId,
                    IntlDateFormatter::GREGORIAN,
                    '(z) VVVV'
                );
                if (!$fmt) {
                    continue;
                }
                $timeZone = new DateTimeZone($timeZoneId);
                $result[$timeZoneId] = [
                    'id' => $timeZoneId,
                    'name' => $fmt->format($now),
                    'offset' => $timeZone->getOffset($now),
                ];
            }
            uasort(
                $result,
                function ($a, $b) {
                    $result = $a['offset'] - $b['offset'];
                    if (!$result) {
                        $result = strcmp($a['name'], $b['name']);
                    }

                    return $result;
                }
            );

            $result = array_map(
                function ($a) {
                    return $a['name'];
                },
                $result
            );
        }

        return $result;
    }
}
