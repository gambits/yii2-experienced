<?php

namespace app\components;

use app\assets\PhoneMaskAsset;
use app\helpers\Helper;
use app\models\AbstractType;
use app\models\Activity;
use app\models\AEBUploadHistory;
use app\models\Company;
use app\models\Competitor;
use app\models\Contract;
use app\models\ContractCreationReason;
use app\models\ContractTerminationReason;
use app\models\ContractType;
use app\models\CorporateClientOutsourcing;
use app\models\DcConcept;
use app\models\DcPlacingFormat;
use app\models\DcStatus;
use app\models\Dealer;
use app\models\DealerContact;
use app\models\DealerContactType;
use app\models\DecisionCriteria;
use app\models\Department;
use app\models\DocumentType;
use app\models\EntityType;
use app\models\EventType;
use app\models\Holding;
use app\models\HoldingCompany;
use app\models\MarketType;
use app\models\MediaType;
use app\models\Month;
use app\models\OutdatedParkUpdatedRule;
use app\models\Position;
use app\models\PurchaseMechanism;
use app\models\RegionExport;
use app\models\RegistrationType;
use app\models\Source;
use app\models\Tag;
use app\models\Task;
use app\models\TransportWorkType;
use app\models\Upload;
use app\models\User;
use app\models\Workflow;
use app\modules\DealerPoll\models\DealerPoll;
use app\modules\DealerPoll\models\DealerSurvey;
use app\modules\ActionPlan\models\ActionPlan;
use app\modules\HistoryChange\models\HistoryChange;
use app\modules\poll\models\PollQuestion;
use DateInterval;
use DateTime;
use DateTimeZone;
use DomainException;
use Exception;
use infotech\reference\models\City;
use infotech\reference\models\Country;
use infotech\reference\models\FederalDistrict;
use infotech\reference\models\ModelClass;
use infotech\reference\models\ModelSegment;
use infotech\reference\models\Region;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class Formatter
 * @package app\components
 */
class Formatter extends \yii\i18n\Formatter
{
    const PHONE_MASK = 'phone-mask-input-patterns';

    /**
     * @param int|float $value
     * @param int|float $total
     * @return string
     */
    public function valueWithPercent($value, $total)
    {
        $percent = $total ? round($value / $total * 100) : 0;

        return $value . ' (' . $percent . '%)';
    }


    private function getMaskedInputPatterns()
    {
        $patterns = Yii::$app->getCache()->get(self::PHONE_MASK);
        if ($patterns === false) {
            $asset = new PhoneMaskAsset();
            foreach ($asset->js as $js) {
                $content = file_get_contents($asset->sourcePath . '/' . $js);
                if (preg_match_all('/mask\:\s*\"([^\"]+)\"/', $content, $match)) {
                    $patterns = [];
                    foreach (array_reverse($match[1]) as $mask) {
                        // заменим скобки на минус чтобы не потерять границы групп цифр
                        $mask = str_replace(['(', ')'], ['-', '-'], $index = $mask);
                        // объединим группы цифр в ()
                        $mask = preg_replace('/(#+)/', '(${0})', $mask);
                        // экранируем +, убираем разделите групп - и меняем # на \d применив такой шаблон к телефону +7xxxxxxxxx мы разобьем цифры на группы и будем использовать эти группы при форматировании
                        $mask = '/' . str_replace(['+', '-', '#'], ['\+', '', '\d'], $mask) . '/';
                        $patterns[$index] = $mask;
                    }
                    break;
                }
            }
            if ($patterns) {
                Yii::$app->getCache()->set(self::PHONE_MASK, $patterns, 86400);
            } else {
                throw new DomainException('Invalid phone mask file');
            }
        }

        return $patterns;
    }

    public function asPhone($value)
    {
        foreach ($this->getMaskedInputPatterns() as $format => $mask) {
            if (preg_match($mask, $value, $matches)) {
                array_shift($matches);
                $value = preg_replace_callback(
                    '/#+/',
                    function () use (&$matches) {
                        return array_shift($matches);
                    },
                    $format
                );

                return $value;
            }
        }

        return $value;
    }


    /**
     * @param $value
     * @return string
     */
    public function asMonthName($value)
    {
        return ArrayHelper::getValue(Month::getList(), $value);
    }

    public function asDiff($a, $b)
    {
        if ($a != $b) {
            $diff = $a - $b;

            return ($diff > 0 ? '+' : '') . $diff;
        }

        return null;
    }

    public function asRequestOverdue($value)
    {
        return $value ? '<span class="glyphicon glyphicon-warning-sign text-danger" style="font-size:14px"></span>' :
            '';
    }

    public function linkIfNotEmpty($arr, $field, $url)
    {
        $value = ArrayHelper::getValue($arr, $field);

        return $value ? Html::a($value, $url) : 0;
    }

    public function linkAndPercent($arr, $fieldValue, $fieldDisc, $url, $htmlOptions = [])
    {
        $value = (int)ArrayHelper::getValue($arr, $fieldValue, 0);
        $fieldDisc = (int)ArrayHelper::getValue($arr, $fieldDisc, 0);
        if (!$fieldDisc || !$value) {
            return $value;
        }

        return Html::a($value, $url, $htmlOptions) . ' (' . round($value / $fieldDisc * 100) . '%)';
    }

    /**
     * Форматирует оставшееся либо просроченное время
     * @param int  $period
     * @param int  $started
     * @param null $cutoff
     * @return string
     * @throws Exception
     */
    public function asTimeLeft(int $period, int $started, $cutoff = null): string
    {
        $left = $started + $period - time();
        $text = 'Осталось ';
        if ($left < 0) {
            $text = 'Просрочено на ';
            $left = -$left;
        }

        return $text . $this->asDurationWithFormat($left, ', ', '-', 'ymdhi', $cutoff);
    }

    /**
     * Переделанный форматтер asDuration
     * добавлена возможность указывать формат вывода
     * @param mixed  $value
     * @param string $implodeString
     * @param string $negativeSign
     * @param string $format
     * @param null   $cutoff
     * @return string
     * @throws Exception
     */
    public function asDurationWithFormat(
        $value,
        $implodeString = ', ',
        $negativeSign = '-',
        $format = 'ymdhis',
        $cutoff = null
    ): string {
        if ($value === null) {
            return $this->nullDisplay;
        }

        if ($value instanceof DateInterval) {
            $isNegative = $value->invert;
            $interval = $value;
        } elseif (is_numeric($value)) {
            $isNegative = $value < 0;
            $zeroDateTime = (new DateTime())->setTimestamp(0);
            $valueDateTime = (new DateTime())->setTimestamp(abs($value));
            $interval = $valueDateTime->diff($zeroDateTime);
        } elseif (strpos($value, 'P-') === 0) {
            $interval = new DateInterval('P' . substr($value, 2));
            $isNegative = true;
        } else {
            $interval = new DateInterval($value);
            $isNegative = $interval->invert;
        }

        $skip = false;

        if (is_int(strpos($format, 'y')) && $interval->y > 0) {
            $parts[] = Yii::t(
                'yii',
                '{delta, plural, =1{1 year} other{# years}}',
                ['delta' => $interval->y],
                $this->locale
            );
            $skip = $cutoff === 'y';
        }
        if (is_int(strpos($format, 'm')) && $interval->m > 0 && !$skip) {
            $parts[] = Yii::t(
                'yii',
                '{delta, plural, =1{1 month} other{# months}}',
                ['delta' => $interval->m],
                $this->locale
            );
            $skip = $cutoff === 'm';
        }
        if (is_int(strpos($format, 'd')) && $interval->d > 0 && !$skip) {
            $parts[] = Yii::t(
                'yii',
                '{delta, plural, =1{1 day} other{# days}}',
                ['delta' => $interval->d],
                $this->locale
            );
            $skip = $cutoff === 'd';
        }
        if (is_int(strpos($format, 'h')) && $interval->h > 0 && !$skip) {
            $parts[] = Yii::t(
                'yii',
                '{delta, plural, =1{1 hour} other{# hours}}',
                ['delta' => $interval->h],
                $this->locale
            );
            $skip = $cutoff === 'h';
        }
        if (is_int(strpos($format, 'i')) && $interval->i > 0 && !$skip) {
            $parts[] = Yii::t(
                'yii',
                '{delta, plural, =1{1 minute} other{# minutes}}',
                ['delta' => $interval->i],
                $this->locale
            );
            $skip = $cutoff === 'i';
        }
        if (is_int(strpos($format, 's')) && $interval->s > 0 && !$skip) {
            $parts[] = Yii::t(
                'yii',
                '{delta, plural, =1{1 second} other{# seconds}}',
                ['delta' => $interval->s],
                $this->locale
            );
            $skip = $cutoff === 's';
        }
        if (is_int(strpos($format, 's')) && $interval->s === 0 && empty($parts) && !$skip) {
            $parts[] = Yii::t(
                'yii',
                '{delta, plural, =1{1 second} other{# seconds}}',
                ['delta' => $interval->s],
                $this->locale
            );
            $isNegative = false;
        }

        return empty($parts) ? $this->nullDisplay : (($isNegative ? $negativeSign : '') . implode(
            $implodeString,
            $parts
        ));
    }

    public function asDealerStatisticType($statistic_type)
    {
        return ArrayHelper::getValue(Dealer::getStatisticTypeList(), $statistic_type);
    }


    public function asRelativeTime($value, $referenceTime = null)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }

        if ($value instanceof DateInterval) {
            $interval = $value;
        } else {
            $timestamp = $this->normalizeDatetimeValue($value);

            if ($timestamp === false) {
                // $value is not a valid date/time value, so we try
                // to create a DateInterval with it
                try {
                    $interval = new DateInterval($value);
                } catch (Exception $e) {
                    // invalid date/time and invalid interval
                    return $this->nullDisplay;
                }
            } else {
                $timeZone = new DateTimeZone($this->timeZone);

                if ($referenceTime === null) {
                    $dateNow = new DateTime('now', $timeZone);
                } else {
                    $dateNow = $this->normalizeDatetimeValue($referenceTime);
                    $dateNow->setTimezone($timeZone);
                }

                $dateThen = $timestamp->setTimezone($timeZone);

                $interval = $dateThen->diff($dateNow);
            }
        }
        $parts = [];
        if ($interval->y > 0) {
            $parts[] = Yii::t(
                'app',
                '{delta, plural, one{# год} few{# года} many{# лет} other{# года}}',
                ['delta' => $interval->y],
                $this->locale
            );
        }
        if ($interval->m > 0) {
            $parts[] = Yii::t(
                'app',
                '{delta, plural, one{# месяц} few{# месяца} many{# месяцев} other{# месяца}}',
                ['delta' => $interval->m],
                $this->locale
            );
        }
        if ($interval->d > 0) {
            $parts[] = Yii::t(
                'app',
                '{delta, plural, one{# день} few{# дня} many{# дней} other{# дня}}',
                ['delta' => $interval->d],
                $this->locale
            );
        }
        if ($interval->h > 0) {
            $parts[] = Yii::t(
                'app',
                '{delta, plural, one{# час} few{# часа} many{# часов} other{# часа}}',
                ['delta' => $interval->h],
                $this->locale
            );
        }
        if ($interval->i > 0) {
            $parts[] = Yii::t(
                'app',
                '{delta, plural, one{# минута} few{# минут} many{# минут} other{# минуты}}',
                ['delta' => $interval->i],
                $this->locale
            );
        }
        if ($interval->s > 0) {
            $parts[] = Yii::t(
                'app',
                '{delta, plural, one{# секунду} few{# секунды} many{# секунд} other{# секунды}}',
                ['delta' => $interval->s],
                $this->locale
            );
        }

        if ($interval->invert) {
            return $parts ? implode(' ', $parts) : Yii::t('app', 'прямо сейчас', []);
        }

        return $parts ? Yii::t('app', 'просрочено на ') . implode(' ', $parts) : Yii::t('app', 'прямо сейчас', []);
    }

    public function asSourceStatus($value)
    {
        return ArrayHelper::getValue(Source::getStatusList(), $value);
    }

    public function asCompanyStatusName($value)
    {
        return ArrayHelper::getValue(Company::getCompanyStatusList(), $value);
    }

    public function asPurchaseMechanismStatus($value)
    {
        return ArrayHelper::getValue(PurchaseMechanism::getStatusList(), $value);
    }

    public function asDecisionCriteriaStatus($value)
    {
        return DecisionCriteria::getStatusList()[$value] ?? null;
    }

    public function asDealerContactStatus($value)
    {
        return ArrayHelper::getValue(DealerContact::getStatusList(), $value);
    }

    public function asDealerStatus($value)
    {
        return ArrayHelper::getValue(Dealer::getStatusList(), $value);
    }

    public function asDealerFullName(Dealer $model)
    {
        if ($model->name) {
            return "$model->code ($model->name)";
        } else {
            return "$model->code (&nbsp;-&nbsp;)";
        }
    }

    /**
     * @param $val
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function asMoney($val)
    {
        return $this->asCurrency($val, '');
    }

    /**
     * @param Task $task
     * @param bool $absolute
     * @return null|string
     */
    public function taskEntityLink(Task $task, $absolute = false)
    {
        $params = ['/task/index', 'id' => $task->id];

        $entity = Workflow::getEntityByTask($task);
        if ($entity instanceof DealerPoll) {
            $params = ['/dealer-poll/default/index', 'id' => $entity->id];
        }
        if ($entity instanceof ActionPlan) {
            $params = ['/action-plan/default/index', 'id' => $entity->id];
        }

        return Url::to($params, $absolute);
    }

    public function asDocumentTypeStatus($value)
    {
        return ArrayHelper::getValue(DocumentType::getStatusList(), $value);
    }

    /**
     * @param $roleName
     * @return string
     */
    public function asUserRole($roleName)
    {
        if ($role = Yii::$app->authManager->getRole($roleName)) {
            return $role->description;
        }

        return Yii::t('app', 'Роль не задана');
    }

    public function asEntityTypeStatus($value)
    {
        return ArrayHelper::getValue(EntityType::getStatusList(), $value);
    }

    public function asRegionExportStatus($value)
    {
        return ArrayHelper::getValue(RegionExport::getStatusList(), $value);
    }

    public function asPositionStatus($value)
    {
        return ArrayHelper::getValue(Position::getStatusList(), $value);
    }

    public function asYesNo($value)
    {
        return Helper::getYesNoList()[intval($value)] ?? null;
    }

    public function asDepartmentStatus($value)
    {
        return ArrayHelper::getValue(Department::getStatusList(), $value);
    }

    public function asActivityStatus($value)
    {
        return ArrayHelper::getValue(Activity::getStatusList(), $value);
    }

    public function asTagGroupStatus($value)
    {
        return ArrayHelper::getValue(Tag::getStatusList(), $value);
    }

    public function asTagGroupName($value)
    {
        return Tag::getTagGroupList()[$value] ?? null;
    }

    public function asFederalDistrictStatus($value)
    {
        return FederalDistrict::getStatusList()[$value] ?? null;
    }

    public function asRegionStatus($value)
    {
        return Region::getStatusList()[$value] ?? null;
    }

    public function asModelClassStatus($value)
    {
        return ModelClass::getStatusList()[$value] ?? null;
    }

    public function asModelSegmentStatus($value)
    {
        return ModelSegment::getStatusList()[$value] ?? null;
    }

    public function asContractTypeStatus($value)
    {
        return ContractType::getStatusList()[$value] ?? null;
    }

    public function asCompetitorStatus($value)
    {
        return Competitor::getStatusList()[$value] ?? null;
    }

    public function asCompanyStatus($value)
    {
        return Company::getStatusList()[$value] ?? null;
    }

    public function asDcConceptStatus($value)
    {
        return DcConcept::getStatusList()[$value] ?? null;
    }

    public function asMediaTypeStatus($value)
    {
        return MediaType::getStatusList()[$value] ?? null;
    }

    public function asPositionCode($value)
    {
        return Position::getCodeList()[$value] ?? null;
    }

    public function asPositionIsDealer($value)
    {
        return Helper::getYesNoList()[$value] ?? null;
    }

    public function asUserIdFullName($item)
    {
        if (!$item) {
            return '[#]';
        }

        if (is_int($item)) {
            $item = User::findOne($item);
        }

        return Html::encode("[#{$item->id}] " . $item->fullName);
    }

    public function asUserFullName($item)
    {
        return $item ? Html::encode($item->fullName) : $this->nullDisplay;
    }

    public function asHistoryChangeAction($value)
    {
        return ArrayHelper::getValue(HistoryChange::getActionList(), $value);
    }

    public function asRegionIdName($value)
    {
        return "[#{$value}] " . ArrayHelper::getValue(Region::findOne($value), 'name');
    }

    public function asCountryIdName($value)
    {
        return "[#{$value}] " . ArrayHelper::getValue(Country::findOne($value), 'name');
    }

    public function asCityIdName($value)
    {
        return "[#{$value}] " . ArrayHelper::getValue(City::findOne($value), 'name');
    }

    public function asUserIdName($value)
    {
        return "[#{$value}] " . ArrayHelper::getValue(User::findOne($value), 'fullName');
    }

    public function asDealerIdMarketingName($value)
    {
        $dealer = Dealer::findOne($value);

        return "[#{$value}] " . ArrayHelper::getValue($dealer, 'name');
    }

    public function asHoldingIdName($value)
    {
        return "[#{$value}] " . ArrayHelper::getValue(Holding::findOne($value), 'name');
    }

    public function asDcConceptIdName($value)
    {
        return "[#{$value}] " . ArrayHelper::getValue(DcConcept::findOne($value), 'name');
    }

    public function asDcStatusIdName($value)
    {
        return "[#{$value}] " . ArrayHelper::getValue(DcStatus::findOne($value), 'name');
    }

    public function asDcPlacingFormatIdName($value)
    {
        return "[#{$value}] " . ArrayHelper::getValue(DcPlacingFormat::findOne($value), 'name');
    }

    public function asMediaTypeIdName($value)
    {
        return "[#{$value}] " . ArrayHelper::getValue(MediaType::findOne($value), 'name');
    }

    public function asUploadIdUrl($value)
    {
        return '[#' . $value . '] ' . ArrayHelper::getValue(Upload::findOne($value), 'url');
    }

    public function asDealerContactTypeIdName($value)
    {
        return "[#{$value}] " . ArrayHelper::getValue(DealerContactType::findOne($value), 'name');
    }

    public function asContractTypeIdName($value)
    {
        return ArrayHelper::getValue(ContractType::findOne($value), 'name');
    }

    public function asContractCreationReason($value)
    {
        return ArrayHelper::getValue(ContractCreationReason::findOne($value), 'name');
    }

    public function asContractTerminationReason($value)
    {
        return ArrayHelper::getValue(ContractTerminationReason::findOne($value), 'name');
    }

    public function asContract($value)
    {
        return $this->asIdName(Contract::class, $value);
    }

    public function asDocumentTypeName($value)
    {
        return ArrayHelper::getValue(DocumentType::findOne($value), 'name');
    }

    public function asCompanyIdName($value)
    {
        return ArrayHelper::getValue(Company::findOne($value), 'name');
    }

    public function asCompanyInnShortName($value): string
    {
        if (!$company = Company::findOne($value)) {
            return $this->nullDisplay;
        }

        return "[#{$value}] " . implode(', ', [$company->inn, $company->marketing_name]);
    }

    public function asTagIdName($value)
    {
        return "[#{$value}] " . ArrayHelper::getValue(Tag::findOne($value), 'name');
    }

    public function asActivityIdName($value)
    {
        return "[#{$value}] " . ArrayHelper::getValue(Activity::findOne($value), 'name');
    }

    public function asBrandLogoUrl($path)
    {
        return Yii::$app->params['brandLogoHost'] . $path;
    }

    public function getBrandLogoImg($path, $mh = 50, $mw = 100)
    {
        return Html::img($this->asBrandLogoUrl($path), [
            'style' => "max-height:{$mh}px; max-width:{$mw}px;",
        ]);
    }

    public function asStatus($value, $class = null)
    {
        if (null === $class) {
            $class = AbstractType::class;
        }

        return ArrayHelper::getValue(call_user_func([$class, 'getStatusList']), $value, $this->nullDisplay);
    }

    public function asUserStatus($value)
    {
        return User::getStatusList()[$value] ?? null;
    }

    public function asContractStatus($value)
    {
        return Contract::getStatusList()[$value] ?? null;
    }

    public function asAEBUploadHistoryType($value)
    {
        return AEBUploadHistory::getTypeList()[$value] ?? null;
    }

    public function asHoldingCompanyType($value)
    {
        return HoldingCompany::getTypeList()[$value] ?? null;
    }

    public function asCorporateClientOutsourcingType($value)
    {
        return CorporateClientOutsourcing::getOutsourcingTypeList()[$value] ?? null;
    }

    public function asCorporateClientOutsourcingRelationType($value)
    {
        return CorporateClientOutsourcing::getRelationTypeList()[$value] ?? null;
    }

    public function asCorporateClientOutsourcingStatus($value)
    {
        return CorporateClientOutsourcing::getStatusList()[$value] ?? null;
    }

    public function asTransportWorkTypeStatus($value)
    {
        return TransportWorkType::getStatusList()[$value] ?? null;
    }

    public function asPollQuestionCondition(PollQuestion $model)
    {
        if ($model->isCheckboxAnswer()) {
            return Yii::t('app', 'Да') . ' \ ' . Yii::t('app', 'Нет');
        } elseif ($model->isListAnswer()) {
            return Yii::t('app', 'Вариант ответа');
        } elseif ($model->isNumberAnswer()) {
            if ($model->getAnswerCriteria() == PollQuestion::ANSWER_CRITERIA_RANGE) {
                return Yii::t('app', 'от')
                    . ' ' . $model->getRangeFrom()
                    . ' ' . Yii::t('app', 'до')
                    . ' ' . $model->getRangeTo();
            } elseif ($model->getAnswerCriteria() == PollQuestion::ANSWER_CRITERIA_FORMULA) {
                return Yii::t('app', 'Формула');
            } else {
                return $model->getAnswerCriteria() . ' ' . $model->getRangeFrom();
            }
        }

        return null;
    }

    public function asSurveyAnswer(DealerSurvey $model)
    {
        if ($model->question->isCheckboxAnswer()) {
            return ArrayHelper::getValue(Helper::getYesNoList(), (int)$model->kpi_answer);
        }

        if ($model->question->isListAnswer()) {
            return ArrayHelper::getValue($model->answer, 'name');
        }

        if ($model->question->isNumberAnswer()) {
            return $model->any_answer;
        }

        return null;
    }

    public function asFunnelFieldValue($currentValue, $traffic, $compareValue, $invert = false, $title = null)
    {
        if (!$currentValue) {
            return '';
        }
        $result = (string)$currentValue;
        if ($traffic) {
            $percent = $traffic ? round($currentValue / $traffic * 100) : 0;
            $result .= " ({$percent}%) ";
        }
        if ($compareValue !== null) {
            $result .=
                '<div class="total-secondary">' .
                $this->asDifferenceSpan(
                    $currentValue - $compareValue,
                    '',
                    $invert
                )
                . ' ' . $this->asDifferenceSpan(
                    Helper::getRatioDiff($currentValue, $compareValue),
                    '%',
                    $invert
                ) . '</div>';
        }

        return Html::tag('span', $result, ['title' => $title, 'data-toggle' => 'tooltip']);
    }

    /**
     * Собирает цветной <span/> для отображения разницы количества..
     * @param        $value
     * @param string $char
     * @param bool   $invertValue
     * @param array  $htmlOptions
     * @return string <span/> для вставки
     */
    public function asDifferenceSpan($value, $char = '', $invertValue = false, $htmlOptions = [])
    {
        $format = '%+d%s';
        if ($invertValue) {
            $color = $value < 0 ? 'green' : 'red';
        } else {
            $color = $value >= 0 ? 'green' : 'red';
        }

        if ($value == 0) {
            $format = '%d%s';
            $color = 'maroon';
        }

        return Html::tag(
            'span',
            sprintf($format, $value, $char),
            ArrayHelper::merge([
                'style' => [
                    'color' => $color,
                ],
            ], $htmlOptions)
        );
    }

    public function asTreeTrafficTitle($data, $separator = '='): array
    {
        $title = [];
        $events = [
            EventType::TYPE_CALL,
            EventType::TYPE_VISIT,
            EventType::TYPE_INTERNET,
            EventType::TYPE_COLD_CALL,
        ];

        foreach ($events as $event) {
            if ($val = Helper::getTreeSum($data, $event)) {
                $eventName = ($event != EventType::TYPE_VISIT)
                    ? EventType::getNames()[$event]
                    : Yii::t('app', 'Первичные визиты');
                $title[] = $eventName . $separator . $val;
            }
        }

        return $title;
    }

    public function asFullUrl($url, $schema = 'http'): string
    {
        if ($url) {
            if (strpos($url, 'http://') === false && strpos($url, 'https://') === false) {
                return $schema . '://' . $url;
            }
        }

        return $url;
    }

    public function asNl2br($value)
    {
        return nl2br($value);
    }

    public function asOutdatedParkUpdateRuleBrandType($value)
    {
        return ArrayHelper::getValue(OutdatedParkUpdatedRule::getBrandTypeList(), $value, $value);
    }

    public function asWorkTime($workTime)
    {
        if (!isset($workTime['from'], $workTime['to'])) {
            return '-';
        }

        if (!$workTime['from'] && !$workTime['to']) {
            return Yii::t('app', 'Выходной');
        }

        return sprintf('с %s по %s', $workTime['from'], $workTime['to']);
    }

    /**
     * @param $id
     * @param int $length
     * @return string
     */
    public function asRdsId($id, $length = 5)
    {
        return str_pad($id, $length, '0', STR_PAD_LEFT);
    }

    public function asIdName($class, $id, $attribute = 'name')
    {
        if (!$id) {
            return $this->nullDisplay;
        }

        return "[#{$id}] " . ArrayHelper::getValue($class::findOne($id), $attribute);
    }

    public function asName($class, $id, $attr = 'name')
    {
        if (!$id) {
            return $this->nullDisplay;
        }

        return Html::encode(ArrayHelper::getValue($class::findOne($id), $attr));
    }

    public function asMarketTypeName($value): ?string
    {
        return MarketType::getList()[$value] ?? null;
    }

    public function asRegistrationType($value): ?string
    {
        return RegistrationType::getList()[$value] ?? null;
    }
}
