<?php

/**
 * Created by PhpStorm.
 * User: yakov
 * Date: 11.12.2018
 * Time: 15:05
 */

namespace app\components;

/**
 * Class VinCodeComponent
 * @package app\components
 */
class VinCodeComponent
{
    public static function isVinRussiaManufactured($vin, $includeUSSR = true)
    {
        if (!$vin) {
            return false;
        }

        $countryVin = mb_strtoupper(substr($vin, 0, 2));
        if ($includeUSSR) {
            if ($countryVin >= 'XS' && $countryVin <= 'XW') {
                return true;
            }
        }

        if ($countryVin >= 'X3' && $countryVin <= 'X9') {
            return true;
        }

        if ($countryVin === 'X0') {
            return true;
        }

        if ($countryVin >= 'Z7' && $countryVin <= 'Z9') {
            return true;
        }

        if ($countryVin === 'Z0') {
            return true;
        }

        return false;
    }
}
