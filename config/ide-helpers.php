<?php

/**
 * Этот файл не для исполнения. Он не подключается в других файлах, а нужен
 * лишь для того, чтобы IDE делала правильные подсказки при написании кода.
 *
 * Для избавления от предупреждения PhpStorm "Multiple Implementations"
 * нужно пометить исходный файл vendor/yiisoft/yii2/Yii.php как Plain Text
 */

namespace {

    use app\components\DsfComponent;
    use app\components\Formatter;
    use app\components\SSOComponent;
    use app\components\DaDataComponent;
    use app\components\StorageContainer;
    use app\components\workflow\DealerPollMonitoring;
    use yii\queue\Queue;

    require __DIR__ . '/../vendor/yiisoft/yii2/BaseYii.php';

    /**
     *
     * @property-read \yii\db\Connection $ref_db  Соединение с референсной БД
     * @property-read Formatter          $formatter
     * @property-read Queue              $queue
     * @property-read SSOComponent       $sso
     * @property-read DaDataComponent    $dadata
     * @property-read DsfComponent       $dsf
     * @property-read StorageContainer   $storageContainer
     * @property-read DealerPollMonitoring  $dealerPollMonitoring
     */
    trait BaseApplication
    {
    }

    /**
     * Class Yii
     */
    class Yii extends \yii\BaseYii
    {
        /**
         * @var WebApplication|ConsoleApplication the application instance
         */
        public static $app;
    }

    /**
     * @property Formatter $formatter
     */
    class WebApplication extends yii\web\Application
    {
        use BaseApplication;
    }

    /**
     */
    class ConsoleApplication extends yii\console\Application
    {
        use BaseApplication;
    }
}
