<?php

namespace app\tests\unit\models;

use app\models\User;
use PHPUnit\Framework\TestCase;
use yii\test\ActiveFixture;

/**
 * Class UserTest
 * @method ActiveFixture getFixture(string $name)
 */
class UserTest extends TestCase
{
    public function testTableName()
    {
        $this->assertEquals('user', User::tableName());
    }

    public function testFindIdentityByAccessToken()
    {
        $this->assertNull(User::findIdentityByAccessToken(null, null));
    }

    public function testGenerateAuthKey()
    {
        $model = new User();
        $this->assertNull($model->auth_key);
        $model->generateAuthKey();
        $this->assertNotNull($model->auth_key);
    }

    public function testGenerateAccessToken()
    {
        $model = new User();
        $this->assertNull($model->access_token);
        $model->generateAccessToken();
        $this->assertNotNull($model->access_token);
    }
}
