<?php

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'tests');

require_once __DIR__.'/../../vendor/autoload.php';
require_once __DIR__.'/../../vendor/yiisoft/yii2/Yii.php';

$application = new yii\web\Application(require __DIR__.'/../_config/unit.php');
