<?php

/**
 * Created by PhpStorm.
 * User: yakov
 * Date: 28.06.2018
 * Time: 12:19
 */

namespace app\tests\unit\rbac;

use app\models\Position;
use app\models\User;
use app\tests\fixtures\UserFixture;
use PHPUnit\Framework\TestCase;
use yii\test\FixtureTrait;

/**
 * Class RoleTestTest
 * @package app\tests\unit\rbac
 */
class RoleTestTest extends TestCase
{
    use FixtureTrait;
    
    /** @var User */
    public $user;
    
    public function fixtures()
    {
        return [
            'users' => UserFixture::class,
        ];
    }
    
    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function setUp()
    {
        $this->loadFixtures();
        $this->user = $this->getFixture('users')->getModel(User::ROLE_TEST);
    }
    
    public function tearDown()
    {
        $this->unloadFixtures();
    }
    
    public function dataSet()
    {
        return [
            [User::DASHBOARD, false],
            [User::ACCESS_EDIT, false],
        ];
    }
    
    /**
     * @param $permission
     * @param $result
     *
     * @throws \yii\base\InvalidConfigException
     * @dataProvider  dataSet
     */
    public function testPermission($permission, $result)
    {
        $this->assertEquals($result, \Yii::$app->authManager->checkAccess($this->user->id, $permission));
    }
}
