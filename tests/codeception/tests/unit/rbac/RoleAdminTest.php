<?php

namespace app\tests\unit\rbac;

use app\models\Position;
use app\models\User;
use app\tests\fixtures\UserFixture;
use PHPUnit\Framework\TestCase;
use yii\test\ActiveFixture;
use yii\test\FixtureTrait;

/**
 * Class RoleAdminPermissionTest
 * @package app\tests\unit\rbac
 * @method ActiveFixture getFixture(string $name)
 */
class RoleAdminTest extends TestCase
{
    use FixtureTrait;
    
    /** @var User */
    public $user;
    
    public function fixtures()
    {
        return [
            'users' => UserFixture::class,
        ];
    }
    
    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function setUp()
    {
        $this->loadFixtures();
        $this->user = $this->getFixture('users')->getModel(User::ROLE_ADMIN);
    }
    
    public function tearDown()
    {
        $this->unloadFixtures();
    }
    
    public function dataSet()
    {
        return [
            [User::DASHBOARD],
            [User::ACCESS_EDIT],
        ];
    }
    
    /**
     * @param $permission
     *
     * @throws \yii\base\InvalidConfigException
     * @dataProvider  dataSet
     */
    public function testPermission($permission)
    {
        $this->assertTrue(
            \Yii::$app->authManager->checkAccess($this->user->id, $permission),
            'Failed check permission: ' . $permission
        );
    }
}
