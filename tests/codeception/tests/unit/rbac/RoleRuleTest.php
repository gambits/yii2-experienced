<?php

namespace app\tests\unit\rbac;

use app\models\Position;
use app\models\User;
use app\rbac\RoleRule;
use app\tests\fixtures\UserFixture;
use PHPUnit\Framework\TestCase;
use yii\rbac\Item;
use yii\test\ActiveFixture;
use yii\test\FixtureTrait;

/**
 * Class RoleRuleTest
 * @package app\tests\unit\rbac
 * @method ActiveFixture getFixture(string $name)
 */
class RoleRuleTest extends TestCase
{
    use FixtureTrait;
    
    public function fixtures()
    {
        return [
            'users' => UserFixture::class,
        ];
    }

    public function setUp()
    {
        $this->loadFixtures();
    }

    public function tearDown()
    {
        $this->unloadFixtures();
    }

    public function roles()
    {
        return [
            [User::ROLE_ADMIN, User::ROLE_TEST],
        ];
    }

    /**
     * @dataProvider roles
     * @param $role
     */
    public function testRole($role)
    {
        $user = $this->getFixture('users')->getModel($role);
        $this->assertNotNull($user);
        $rule = new RoleRule();
        $roleItem = new Item(['name' => $role]);
        $this->assertTrue($rule->execute($user->id, $roleItem, []));
    }

    /**
     * @dataProvider roles
     * @param $role
     */
    public function testRoleAsWithAuthManager($role)
    {
        $user = $this->getFixture('users')->getModel($role);
        $this->assertNotNull($user);
        $this->assertTrue(\Yii::$app->authManager->checkAccess($user->id, $role));
    }
}
