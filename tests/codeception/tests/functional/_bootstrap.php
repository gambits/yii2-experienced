<?php

defined('YII_DEBUG') or define('YII_DEBUG', false);
defined('YII_ENV') or define('YII_ENV', 'tests');

require_once __DIR__.'/../../vendor/autoload.php';
require_once __DIR__.'/../../vendor/yiisoft/yii2/Yii.php';

new \yii\web\Application(require(dirname(__DIR__) . '/_config/acceptance.php'));
