<?php

namespace app\tests\fixtures;

use yii\test\ActiveFixture;

class PollFixture extends DisabledFkFixture
{
    public $depends = [
        PollAnswerFixture::class,
        PollBlockFixture::class,
        PollQuestionFixture::class,
    ];
    public $modelClass = 'app\modules\poll\models\Poll';
    public $dataFile = '@tests/fixtures/data/poll.php';
}
