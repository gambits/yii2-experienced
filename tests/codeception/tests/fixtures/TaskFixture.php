<?php

namespace app\tests\fixtures;

use yii\test\ActiveFixture;

class TaskFixture extends DisabledFkFixture
{
    public $depends = [
        TaskStageFixture::class,
    ];
    public $modelClass = 'app\models\Task';
    public $dataFile = '@tests/fixtures/data/task.php';
}
