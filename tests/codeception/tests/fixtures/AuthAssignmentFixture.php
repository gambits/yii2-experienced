<?php

namespace app\tests\fixtures;

use yii\test\ActiveFixture;

class AuthAssignmentFixture extends DisabledFkFixture
{
    public $modelClass = 'app\models\AuthAssignment';
    public $dataFile = '@tests/fixtures/data/auth_assignment.php';
}
