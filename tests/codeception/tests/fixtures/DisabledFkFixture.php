<?php

namespace app\tests\fixtures;

use Yii;
use yii\test\ActiveFixture;

/**
 * Включаем и отключаем проверки внешних ключей
 *
 * Class DisabledFkFixture
 */
class DisabledFkFixture extends ActiveFixture
{
    public function beforeLoad()
    {
        Yii::$app->db->createCommand("SET FOREIGN_KEY_CHECKS=0")->execute();
    }
    
    public function afterLoad()
    {
        Yii::$app->db->createCommand("SET FOREIGN_KEY_CHECKS=1")->execute();
    }
    
    public function beforeUnload()
    {
        Yii::$app->db->createCommand("SET FOREIGN_KEY_CHECKS=0")->execute();
    }
    
    public function afterUnload()
    {
        Yii::$app->db->createCommand("SET FOREIGN_KEY_CHECKS=1")->execute();
    }
}
