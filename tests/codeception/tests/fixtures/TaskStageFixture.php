<?php

namespace app\tests\fixtures;

use yii\test\ActiveFixture;

class TaskStageFixture extends DisabledFkFixture
{
    public $modelClass = 'app\models\TaskStage';
    public $dataFile = '@tests/fixtures/data/task_stage.php';
}
