<?php

namespace app\tests\fixtures;

use yii\test\ActiveFixture;

class AuthRuleFixture extends DisabledFkFixture
{
    public $modelClass = 'app\models\AuthRule';
    public $dataFile = '@tests/fixtures/data/auth_rule.php';
}
