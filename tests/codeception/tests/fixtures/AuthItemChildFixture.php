<?php

namespace app\tests\fixtures;

use yii\test\ActiveFixture;

class AuthItemChildFixture extends DisabledFkFixture
{
    public $modelClass = 'app\models\AuthItemChild';
    public $dataFile = '@tests/fixtures/data/auth_item_child.php';
}
