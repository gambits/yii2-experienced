<?php

namespace app\tests\fixtures;

use yii\test\ActiveFixture;

class PositionFixture extends DisabledFkFixture
{
    public $modelClass = 'app\models\Position';
    public $dataFile = '@tests/fixtures/data/position.php';
}
