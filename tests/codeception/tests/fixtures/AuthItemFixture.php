<?php

namespace app\tests\fixtures;

use yii\test\ActiveFixture;

class AuthItemFixture extends DisabledFkFixture
{
    public $depends = [
        AuthRuleFixture::class,
        AuthItemChildFixture::class,
        AuthAssignmentFixture::class,
    ];
    public $modelClass = 'app\models\AuthItem';
    public $dataFile = '@tests/fixtures/data/auth_item.php';
}
