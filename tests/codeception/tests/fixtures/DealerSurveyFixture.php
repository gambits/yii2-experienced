<?php

namespace app\tests\fixtures;

use yii\test\ActiveFixture;

class DealerSurveyFixture extends ActiveFixture
{
    public $modelClass = 'app\modules\DealerPoll\models\DealerSurvey';
    public $dataFile = '@tests/fixtures/data/dealer_survey.php';
}
