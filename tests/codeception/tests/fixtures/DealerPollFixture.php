<?php

namespace app\tests\fixtures;

class DealerPollFixture extends DisabledFkFixture
{
    public $depends = [
        DealerSurveyFixture::class,
    ];
    public $modelClass = 'app\modules\DealerPoll\models\DealerPoll';
    public $dataFile = '@tests/fixtures/data/dealer_poll.php';
}
