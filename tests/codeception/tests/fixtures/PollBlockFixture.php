<?php

namespace app\tests\fixtures;

use yii\test\ActiveFixture;

class PollBlockFixture extends DisabledFkFixture
{
    public $modelClass = 'app\modules\poll\models\PollBlock';
    public $dataFile = '@tests/fixtures/data/poll_block.php';
}
