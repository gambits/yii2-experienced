<?php

namespace app\tests\fixtures;

use yii\test\ActiveFixture;

class WorkflowFixture extends DisabledFkFixture
{
    public $modelClass = 'app\models\Workflow';
    public $dataFile = '@tests/fixtures/data/workflow.php';
}
