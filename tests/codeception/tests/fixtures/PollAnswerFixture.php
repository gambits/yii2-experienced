<?php

namespace app\tests\fixtures;

use yii\test\ActiveFixture;

class PollAnswerFixture extends DisabledFkFixture
{
    public $modelClass = 'app\modules\poll\models\PollAnswer';
    public $dataFile = '@tests/fixtures/data/poll_answer.php';
}
