<?php

namespace app\tests\fixtures;

use Yii;

class UserFixture extends DisabledFkFixture
{
    public $depends = [
        PositionFixture::class,
        AuthItemFixture::class,
        AuthItemChildFixture::class,
        AuthRuleFixture::class,
        AuthAssignmentFixture::class,
    ];
    public $modelClass = 'app\models\User';
    public $dataFile = '@tests/fixtures/data/user.php';
    
    public function beforeLoad()
    {
        Yii::$app->db->createCommand("SET FOREIGN_KEY_CHECKS=0")->execute();
    }
    
    public function afterLoad()
    {
        Yii::$app->db->createCommand("SET FOREIGN_KEY_CHECKS=1")->execute();
    }
    
    public function beforeUnload()
    {
        Yii::$app->db->createCommand("SET FOREIGN_KEY_CHECKS=0")->execute();
    }
    
    public function afterUnload()
    {
        Yii::$app->db->createCommand("SET FOREIGN_KEY_CHECKS=1")->execute();
    }
}
