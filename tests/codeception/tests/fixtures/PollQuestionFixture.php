<?php

namespace app\tests\fixtures;

use yii\test\ActiveFixture;

class PollQuestionFixture extends DisabledFkFixture
{
    public $modelClass = 'app\modules\poll\models\PollQuestion';
    public $dataFile = '@tests/fixtures/data/poll_question.php';
}
