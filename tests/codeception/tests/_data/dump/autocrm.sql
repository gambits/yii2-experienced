/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 10.4.11-MariaDB-1:10.4.11+maria~bionic : Database - autocrm
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`autocrm_test` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `autocrm_test`;

/*Table structure for table `aeb_brand_mapping` */

DROP TABLE IF EXISTS `aeb_brand_mapping`;

CREATE TABLE `aeb_brand_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) NOT NULL,
  `name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `aeb_city_mapping` */

DROP TABLE IF EXISTS `aeb_city_mapping`;

CREATE TABLE `aeb_city_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `aeb_region_upload_history_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `city_mapping_upload_idx` (`aeb_region_upload_history_id`),
  CONSTRAINT `city_mapping_upload_idx` FOREIGN KEY (`aeb_region_upload_history_id`) REFERENCES `aeb_region_upload_history` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=274 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `aeb_model_mapping` */

DROP TABLE IF EXISTS `aeb_model_mapping`;

CREATE TABLE `aeb_model_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `aeb_region_upload_history_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `model_mapping_upload_idx` (`aeb_region_upload_history_id`),
  CONSTRAINT `model_mapping_upload_idx` FOREIGN KEY (`aeb_region_upload_history_id`) REFERENCES `aeb_region_upload_history` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=404 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `aeb_region` */

DROP TABLE IF EXISTS `aeb_region`;

CREATE TABLE `aeb_region` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `federal_district_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `segment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3804431 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `aeb_region_mapping` */

DROP TABLE IF EXISTS `aeb_region_mapping`;

CREATE TABLE `aeb_region_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `region_id` int(11) NOT NULL,
  `name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `aeb_region_upload_history_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `region_mapping_upload_idx` (`aeb_region_upload_history_id`),
  CONSTRAINT `region_mapping_upload_idx` FOREIGN KEY (`aeb_region_upload_history_id`) REFERENCES `aeb_region_upload_history` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `aeb_region_upload_history` */

DROP TABLE IF EXISTS `aeb_region_upload_history`;

CREATE TABLE `aeb_region_upload_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `import_status` int(11) NOT NULL DEFAULT 0,
  `imported_success` int(11) NOT NULL DEFAULT 0,
  `imported_errors` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `aeb_temporary_aeb_region_data` */

DROP TABLE IF EXISTS `aeb_temporary_aeb_region_data`;

CREATE TABLE `aeb_temporary_aeb_region_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `segment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `federal_district` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `year` int(11) NOT NULL,
  `data_1` int(11) DEFAULT NULL,
  `data_2` int(11) DEFAULT NULL,
  `data_3` int(11) DEFAULT NULL,
  `data_4` int(11) DEFAULT NULL,
  `data_5` int(11) DEFAULT NULL,
  `data_6` int(11) DEFAULT NULL,
  `data_7` int(11) DEFAULT NULL,
  `data_8` int(11) DEFAULT NULL,
  `data_9` int(11) DEFAULT NULL,
  `data_10` int(11) DEFAULT NULL,
  `data_11` int(11) DEFAULT NULL,
  `data_12` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1305890 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `bodies` */

DROP TABLE IF EXISTS `bodies`;

CREATE TABLE `bodies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `tradein_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

/*Table structure for table `brands` */

DROP TABLE IF EXISTS `brands`;

CREATE TABLE `brands` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `name_eng` varchar(255) NOT NULL,
  `name_market` varchar(255) NOT NULL COMMENT 'Торговое наименование',
  `logo` varchar(64) NOT NULL,
  `importer_db_name` varchar(32) NOT NULL,
  `host` varchar(128) DEFAULT NULL,
  `token` char(16) DEFAULT NULL,
  `ecm_id` int(11) DEFAULT NULL,
  `is_supported` tinyint(1) NOT NULL DEFAULT 0,
  `origin_id` int(11) DEFAULT NULL COMMENT 'ссылка на таблицу eqt_car_mark',
  `is_vin_manufacturer` int(1) NOT NULL DEFAULT 0 COMMENT 'Ипользуется ли вин производителя',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1439 DEFAULT CHARSET=utf8;

/*Table structure for table `car_characteristic` */

DROP TABLE IF EXISTS `car_characteristic`;

CREATE TABLE `car_characteristic` (
  `id_car_characteristic` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Название',
  `id_parent` int(8) DEFAULT NULL COMMENT 'Родитель',
  `id_car_type` int(8) NOT NULL COMMENT 'Тип авто',
  `is_main` tinyint(1) DEFAULT NULL,
  `origin_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_car_characteristic`),
  KEY `id_car_type` (`id_car_type`),
  KEY `car_characteristic_origin` (`origin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2406 DEFAULT CHARSET=utf8 COMMENT='Характеристики автомобилей';

/*Table structure for table `car_characteristic_value` */

DROP TABLE IF EXISTS `car_characteristic_value`;

CREATE TABLE `car_characteristic_value` (
  `id_car_characteristic_value` int(8) NOT NULL AUTO_INCREMENT,
  `value` varchar(255) NOT NULL COMMENT 'Значение',
  `unit` varchar(255) DEFAULT NULL COMMENT 'Еденица измерения',
  `id_car_characteristic` int(8) NOT NULL COMMENT 'Характеристика',
  `id_car_modification` int(8) NOT NULL COMMENT 'Модификация Авто',
  `is_visible` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Видимость',
  `id_car_type` int(8) NOT NULL COMMENT 'Тип авто',
  `origin_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_car_characteristic_value`),
  UNIQUE KEY `id_characteristic` (`id_car_characteristic`,`id_car_modification`),
  UNIQUE KEY `id_car_characteristic` (`id_car_characteristic`,`id_car_modification`,`id_car_type`),
  KEY `id_car_type` (`id_car_type`),
  KEY `car_characteristic_value_origin` (`origin_id`),
  KEY `id_car_modification` (`id_car_modification`)
) ENGINE=InnoDB AUTO_INCREMENT=36408386 DEFAULT CHARSET=utf8 COMMENT='Значения характеристик автомобиля';

/*Table structure for table `car_generation` */

DROP TABLE IF EXISTS `car_generation`;

CREATE TABLE `car_generation` (
  `id_car_generation` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `model_id` int(11) DEFAULT NULL COMMENT 'при выборке этот атрибут использовать нельзя',
  `year_begin` varchar(255) DEFAULT NULL COMMENT 'Год начала выпуска',
  `year_end` varchar(255) DEFAULT NULL COMMENT 'Год окончания выпуска',
  `is_visible` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Видимость',
  `id_car_type` int(8) NOT NULL COMMENT 'Тип авто',
  `is_recent` tinyint(1) NOT NULL,
  `origin_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_car_generation`),
  KEY `id_car_type` (`id_car_type`),
  KEY `car_generation_origin` (`origin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12880 DEFAULT CHARSET=utf8 COMMENT='Поколения Моделей';

/*Table structure for table `car_modification` */

DROP TABLE IF EXISTS `car_modification`;

CREATE TABLE `car_modification` (
  `id_car_modification` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `id_car_serie` int(11) NOT NULL COMMENT 'Серия автомобиля',
  `name` varchar(255) NOT NULL COMMENT 'Название модификции',
  `start_production_year` int(8) DEFAULT NULL COMMENT 'Год начала производства',
  `end_production_year` int(8) DEFAULT NULL COMMENT 'Год конца производства',
  `drive_type` int(1) DEFAULT NULL,
  `engine_type` int(1) DEFAULT NULL,
  `transmission_type` int(1) DEFAULT NULL,
  `is_visible` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Видимость',
  `id_car_type` int(8) NOT NULL COMMENT 'Тип авто',
  `price_min` int(8) DEFAULT NULL COMMENT 'Минимальная цена',
  `price_max` int(8) DEFAULT NULL COMMENT 'Максимальная цена',
  `package_code` varchar(255) DEFAULT NULL,
  `is_recent` tinyint(1) NOT NULL DEFAULT 1,
  `origin_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_car_modification`),
  KEY `id_car_serie` (`id_car_serie`),
  KEY `id_car_type` (`id_car_type`),
  KEY `car_modification_origin` (`origin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=243478 DEFAULT CHARSET=utf8 COMMENT='Модификации автомобилей';

/*Table structure for table `car_serie` */

DROP TABLE IF EXISTS `car_serie`;

CREATE TABLE `car_serie` (
  `id_car_serie` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `model_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL COMMENT 'Название серии',
  `is_visible` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Видимость',
  `id_car_generation` int(8) DEFAULT NULL COMMENT 'Поколение',
  `id_car_type` int(8) NOT NULL COMMENT 'Тип авто',
  `body_id` int(11) DEFAULT NULL,
  `is_recent` tinyint(1) NOT NULL,
  `origin_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_car_serie`),
  KEY `name` (`name`),
  KEY `id_car_type` (`id_car_type`),
  KEY `fk_car_serie_body` (`body_id`),
  KEY `fk_car_serie_model` (`model_id`),
  KEY `car_serie_origin` (`origin_id`),
  CONSTRAINT `fk_car_serie_body` FOREIGN KEY (`body_id`) REFERENCES `bodies` (`id`),
  CONSTRAINT `fk_car_serie_model` FOREIGN KEY (`model_id`) REFERENCES `models` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55645 DEFAULT CHARSET=utf8 COMMENT='Cерии автомобилей';

/*Table structure for table `car_type` */

DROP TABLE IF EXISTS `car_type`;

CREATE TABLE `car_type` (
  `id_car_type` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Название',
  PRIMARY KEY (`id_car_type`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Автомобильный сайт';

/*Table structure for table `cities` */

DROP TABLE IF EXISTS `cities`;

CREATE TABLE `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `timezone` varchar(255) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `is_regional_center` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`),
  KEY `region_id` (`region_id`),
  CONSTRAINT `cities_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `cities_ibfk_2` FOREIGN KEY (`region_id`) REFERENCES `regions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2615 DEFAULT CHARSET=utf8;

/*Table structure for table `configurable_reference_params_weights` */

DROP TABLE IF EXISTS `configurable_reference_params_weights`;

CREATE TABLE `configurable_reference_params_weights` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `paramId` varchar(32) NOT NULL,
  `fieldId` int(10) unsigned NOT NULL,
  `weight` int(10) unsigned DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`paramId`,`fieldId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `configurable_reference_vehicle` */

DROP TABLE IF EXISTS `configurable_reference_vehicle`;

CREATE TABLE `configurable_reference_vehicle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `modelId` int(11) DEFAULT NULL,
  `brandId` int(10) unsigned DEFAULT NULL,
  `productionYear` year(4) DEFAULT NULL,
  `transmissionType` smallint(6) DEFAULT NULL,
  `engineType` smallint(6) DEFAULT NULL,
  `regionId` int(11) DEFAULT NULL,
  `generationId` int(10) unsigned DEFAULT NULL,
  `cache` bit(6) NOT NULL,
  `weight` int(10) unsigned DEFAULT 0,
  `paramId` varchar(32) NOT NULL,
  `paramValue` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `params` (`paramId`,`modelId`,`brandId`,`productionYear`,`transmissionType`,`engineType`,`regionId`)
) ENGINE=InnoDB AUTO_INCREMENT=1528 DEFAULT CHARSET=utf8;

/*Table structure for table `countries` */

DROP TABLE IF EXISTS `countries`;

CREATE TABLE `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `phone_code` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=843 DEFAULT CHARSET=utf8;

/*Table structure for table `cover` */

DROP TABLE IF EXISTS `cover`;

CREATE TABLE `cover` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `crm_instructions` */

DROP TABLE IF EXISTS `crm_instructions`;

CREATE TABLE `crm_instructions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userGroupCode` varchar(32) NOT NULL,
  `brandId` int(11) DEFAULT NULL,
  `path` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `isForMobile` tinyint(1) NOT NULL DEFAULT 0,
  `excludedId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=245 DEFAULT CHARSET=utf8;

/*Table structure for table `crms` */

DROP TABLE IF EXISTS `crms`;

CREATE TABLE `crms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(16) NOT NULL,
  `hostname` varchar(32) DEFAULT NULL,
  `db_name` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=879 DEFAULT CHARSET=utf8;

/*Table structure for table `currency` */

DROP TABLE IF EXISTS `currency`;

CREATE TABLE `currency` (
  `number_code` char(3) NOT NULL,
  `string_code` char(3) NOT NULL,
  `name` varchar(255) NOT NULL,
  `short_name` varchar(5) NOT NULL,
  UNIQUE KEY `number_code` (`number_code`),
  UNIQUE KEY `string_code` (`string_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `discount_types` */

DROP TABLE IF EXISTS `discount_types`;

CREATE TABLE `discount_types` (
  `discount_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) NOT NULL,
  `discount_type_name` varchar(255) DEFAULT NULL,
  `discount_type_disabled` tinyint(1) NOT NULL DEFAULT 0,
  `discount_type_unconditional` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`discount_type_id`),
  KEY `brand_id_discount_type_unconditional` (`discount_type_unconditional`,`brand_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

/*Table structure for table `dsf_queue` */

DROP TABLE IF EXISTS `dsf_queue`;

CREATE TABLE `dsf_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `job` blob NOT NULL,
  `pushed_at` int(11) NOT NULL,
  `ttr` int(11) NOT NULL,
  `delay` int(11) NOT NULL DEFAULT 0,
  `priority` int(11) unsigned NOT NULL DEFAULT 1024,
  `reserved_at` int(11) DEFAULT NULL,
  `attempt` int(11) DEFAULT NULL,
  `done_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `channel` (`channel`),
  KEY `reserved_at` (`reserved_at`),
  KEY `priority` (`priority`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `dsf_upload` */

DROP TABLE IF EXISTS `dsf_upload`;

CREATE TABLE `dsf_upload` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('selectel') COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=287 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `eqt_brand_logo` */

DROP TABLE IF EXISTS `eqt_brand_logo`;

CREATE TABLE `eqt_brand_logo` (
  `brand_id` int(11) unsigned NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`brand_id`),
  CONSTRAINT `eqt_fk_brand_logo_brand` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `eqt_car_characteristic` */

DROP TABLE IF EXISTS `eqt_car_characteristic`;

CREATE TABLE `eqt_car_characteristic` (
  `id_car_characteristic` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) DEFAULT NULL,
  `id_parent` int(8) DEFAULT NULL,
  `date_create` int(10) unsigned DEFAULT NULL,
  `date_update` int(10) unsigned DEFAULT NULL,
  `id_car_type` int(8) NOT NULL,
  PRIMARY KEY (`id_car_characteristic`),
  KEY `name` (`name`,`id_parent`,`id_car_type`),
  KEY `id_car_type` (`id_car_type`)
) ENGINE=InnoDB AUTO_INCREMENT=1585 DEFAULT CHARSET=utf8 COMMENT='Характеристики автомобилей';

/*Table structure for table `eqt_car_characteristic_value` */

DROP TABLE IF EXISTS `eqt_car_characteristic_value`;

CREATE TABLE `eqt_car_characteristic_value` (
  `id_car_characteristic_value` int(8) NOT NULL AUTO_INCREMENT,
  `id_car_modification` int(8) NOT NULL,
  `id_car_characteristic` int(8) NOT NULL,
  `value` varchar(300) DEFAULT NULL,
  `unit` varchar(255) DEFAULT NULL COMMENT 'Еденица измерения',
  `date_create` int(10) unsigned DEFAULT NULL,
  `date_update` int(10) unsigned DEFAULT NULL,
  `id_car_type` int(8) NOT NULL,
  PRIMARY KEY (`id_car_characteristic_value`),
  UNIQUE KEY `id_characteristic` (`id_car_characteristic`,`id_car_modification`),
  UNIQUE KEY `id_car_characteristic` (`id_car_characteristic`,`id_car_modification`,`id_car_type`),
  KEY `id_car_type` (`id_car_type`),
  KEY `id_car_type_2` (`id_car_type`),
  KEY `value` (`value`(255),`id_car_type`),
  KEY `value_2` (`value`(255)),
  KEY `fk_car_characteristic_value_car_modification` (`id_car_modification`)
) ENGINE=InnoDB AUTO_INCREMENT=35985427 DEFAULT CHARSET=utf8 COMMENT='Значения характеристик автомобиля';

/*Table structure for table `eqt_car_equipment` */

DROP TABLE IF EXISTS `eqt_car_equipment`;

CREATE TABLE `eqt_car_equipment` (
  `id_car_equipment` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `id_car_modification` int(8) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price_min` int(8) DEFAULT NULL COMMENT 'Цена от',
  `year` int(8) DEFAULT NULL,
  `date_create` int(10) unsigned NOT NULL,
  `date_update` int(10) unsigned NOT NULL,
  `id_car_type` int(8) NOT NULL,
  PRIMARY KEY (`id_car_equipment`),
  KEY `id_car_type` (`id_car_type`),
  KEY `date_delete` (`id_car_type`),
  KEY `fk_car_equipment_car_modification` (`id_car_modification`)
) ENGINE=InnoDB AUTO_INCREMENT=23221 DEFAULT CHARSET=utf8 COMMENT='Комплектации';

/*Table structure for table `eqt_car_generation` */

DROP TABLE IF EXISTS `eqt_car_generation`;

CREATE TABLE `eqt_car_generation` (
  `id_car_generation` int(8) NOT NULL AUTO_INCREMENT,
  `id_car_model` int(8) NOT NULL,
  `name` varchar(255) NOT NULL,
  `year_begin` varchar(255) DEFAULT NULL,
  `year_end` varchar(255) DEFAULT NULL,
  `date_create` int(10) unsigned NOT NULL,
  `date_update` int(10) unsigned DEFAULT NULL,
  `id_car_type` int(8) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_car_generation`),
  KEY `id_car_model` (`id_car_model`),
  KEY `id_car_type` (`id_car_type`)
) ENGINE=InnoDB AUTO_INCREMENT=108246 DEFAULT CHARSET=utf8 COMMENT='Поколения Моделей';

/*Table structure for table `eqt_car_mark` */

DROP TABLE IF EXISTS `eqt_car_mark`;

CREATE TABLE `eqt_car_mark` (
  `id_car_mark` int(8) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) NOT NULL,
  `name_rus` varchar(255) DEFAULT NULL,
  `date_create` int(10) unsigned DEFAULT NULL,
  `date_update` int(10) unsigned DEFAULT NULL,
  `id_car_type` int(8) NOT NULL,
  PRIMARY KEY (`id_car_mark`),
  KEY `id_car_type` (`id_car_type`)
) ENGINE=InnoDB AUTO_INCREMENT=4150 DEFAULT CHARSET=utf8 COMMENT='Марки автомобилей';

/*Table structure for table `eqt_car_model` */

DROP TABLE IF EXISTS `eqt_car_model`;

CREATE TABLE `eqt_car_model` (
  `id_car_model` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `id_car_mark` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `name_rus` varchar(255) DEFAULT NULL,
  `date_create` int(10) unsigned DEFAULT NULL,
  `date_update` int(10) unsigned DEFAULT NULL,
  `id_car_type` int(8) NOT NULL,
  PRIMARY KEY (`id_car_model`),
  KEY `name` (`name`),
  KEY `id_car_mark` (`id_car_mark`),
  KEY `id_car_type` (`id_car_type`)
) ENGINE=InnoDB AUTO_INCREMENT=22673 DEFAULT CHARSET=utf8 COMMENT='Модели автомобилей';

/*Table structure for table `eqt_car_modification` */

DROP TABLE IF EXISTS `eqt_car_modification`;

CREATE TABLE `eqt_car_modification` (
  `id_car_modification` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `id_car_serie` int(11) NOT NULL,
  `id_car_model` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `start_production_year` int(8) DEFAULT NULL,
  `end_production_year` int(8) DEFAULT NULL,
  `date_create` int(10) unsigned DEFAULT NULL,
  `date_update` int(10) unsigned DEFAULT NULL,
  `id_car_type` int(8) NOT NULL,
  PRIMARY KEY (`id_car_modification`),
  KEY `id_car_model` (`id_car_model`),
  KEY `id_car_serie` (`id_car_serie`),
  KEY `id_car_type` (`id_car_type`)
) ENGINE=InnoDB AUTO_INCREMENT=241687 DEFAULT CHARSET=utf8 COMMENT='Модификации автомобилей';

/*Table structure for table `eqt_car_option` */

DROP TABLE IF EXISTS `eqt_car_option`;

CREATE TABLE `eqt_car_option` (
  `id_car_option` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `id_parent` int(8) DEFAULT NULL,
  `date_create` int(10) unsigned NOT NULL,
  `date_update` int(10) unsigned NOT NULL,
  `id_car_type` int(8) NOT NULL,
  PRIMARY KEY (`id_car_option`),
  KEY `id_car_type` (`id_car_type`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=37343 DEFAULT CHARSET=utf8 COMMENT='Опции';

/*Table structure for table `eqt_car_option_value` */

DROP TABLE IF EXISTS `eqt_car_option_value`;

CREATE TABLE `eqt_car_option_value` (
  `id_car_option_value` int(8) NOT NULL AUTO_INCREMENT,
  `id_car_option` int(8) NOT NULL,
  `id_car_equipment` int(8) NOT NULL,
  `is_base` tinyint(1) NOT NULL DEFAULT 1,
  `date_create` int(10) unsigned NOT NULL,
  `date_update` int(10) unsigned DEFAULT NULL,
  `id_car_type` int(8) NOT NULL,
  PRIMARY KEY (`id_car_option_value`),
  UNIQUE KEY `id_car_option` (`id_car_option`,`id_car_equipment`,`id_car_type`),
  KEY `id_car_type` (`id_car_type`),
  KEY `date_delete` (`id_car_type`),
  KEY `fk_car_option_value_car_equipment` (`id_car_equipment`)
) ENGINE=InnoDB AUTO_INCREMENT=2814570 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='Значения опций';

/*Table structure for table `eqt_car_serie` */

DROP TABLE IF EXISTS `eqt_car_serie`;

CREATE TABLE `eqt_car_serie` (
  `id_car_serie` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `id_car_model` int(8) NOT NULL,
  `id_car_generation` int(8) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `date_create` int(10) unsigned DEFAULT NULL,
  `date_update` int(10) unsigned DEFAULT NULL,
  `id_car_type` int(8) NOT NULL,
  PRIMARY KEY (`id_car_serie`),
  KEY `name` (`name`),
  KEY `id_car_model` (`id_car_model`),
  KEY `id_car_type` (`id_car_type`),
  KEY `fk_car_serie_car_generation` (`id_car_generation`)
) ENGINE=InnoDB AUTO_INCREMENT=54058 DEFAULT CHARSET=utf8 COMMENT='Cерии автомобилей';

/*Table structure for table `eqt_car_type` */

DROP TABLE IF EXISTS `eqt_car_type`;

CREATE TABLE `eqt_car_type` (
  `id_car_type` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id_car_type`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='Автомобильный сайт';

/*Table structure for table `eqt_catalog_emplacement` */

DROP TABLE IF EXISTS `eqt_catalog_emplacement`;

CREATE TABLE `eqt_catalog_emplacement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NOT NULL,
  `serie_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `equipment_id` int(11) DEFAULT NULL,
  `is_main` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `eqt_fk_catalog_emplacement_model` (`model_id`),
  KEY `eqt_fk_catalog_emplacement_serie` (`serie_id`),
  KEY `eqt_fk_catalog_emplacement_color` (`color_id`),
  KEY `eqt_fk_catalog_emplacement_equipment` (`equipment_id`),
  CONSTRAINT `eqt_fk_catalog_emplacement_color` FOREIGN KEY (`color_id`) REFERENCES `eqt_color` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `eqt_fk_catalog_emplacement_equipment` FOREIGN KEY (`equipment_id`) REFERENCES `eqt_equipment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `eqt_fk_catalog_emplacement_model` FOREIGN KEY (`model_id`) REFERENCES `models` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `eqt_fk_catalog_emplacement_serie` FOREIGN KEY (`serie_id`) REFERENCES `car_serie` (`id_car_serie`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13659 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `eqt_catalog_foreshortening` */

DROP TABLE IF EXISTS `eqt_catalog_foreshortening`;

CREATE TABLE `eqt_catalog_foreshortening` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `order` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Table structure for table `eqt_catalog_image` */

DROP TABLE IF EXISTS `eqt_catalog_image`;

CREATE TABLE `eqt_catalog_image` (
  `emplacement_id` int(11) NOT NULL,
  `foreshortening_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `is_main` tinyint(1) NOT NULL DEFAULT 0,
  `is_serie_main` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`emplacement_id`,`foreshortening_id`),
  KEY `eqt_fk_catalog_image_foreshortening` (`foreshortening_id`),
  CONSTRAINT `eqt_fk_catalog_image_emplacement` FOREIGN KEY (`emplacement_id`) REFERENCES `eqt_catalog_emplacement` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `eqt_fk_catalog_image_foreshortening` FOREIGN KEY (`foreshortening_id`) REFERENCES `eqt_catalog_foreshortening` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `eqt_color` */

DROP TABLE IF EXISTS `eqt_color`;

CREATE TABLE `eqt_color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `common_color_id` int(11) DEFAULT NULL,
  `rgb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `eqt_fk_color_model` (`model_id`),
  KEY `eqt_fk_color_common_color` (`common_color_id`),
  CONSTRAINT `eqt_fk_color_common_color` FOREIGN KEY (`common_color_id`) REFERENCES `eqt_color` (`id`),
  CONSTRAINT `eqt_fk_color_model` FOREIGN KEY (`model_id`) REFERENCES `models` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15694 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='id =';

/*Table structure for table `eqt_equipment` */

DROP TABLE IF EXISTS `eqt_equipment`;

CREATE TABLE `eqt_equipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NOT NULL,
  `series_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `archive_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tech_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `origin_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `eqt_fk_equipment_model` (`model_id`),
  KEY `eqt_fk_equipment_series` (`series_id`),
  KEY `equipment-country-fk` (`country_id`),
  CONSTRAINT `eqt_fk_equipment_model` FOREIGN KEY (`model_id`) REFERENCES `models` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `eqt_fk_equipment_series` FOREIGN KEY (`series_id`) REFERENCES `car_serie` (`id_car_serie`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `equipment-country-fk` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12479 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `eqt_equipment_catalog_emplacement` */

DROP TABLE IF EXISTS `eqt_equipment_catalog_emplacement`;

CREATE TABLE `eqt_equipment_catalog_emplacement` (
  `catalog_emplacement_id` int(11) NOT NULL,
  `equipment_id` int(11) NOT NULL,
  PRIMARY KEY (`catalog_emplacement_id`,`equipment_id`),
  KEY `equipment_catalog_emplacement-equipment-fk` (`equipment_id`),
  CONSTRAINT `equipment_catalog_emplacement-catalog_emplacement-fk` FOREIGN KEY (`catalog_emplacement_id`) REFERENCES `eqt_catalog_emplacement` (`id`),
  CONSTRAINT `equipment_catalog_emplacement-equipment-fk` FOREIGN KEY (`equipment_id`) REFERENCES `eqt_equipment` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `eqt_equipment_country` */

DROP TABLE IF EXISTS `eqt_equipment_country`;

CREATE TABLE `eqt_equipment_country` (
  `country_id` int(11) NOT NULL,
  `equipment_id` int(11) NOT NULL,
  PRIMARY KEY (`country_id`,`equipment_id`),
  KEY `equipment_country-equipment-fk` (`equipment_id`),
  CONSTRAINT `equipment_country-country-fk` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`),
  CONSTRAINT `equipment_country-equipment-fk` FOREIGN KEY (`equipment_id`) REFERENCES `eqt_equipment` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `eqt_image` */

DROP TABLE IF EXISTS `eqt_image`;

CREATE TABLE `eqt_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serie_id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_main` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `color_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `eqt_fk_image_model` (`model_id`),
  KEY `eqt_fk_image_color` (`color_id`),
  KEY `eqt_fk_image_serie` (`serie_id`),
  CONSTRAINT `eqt_fk_image_color` FOREIGN KEY (`color_id`) REFERENCES `eqt_color` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `eqt_fk_image_model` FOREIGN KEY (`model_id`) REFERENCES `models` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `eqt_fk_image_serie` FOREIGN KEY (`serie_id`) REFERENCES `car_serie` (`id_car_serie`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `eqt_migration` */

DROP TABLE IF EXISTS `eqt_migration`;

CREATE TABLE `eqt_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `eqt_model_option` */

DROP TABLE IF EXISTS `eqt_model_option`;

CREATE TABLE `eqt_model_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NOT NULL,
  `option_group_id` int(11) NOT NULL,
  `model_option_tag_id` int(11) DEFAULT NULL,
  `name` varchar(1500) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `eqt_fk_model_option_model` (`model_id`),
  KEY `eqt_fk_model_option_option_group` (`option_group_id`),
  KEY `eqt_fk_model_option_model_option_tag` (`model_option_tag_id`),
  CONSTRAINT `eqt_fk_model_option_model` FOREIGN KEY (`model_id`) REFERENCES `models` (`id`),
  CONSTRAINT `eqt_fk_model_option_model_option_tag` FOREIGN KEY (`model_option_tag_id`) REFERENCES `eqt_model_option_tag` (`id`),
  CONSTRAINT `eqt_fk_model_option_option_group` FOREIGN KEY (`option_group_id`) REFERENCES `eqt_option_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=147013 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `eqt_model_option_tag` */

DROP TABLE IF EXISTS `eqt_model_option_tag`;

CREATE TABLE `eqt_model_option_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=739 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `eqt_offer` */

DROP TABLE IF EXISTS `eqt_offer`;

CREATE TABLE `eqt_offer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_id` int(10) unsigned DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `body_id` int(11) DEFAULT NULL,
  `modification_id` int(11) DEFAULT NULL,
  `equipment_id` int(11) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `skin_id` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `vin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cost` decimal(19,4) DEFAULT NULL,
  `additions_cost` decimal(19,4) DEFAULT NULL,
  `discount` decimal(19,4) DEFAULT NULL,
  `trade_in_cost` decimal(19,4) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `eqt_fk_offer_brand` (`brand_id`),
  KEY `eqt_fk_offer_model` (`model_id`),
  KEY `eqt_fk_offer_body` (`body_id`),
  KEY `eqt_fk_offer_modification` (`modification_id`),
  KEY `eqt_fk_offer_color` (`color_id`),
  KEY `eqt_fk_offer_skin` (`skin_id`),
  KEY `eqt_fk_offer_equipment` (`equipment_id`),
  CONSTRAINT `eqt_fk_offer_body` FOREIGN KEY (`body_id`) REFERENCES `bodies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `eqt_fk_offer_brand` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `eqt_fk_offer_color` FOREIGN KEY (`color_id`) REFERENCES `eqt_color` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `eqt_fk_offer_equipment` FOREIGN KEY (`equipment_id`) REFERENCES `eqt_equipment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `eqt_fk_offer_model` FOREIGN KEY (`model_id`) REFERENCES `models` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `eqt_fk_offer_modification` FOREIGN KEY (`modification_id`) REFERENCES `car_modification` (`id_car_modification`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `eqt_fk_offer_skin` FOREIGN KEY (`skin_id`) REFERENCES `eqt_skin` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `eqt_option` */

DROP TABLE IF EXISTS `eqt_option`;

CREATE TABLE `eqt_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equipment_id` int(11) NOT NULL,
  `model_option_id` int(11) NOT NULL,
  `name` varchar(1500) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `option_group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `equipment_id_model_option_id` (`equipment_id`,`model_option_id`),
  KEY `eqt_fk_option_equipment` (`equipment_id`),
  KEY `eqt_fk_option_option_group` (`option_group_id`),
  KEY `eqt_fk_option_model_option` (`model_option_id`),
  CONSTRAINT `eqt_fk_option_equipment` FOREIGN KEY (`equipment_id`) REFERENCES `eqt_equipment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `eqt_fk_option_model_option` FOREIGN KEY (`model_option_id`) REFERENCES `eqt_model_option` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `eqt_fk_option_option_group` FOREIGN KEY (`option_group_id`) REFERENCES `eqt_option_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=730642 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `eqt_option_group` */

DROP TABLE IF EXISTS `eqt_option_group`;

CREATE TABLE `eqt_option_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `eqt_fk_option_group_brand` (`brand_id`),
  CONSTRAINT `eqt_fk_option_group_brand` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1515 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `eqt_skin` */

DROP TABLE IF EXISTS `eqt_skin`;

CREATE TABLE `eqt_skin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) DEFAULT NULL,
  `common_skin_id` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `eqt_fk_skin_model` (`model_id`),
  KEY `eqt_fk_skin_common_skin` (`common_skin_id`),
  CONSTRAINT `eqt_fk_skin_common_skin` FOREIGN KEY (`common_skin_id`) REFERENCES `eqt_skin` (`id`),
  CONSTRAINT `eqt_fk_skin_model` FOREIGN KEY (`model_id`) REFERENCES `models` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5511 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `federal_districts` */

DROP TABLE IF EXISTS `federal_districts`;

CREATE TABLE `federal_districts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `short_name` varchar(255) DEFAULT NULL,
  `okato` char(2) DEFAULT NULL,
  `status` smallint(6) NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_federal_districts_country_id` (`country_id`),
  CONSTRAINT `fk_federal_districts_country_id` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Table structure for table `mbr_sap_model` */

DROP TABLE IF EXISTS `mbr_sap_model`;

CREATE TABLE `mbr_sap_model` (
  `model_id` int(11) NOT NULL,
  `generation_id` int(11) NOT NULL,
  `serie_id` int(11) NOT NULL,
  `modification_id` int(11) NOT NULL,
  `equipment_id` int(11) NOT NULL DEFAULT 0,
  `model_code` varchar(255) NOT NULL,
  PRIMARY KEY (`model_id`,`generation_id`,`serie_id`,`modification_id`,`equipment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `mbr_sap_model_class` */

DROP TABLE IF EXISTS `mbr_sap_model_class`;

CREATE TABLE `mbr_sap_model_class` (
  `model_id` int(11) NOT NULL,
  `serie_id` int(11) NOT NULL DEFAULT 0,
  `class_code` varchar(255) DEFAULT NULL,
  `model_code` varchar(255) DEFAULT NULL,
  `brand_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`model_id`,`serie_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `model_alias` */

DROP TABLE IF EXISTS `model_alias`;

CREATE TABLE `model_alias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `brand_id` int(11) unsigned NOT NULL,
  `model_id` int(11) NOT NULL,
  `generation_id` int(11) DEFAULT NULL,
  `serie_id` int(11) DEFAULT NULL,
  `status` tinyint(3) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `classification` tinyint(3) NOT NULL,
  `is_new` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_model_alias_brand_id` (`brand_id`),
  KEY `fk_model_alias_model_id` (`model_id`),
  KEY `fk_model_alias_generation_id` (`generation_id`),
  KEY `fk_model_alias_serie_id` (`serie_id`),
  CONSTRAINT `fk_model_alias_brand_id` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`),
  CONSTRAINT `fk_model_alias_generation_id` FOREIGN KEY (`generation_id`) REFERENCES `car_generation` (`id_car_generation`),
  CONSTRAINT `fk_model_alias_model_id` FOREIGN KEY (`model_id`) REFERENCES `models` (`id`),
  CONSTRAINT `fk_model_alias_serie_id` FOREIGN KEY (`serie_id`) REFERENCES `car_serie` (`id_car_serie`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `model_class` */

DROP TABLE IF EXISTS `model_class`;

CREATE TABLE `model_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `model_image` */

DROP TABLE IF EXISTS `model_image`;

CREATE TABLE `model_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` smallint(6) NOT NULL,
  `priority` smallint(6) NOT NULL,
  `status` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_model_image_model_id` (`model_id`),
  CONSTRAINT `fk_model_image_model_id` FOREIGN KEY (`model_id`) REFERENCES `models` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=369 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `model_segment` */

DROP TABLE IF EXISTS `model_segment`;

CREATE TABLE `model_segment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `model_video` */

DROP TABLE IF EXISTS `model_video`;

CREATE TABLE `model_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NOT NULL,
  `type` smallint(6) NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `priority` smallint(6) NOT NULL,
  `status` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_model_video_model_id` (`model_id`),
  CONSTRAINT `fk_model_video_model_id` FOREIGN KEY (`model_id`) REFERENCES `models` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `model_year` */

DROP TABLE IF EXISTS `model_year`;

CREATE TABLE `model_year` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `is_recent` tinyint(1) NOT NULL DEFAULT 1,
  `is_default` tinyint(1) NOT NULL DEFAULT 0,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_model_year` (`model_id`,`year`),
  CONSTRAINT `fk_model_year_model` FOREIGN KEY (`model_id`) REFERENCES `models` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16761 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `model_year_equipment` */

DROP TABLE IF EXISTS `model_year_equipment`;

CREATE TABLE `model_year_equipment` (
  `model_year_id` int(11) NOT NULL,
  `equipment_id` int(11) NOT NULL,
  PRIMARY KEY (`model_year_id`,`equipment_id`),
  KEY `fk_model_year_equipment_equipment_id` (`equipment_id`),
  CONSTRAINT `fk_model_year_equipment_equipment_id` FOREIGN KEY (`equipment_id`) REFERENCES `eqt_equipment` (`id`),
  CONSTRAINT `fk_model_year_equipment_model_year_id` FOREIGN KEY (`model_year_id`) REFERENCES `model_year` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `models` */

DROP TABLE IF EXISTS `models`;

CREATE TABLE `models` (
  `brand_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `tradein_code` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `is_recent` tinyint(1) NOT NULL DEFAULT 0,
  `dealerpoint_code` varchar(255) DEFAULT NULL,
  `ord` int(11) NOT NULL,
  `ecm_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `is_commercial` tinyint(1) NOT NULL,
  `origin_id` int(11) DEFAULT NULL COMMENT 'ссылка на таблицу eqt_car_model',
  `model_class_id` int(11) DEFAULT NULL,
  `model_segment_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `brand_id_is_recent` (`brand_id`,`is_recent`),
  KEY `fk_models_model_class_id` (`model_class_id`),
  KEY `fk_models_model_segment_id` (`model_segment_id`),
  CONSTRAINT `fk_models_model_class_id` FOREIGN KEY (`model_class_id`) REFERENCES `model_class` (`id`),
  CONSTRAINT `fk_models_model_segment_id` FOREIGN KEY (`model_segment_id`) REFERENCES `model_segment` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18084 DEFAULT CHARSET=utf8;

/*Table structure for table `models_bodies` */

DROP TABLE IF EXISTS `models_bodies`;

CREATE TABLE `models_bodies` (
  `model_id` int(11) NOT NULL,
  `body_id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `photo` varchar(64) NOT NULL,
  PRIMARY KEY (`model_id`,`body_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `modification_equipment` */

DROP TABLE IF EXISTS `modification_equipment`;

CREATE TABLE `modification_equipment` (
  `modification_id` int(11) NOT NULL,
  `equipment_id` int(11) NOT NULL,
  PRIMARY KEY (`modification_id`,`equipment_id`),
  KEY `fk_modification_equipment_equipment_id` (`equipment_id`),
  CONSTRAINT `fk_modification_equipment_equipment_id` FOREIGN KEY (`equipment_id`) REFERENCES `eqt_equipment` (`id`),
  CONSTRAINT `fk_modification_equipment_modification_id` FOREIGN KEY (`modification_id`) REFERENCES `car_modification` (`id_car_modification`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `news` */

DROP TABLE IF EXISTS `news`;

CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_public` datetime DEFAULT NULL,
  `tags_bitmap` bigint(20) DEFAULT 0 COMMENT 'Битовая маска назначенных тэгов (см. класс appmodelsNewsTag)',
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8;

/*Table structure for table `news_brand` */

DROP TABLE IF EXISTS `news_brand`;

CREATE TABLE `news_brand` (
  `news_id` int(11) NOT NULL,
  `brand_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`news_id`,`brand_id`),
  KEY `fk_news_brand_brand_id` (`brand_id`),
  CONSTRAINT `fk_news_brand_brand_id` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`),
  CONSTRAINT `fk_news_brand_news_id` FOREIGN KEY (`news_id`) REFERENCES `news` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `okopf` */

DROP TABLE IF EXISTS `okopf`;

CREATE TABLE `okopf` (
  `code` int(11) unsigned NOT NULL,
  `version` enum('1999','2007','2012') NOT NULL,
  `name` varchar(255) NOT NULL,
  UNIQUE KEY `code_version` (`code`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `okved` */

DROP TABLE IF EXISTS `okved`;

CREATE TABLE `okved` (
  `code` int(10) unsigned NOT NULL,
  `version` enum('2001') NOT NULL,
  `name` varchar(255) NOT NULL,
  UNIQUE KEY `code_version` (`code`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `old_models_map` */

DROP TABLE IF EXISTS `old_models_map`;

CREATE TABLE `old_models_map` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) unsigned NOT NULL,
  `new_model_id` int(11) DEFAULT NULL,
  `new_body_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `brand_id` (`brand_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1017 DEFAULT CHARSET=utf8;

/*Table structure for table `order_types` */

DROP TABLE IF EXISTS `order_types`;

CREATE TABLE `order_types` (
  `code` varchar(255) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ord` int(11) NOT NULL,
  UNIQUE KEY `idx` (`code`,`brand_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `ref_model_class_segment` */

DROP TABLE IF EXISTS `ref_model_class_segment`;

CREATE TABLE `ref_model_class_segment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NOT NULL,
  `model_class_id_type` int(11) DEFAULT NULL,
  `model_class_id` int(11) DEFAULT NULL,
  `model_segment_id_type` int(11) DEFAULT NULL,
  `model_segment_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ref_model_class_segment_model_id` (`model_id`),
  KEY `idx_ref_model_class_segment_model_class_id_type` (`model_class_id_type`),
  KEY `idx_ref_model_class_segment_model_segment_id_type` (`model_segment_id_type`),
  KEY `fk_ref_model_class_segment_model_class_id` (`model_class_id`),
  KEY `fk_ref_model_class_segment_model_segment_id` (`model_segment_id`),
  CONSTRAINT `fk_ref_model_class_segment_model_class_id` FOREIGN KEY (`model_class_id`) REFERENCES `model_class` (`id`),
  CONSTRAINT `fk_ref_model_class_segment_model_segment_id` FOREIGN KEY (`model_segment_id`) REFERENCES `model_segment` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8200 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `regions` */

DROP TABLE IF EXISTS `regions`;

CREATE TABLE `regions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `federal_district_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` smallint(6) NOT NULL,
  `timezone` varchar(255) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `okato` char(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`),
  KEY `federal_district_id` (`federal_district_id`),
  CONSTRAINT `regions_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `regions_ibfk_2` FOREIGN KEY (`federal_district_id`) REFERENCES `federal_districts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=166 DEFAULT CHARSET=utf8;

/*Table structure for table `russian_name` */

DROP TABLE IF EXISTS `russian_name`;

CREATE TABLE `russian_name` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `sex` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_russian_name_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=206116 DEFAULT CHARSET=utf8;

/*Table structure for table `skin_serie` */

DROP TABLE IF EXISTS `skin_serie`;

CREATE TABLE `skin_serie` (
  `skin_id` int(11) NOT NULL,
  `serie_id` int(11) NOT NULL,
  PRIMARY KEY (`skin_id`,`serie_id`),
  KEY `fk_skin_serie_serie_id` (`serie_id`),
  CONSTRAINT `fk_skin_serie_serie_id` FOREIGN KEY (`serie_id`) REFERENCES `car_serie` (`id_car_serie`),
  CONSTRAINT `fk_skin_serie_skin_id` FOREIGN KEY (`skin_id`) REFERENCES `eqt_skin` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `slmi_models` */

DROP TABLE IF EXISTS `slmi_models`;

CREATE TABLE `slmi_models` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` varchar(255) NOT NULL,
  `model_id` int(11) unsigned DEFAULT NULL,
  `body_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=utf8;

/*Table structure for table `sources` */

DROP TABLE IF EXISTS `sources`;

CREATE TABLE `sources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

/*Table structure for table `stat_models` */

DROP TABLE IF EXISTS `stat_models`;

CREATE TABLE `stat_models` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) NOT NULL,
  `code` varchar(32) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `ord` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_idx` (`code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=226 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

/*Table structure for table `statuses` */

DROP TABLE IF EXISTS `statuses`;

CREATE TABLE `statuses` (
  `status_code` varchar(255) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `status_ord` int(11) NOT NULL,
  `status_name` varchar(255) NOT NULL,
  UNIQUE KEY `status_code` (`status_code`,`brand_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_migration` */

DROP TABLE IF EXISTS `tbl_migration`;

CREATE TABLE `tbl_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `fullname` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `plainPassword` varchar(255) NOT NULL,
  `password` varchar(60) NOT NULL,
  `role` enum('reference:guest','reference:admin','reference:manager','reference:user') NOT NULL,
  `photo` varchar(2048) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx-updated` (`updatedAt`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Table structure for table `vehicle_internal_status` */

DROP TABLE IF EXISTS `vehicle_internal_status`;

CREATE TABLE `vehicle_internal_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Table structure for table `vehicle_option_group` */

DROP TABLE IF EXISTS `vehicle_option_group`;

CREATE TABLE `vehicle_option_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

/*Table structure for table `vehicle_option_type` */

DROP TABLE IF EXISTS `vehicle_option_type`;

CREATE TABLE `vehicle_option_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `values_json` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `fk_vehicle_option_type_group_id` (`group_id`),
  CONSTRAINT `fk_vehicle_option_type_group_id` FOREIGN KEY (`group_id`) REFERENCES `vehicle_option_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=509 DEFAULT CHARSET=utf8;

/*Table structure for table `vehicle_passport_status` */

DROP TABLE IF EXISTS `vehicle_passport_status`;

CREATE TABLE `vehicle_passport_status` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `vehicle_verification_program` */

DROP TABLE IF EXISTS `vehicle_verification_program`;

CREATE TABLE `vehicle_verification_program` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_vehicle_verification_program_brand` (`brand_id`),
  CONSTRAINT `fk_vehicle_verification_program_brand` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `works` */

DROP TABLE IF EXISTS `works`;

CREATE TABLE `works` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `maintenance` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index2` (`id`,`name`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
