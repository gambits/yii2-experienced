/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 10.4.11-MariaDB-1:10.4.11+maria~bionic : Database - rds
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`rds_test` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `rds_test`;

/*Table structure for table `action_plan` */

DROP TABLE IF EXISTS `action_plan`;

CREATE TABLE `action_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dealer_poll_id` int(11) DEFAULT NULL,
  `dealer_id` int(11) NOT NULL,
  `deadline_at` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_action_plan_dealer_poll_id` (`dealer_poll_id`),
  KEY `fk_action_plan_dealer_id` (`dealer_id`),
  KEY `fk_action_plan_created_by` (`created_by`),
  KEY `fk_action_plan_updated_by` (`updated_by`),
  CONSTRAINT `fk_action_plan_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_action_plan_dealer_id` FOREIGN KEY (`dealer_id`) REFERENCES `dealer` (`id`),
  CONSTRAINT `fk_action_plan_dealer_poll_id` FOREIGN KEY (`dealer_poll_id`) REFERENCES `dealer_poll` (`id`),
  CONSTRAINT `fk_action_plan_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `activity` */

DROP TABLE IF EXISTS `activity`;

CREATE TABLE `activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_activity_created_by` (`created_by`),
  KEY `fk_activity_updated_by` (`updated_by`),
  CONSTRAINT `fk_activity_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_activity_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `activity_direction` */

DROP TABLE IF EXISTS `activity_direction`;

CREATE TABLE `activity_direction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `aeb_region` */

DROP TABLE IF EXISTS `aeb_region`;

CREATE TABLE `aeb_region` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `federal_district_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `segment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=518812 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `aeb_region_upload_history` */

DROP TABLE IF EXISTS `aeb_region_upload_history`;

CREATE TABLE `aeb_region_upload_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `import_status` int(11) NOT NULL DEFAULT 0,
  `imported_success` int(11) NOT NULL DEFAULT 0,
  `imported_errors` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT 0,
  `month1` int(11) DEFAULT NULL,
  `month2` int(11) DEFAULT NULL,
  `month3` int(11) DEFAULT NULL,
  `month4` int(11) DEFAULT NULL,
  `month5` int(11) DEFAULT NULL,
  `month6` int(11) DEFAULT NULL,
  `month7` int(11) DEFAULT NULL,
  `month8` int(11) DEFAULT NULL,
  `month9` int(11) DEFAULT NULL,
  `month10` int(11) DEFAULT NULL,
  `month11` int(11) DEFAULT NULL,
  `month12` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `auth_assignment` */

DROP TABLE IF EXISTS `auth_assignment`;

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  KEY `idx-auth_assignment-user_id` (`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `auth_item` */

DROP TABLE IF EXISTS `auth_item`;

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `auth_item_child` */

DROP TABLE IF EXISTS `auth_item_child`;

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `auth_rule` */

DROP TABLE IF EXISTS `auth_rule`;

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `body_type` */

DROP TABLE IF EXISTS `body_type`;

CREATE TABLE `body_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_body_type_created_by` (`created_by`),
  KEY `fk_body_type_updated_by` (`updated_by`),
  CONSTRAINT `fk_body_type_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_body_type_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `brand_mapping` */

DROP TABLE IF EXISTS `brand_mapping`;

CREATE TABLE `brand_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) NOT NULL,
  `name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `bus_purpose` */

DROP TABLE IF EXISTS `bus_purpose`;

CREATE TABLE `bus_purpose` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_bus_purpose_created_by` (`created_by`),
  KEY `fk_bus_purpose_updated_by` (`updated_by`),
  CONSTRAINT `fk_bus_purpose_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_bus_purpose_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `cache` */

DROP TABLE IF EXISTS `cache`;

CREATE TABLE `cache` (
  `id` char(128) NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` longblob DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_cache_expire` (`expire`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `car_characteristics` */

DROP TABLE IF EXISTS `car_characteristics`;

CREATE TABLE `car_characteristics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `car_id` int(11) NOT NULL,
  `body_type_id` int(11) DEFAULT NULL,
  `steering_wheel_type_id` int(11) DEFAULT NULL,
  `engine_type_id` int(11) DEFAULT NULL,
  `engine_power` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `engine_volume` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `true_weight` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `curb_weight` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight_range_1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight_range_2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cmk` tinyint(1) DEFAULT NULL,
  `cmk_detailing_id` int(11) DEFAULT NULL,
  `model_class_id` int(11) DEFAULT NULL,
  `gvw_range` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chassis_configuration_id` int(11) DEFAULT NULL,
  `bus_class` tinyint(1) DEFAULT NULL,
  `bus_purpose_id` int(11) DEFAULT NULL,
  `bus_length` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `engine_number` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body_number` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chassis_number` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_at` datetime NOT NULL,
  `end_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `gvw_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gvw_segment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_car_characteristics_body_type_id` (`body_type_id`),
  KEY `fk_car_characteristics_steering_wheel_type_id` (`steering_wheel_type_id`),
  KEY `fk_car_characteristics_engine_type_id` (`engine_type_id`),
  KEY `fk_car_characteristics_cmk_detailing_id` (`cmk_detailing_id`),
  KEY `fk_car_characteristics_model_class_id` (`model_class_id`),
  KEY `fk_car_characteristics_chassis_configuration_id` (`chassis_configuration_id`),
  KEY `fk_car_characteristics_bus_purpose_id` (`bus_purpose_id`),
  KEY `fk_car_characteristics_created_by` (`created_by`),
  KEY `fk_car_characteristics_updated_by` (`updated_by`),
  KEY `car_characteristics_car_idx` (`car_id`),
  KEY `body_number_idx` (`body_number`),
  KEY `engine_number_idx` (`engine_number`),
  KEY `car_characteristics_end_at_index` (`end_at`),
  CONSTRAINT `fk_car_characteristics_body_type_id` FOREIGN KEY (`body_type_id`) REFERENCES `body_type` (`id`),
  CONSTRAINT `fk_car_characteristics_bus_purpose_id` FOREIGN KEY (`bus_purpose_id`) REFERENCES `bus_purpose` (`id`),
  CONSTRAINT `fk_car_characteristics_car_id` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`),
  CONSTRAINT `fk_car_characteristics_chassis_configuration_id` FOREIGN KEY (`chassis_configuration_id`) REFERENCES `chassis_configuration` (`id`),
  CONSTRAINT `fk_car_characteristics_cmk_detailing_id` FOREIGN KEY (`cmk_detailing_id`) REFERENCES `cmk_detailing` (`id`),
  CONSTRAINT `fk_car_characteristics_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_car_characteristics_engine_type_id` FOREIGN KEY (`engine_type_id`) REFERENCES `engine_type` (`id`),
  CONSTRAINT `fk_car_characteristics_model_class_id` FOREIGN KEY (`model_class_id`) REFERENCES `model_class` (`id`),
  CONSTRAINT `fk_car_characteristics_steering_wheel_type_id` FOREIGN KEY (`steering_wheel_type_id`) REFERENCES `steering_wheel_type` (`id`),
  CONSTRAINT `fk_car_characteristics_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55355647 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `car_modification` */

DROP TABLE IF EXISTS `car_modification`;

CREATE TABLE `car_modification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_model_class_segment_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_car_modification_ref_model_class_segment_id` (`ref_model_class_segment_id`),
  KEY `fk_car_modification_created_by` (`created_by`),
  KEY `fk_car_modification_updated_by` (`updated_by`),
  CONSTRAINT `fk_car_modification_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_car_modification_ref_model_class_segment_id` FOREIGN KEY (`ref_model_class_segment_id`) REFERENCES `ref_model_class_segment` (`id`),
  CONSTRAINT `fk_car_modification_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21750 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `car_registration` */

DROP TABLE IF EXISTS `car_registration`;

CREATE TABLE `car_registration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `car_id` int(11) NOT NULL,
  `registration_place_id` int(11) NOT NULL,
  `registration_date` date NOT NULL,
  `state` tinyint(3) NOT NULL,
  `type` tinyint(3) NOT NULL,
  `ownership_type` tinyint(3) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `month` smallint(6) DEFAULT NULL,
  `year` smallint(6) DEFAULT NULL,
  `inn` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `okved_id` int(11) DEFAULT NULL,
  `activity_type` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_car_registration_car_id` (`car_id`),
  KEY `fk_car_registration_registration_place_id` (`registration_place_id`),
  KEY `fk_car_registration_created_by` (`created_by`),
  KEY `fk_car_registration_updated_by` (`updated_by`),
  KEY `idx_market_share_report` (`type`,`registration_date`),
  KEY `fk_car_registration_okved_id` (`okved_id`),
  KEY `ix_car_registration_inn` (`inn`),
  CONSTRAINT `fk_car_registration_car_id` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`),
  CONSTRAINT `fk_car_registration_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_car_registration_okved_id` FOREIGN KEY (`okved_id`) REFERENCES `okved` (`id`),
  CONSTRAINT `fk_car_registration_registration_place_id` FOREIGN KEY (`registration_place_id`) REFERENCES `registration_places` (`id`),
  CONSTRAINT `fk_car_registration_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37518327 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `cars` */

DROP TABLE IF EXISTS `cars`;

CREATE TABLE `cars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `car_type_id` int(11) DEFAULT NULL,
  `lcv_id` int(11) DEFAULT NULL,
  `brand_id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `modification_id` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `vin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `manufacturer` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manufacturer_country_id` int(11) DEFAULT NULL,
  `is_foreign` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `source` smallint(6) NOT NULL DEFAULT 0,
  `year_recycled` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chassis_manufacturer_country_id` int(11) DEFAULT NULL,
  `is_chassis_foreign` int(11) DEFAULT NULL,
  `price_segment_id` int(11) DEFAULT NULL,
  `registration_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `run` int(11) DEFAULT NULL,
  `sale_date` date DEFAULT NULL,
  `market` tinyint(3) DEFAULT NULL,
  `inn` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kpp` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sold_vehicle_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cars_user` (`user_id`),
  KEY `fk_cars_model_id` (`model_id`),
  KEY `fk_cars_modification_id` (`modification_id`),
  KEY `ix_cars_brand_id` (`brand_id`),
  KEY `idx_vin` (`vin`),
  KEY `idx_year` (`year`),
  KEY `idx_year_recycled` (`year_recycled`),
  KEY `fk_cars_sold_vehicle` (`sold_vehicle_id`),
  KEY `id` (`id`,`brand_id`,`model_id`,`modification_id`),
  CONSTRAINT `fk_cars_modification_id` FOREIGN KEY (`modification_id`) REFERENCES `car_modification` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_cars_sold_vehicle` FOREIGN KEY (`sold_vehicle_id`) REFERENCES `sold_vehicle` (`id`),
  CONSTRAINT `fk_cars_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55354727 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `chassis_configuration` */

DROP TABLE IF EXISTS `chassis_configuration`;

CREATE TABLE `chassis_configuration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_chassis_configuration_created_by` (`created_by`),
  KEY `fk_chassis_configuration_updated_by` (`updated_by`),
  CONSTRAINT `fk_chassis_configuration_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_chassis_configuration_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `chat` */

DROP TABLE IF EXISTS `chat`;

CREATE TABLE `chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_type` smallint(6) NOT NULL,
  `object_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_chat_user_id` (`user_id`),
  CONSTRAINT `fk_chat_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `chat_file` */

DROP TABLE IF EXISTS `chat_file`;

CREATE TABLE `chat_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chat_id` int(11) NOT NULL,
  `upload_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_chat_file_user_id` (`user_id`),
  KEY `fk_chat_file_chat_id` (`chat_id`),
  KEY `fk_chat_file_upload_id` (`upload_id`),
  CONSTRAINT `fk_chat_file_chat_id` FOREIGN KEY (`chat_id`) REFERENCES `chat` (`id`),
  CONSTRAINT `fk_chat_file_upload_id` FOREIGN KEY (`upload_id`) REFERENCES `upload` (`id`),
  CONSTRAINT `fk_chat_file_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `chat_message` */

DROP TABLE IF EXISTS `chat_message`;

CREATE TABLE `chat_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chat_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `text` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_chat_message_user_id` (`user_id`),
  KEY `fk_chat_message_chat_id` (`chat_id`),
  CONSTRAINT `fk_chat_message_chat_id` FOREIGN KEY (`chat_id`) REFERENCES `chat` (`id`),
  CONSTRAINT `fk_chat_message_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `city_mapping` */

DROP TABLE IF EXISTS `city_mapping`;

CREATE TABLE `city_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `aeb_region_upload_history_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `city_mapping_upload_idx` (`aeb_region_upload_history_id`),
  CONSTRAINT `city_mapping_upload_idx` FOREIGN KEY (`aeb_region_upload_history_id`) REFERENCES `aeb_region_upload_history` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=260 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `cmk_detailing` */

DROP TABLE IF EXISTS `cmk_detailing`;

CREATE TABLE `cmk_detailing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cmk_detailing_created_by` (`created_by`),
  KEY `fk_cmk_detailing_updated_by` (`updated_by`),
  CONSTRAINT `fk_cmk_detailing_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_cmk_detailing_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `company` */

DROP TABLE IF EXISTS `company`;

CREATE TABLE `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `inn` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `kpp` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `entity_type_id` int(11) DEFAULT NULL,
  `code_incadea` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marketing_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `holding_id` int(11) DEFAULT NULL,
  `has_uaz_group` tinyint(1) DEFAULT NULL,
  `company_status` int(11) DEFAULT NULL,
  `head_fio` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_status_updated_at` datetime DEFAULT NULL,
  `juridical_address` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fact_address` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `inn` (`inn`),
  KEY `fk_company_created_by` (`created_by`),
  KEY `fk_company_updated_by` (`updated_by`),
  KEY `fk_company_entity_type_id_entity_type_id` (`entity_type_id`),
  KEY `fk_company_holding_id_holding_id` (`holding_id`),
  CONSTRAINT `fk_company_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_company_entity_type_id_entity_type_id` FOREIGN KEY (`entity_type_id`) REFERENCES `entity_type` (`id`),
  CONSTRAINT `fk_company_holding_id_holding_id` FOREIGN KEY (`holding_id`) REFERENCES `holding` (`id`),
  CONSTRAINT `fk_company_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1443 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `company_document` */

DROP TABLE IF EXISTS `company_document`;

CREATE TABLE `company_document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `upload_id` int(11) NOT NULL,
  `document_type_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_company_document_company` (`company_id`),
  KEY `fk_company_document_upload` (`upload_id`),
  KEY `fk_company_document_user` (`user_id`),
  CONSTRAINT `fk_company_document_company` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  CONSTRAINT `fk_company_document_upload` FOREIGN KEY (`upload_id`) REFERENCES `upload` (`id`),
  CONSTRAINT `fk_company_document_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `company_status` */

DROP TABLE IF EXISTS `company_status`;

CREATE TABLE `company_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `competing_segment` */

DROP TABLE IF EXISTS `competing_segment`;

CREATE TABLE `competing_segment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `source` tinyint(3) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_competing_segment_created_by` (`created_by`),
  KEY `fk_competing_segment_updated_by` (`updated_by`),
  CONSTRAINT `fk_competing_segment_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_competing_segment_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `competing_segment_comparison` */

DROP TABLE IF EXISTS `competing_segment_comparison`;

CREATE TABLE `competing_segment_comparison` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `segment_id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `modification_id` int(11) DEFAULT NULL,
  `type` tinyint(3) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_competing_segment_comparison_segment_id` (`segment_id`),
  KEY `fk_competing_segment_comparison_modification_id` (`modification_id`),
  KEY `fk_competing_segment_comparison_model_id` (`model_id`),
  KEY `x_segmentTypeModModif` (`segment_id`,`model_id`,`modification_id`,`type`),
  CONSTRAINT `fk_competing_segment_comparison_model_id` FOREIGN KEY (`model_id`) REFERENCES `ref_model_class_segment` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_competing_segment_comparison_modification_id` FOREIGN KEY (`modification_id`) REFERENCES `car_modification` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_competing_segment_comparison_segment_id` FOREIGN KEY (`segment_id`) REFERENCES `competing_segment` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `competitor` */

DROP TABLE IF EXISTS `competitor`;

CREATE TABLE `competitor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postcode` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `holding_id` int(11) DEFAULT NULL,
  `latitude` decimal(10,8) DEFAULT NULL,
  `longitude` decimal(11,8) DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `source_updated_at` date DEFAULT NULL,
  `holding_website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_before_move` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo_id` int(11) DEFAULT NULL,
  `changed_by_user` tinyint(1) DEFAULT 0,
  `display_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `competitor_holding_idx` (`holding_id`),
  KEY `competitor_logo_idx` (`logo_id`),
  CONSTRAINT `competitor_holding_idx` FOREIGN KEY (`holding_id`) REFERENCES `holding` (`id`),
  CONSTRAINT `competitor_logo_idx` FOREIGN KEY (`logo_id`) REFERENCES `upload` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10725 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `competitor_brand` */

DROP TABLE IF EXISTS `competitor_brand`;

CREATE TABLE `competitor_brand` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dc_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_prefix` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax_prefix` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `source_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `distributor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `competitor_id` int(11) DEFAULT NULL,
  `additional` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `source_updated_at` date DEFAULT NULL,
  `source_created_at` date DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `source_closed_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `competitor_competitor_idx` (`competitor_id`),
  CONSTRAINT `competitor_competitor_idx` FOREIGN KEY (`competitor_id`) REFERENCES `competitor` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17484 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `competitor_import_schema` */

DROP TABLE IF EXISTS `competitor_import_schema`;

CREATE TABLE `competitor_import_schema` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rules` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_competitor_import_schema_created_by` (`created_by`),
  CONSTRAINT `fk_competitor_import_schema_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `competitor_upload_history` */

DROP TABLE IF EXISTS `competitor_upload_history`;

CREATE TABLE `competitor_upload_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL,
  `show_step` int(11) NOT NULL,
  `address_status` int(11) NOT NULL,
  `address_api_calls` int(11) NOT NULL,
  `address_api_missed` int(11) NOT NULL,
  `holding_status` int(11) NOT NULL,
  `reference_status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `updated_competitors` int(11) NOT NULL,
  `opened_competitors` int(11) NOT NULL,
  `closed_competitors` int(11) NOT NULL,
  `address_checked` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `competitor_upload_history_log` */

DROP TABLE IF EXISTS `competitor_upload_history_log`;

CREATE TABLE `competitor_upload_history_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `competitor_upload_history_id` int(11) NOT NULL,
  `rds_dealer_id` int(11) DEFAULT NULL,
  `operation` int(11) NOT NULL,
  `dealer_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `brand` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `error` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `competitor_upload_history_log_history_fk` (`competitor_upload_history_id`),
  CONSTRAINT `competitor_upload_history_log_history_fk` FOREIGN KEY (`competitor_upload_history_id`) REFERENCES `competitor_upload_history` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=135522 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `contact_matrix` */

DROP TABLE IF EXISTS `contact_matrix`;

CREATE TABLE `contact_matrix` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_category_id` int(11) NOT NULL,
  `dealer_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `phone` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `mobile_phone` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `full_name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_contact_matrix_created_by` (`created_by`),
  KEY `fk_contact_matrix_updated_by` (`updated_by`),
  KEY `fk_contact_matrix_user_id` (`user_id`),
  KEY `fk_contact_matrix_dealer_id` (`dealer_id`),
  KEY `fk_contact_matrix_contact_category_id` (`contact_category_id`),
  CONSTRAINT `fk_contact_matrix_contact_category_id` FOREIGN KEY (`contact_category_id`) REFERENCES `dealer_contact_category` (`id`),
  CONSTRAINT `fk_contact_matrix_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_contact_matrix_dealer_id` FOREIGN KEY (`dealer_id`) REFERENCES `dealer` (`id`),
  CONSTRAINT `fk_contact_matrix_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_contact_matrix_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `contract` */

DROP TABLE IF EXISTS `contract`;

CREATE TABLE `contract` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dealer_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `status` smallint(6) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `creation_reason_id` int(11) DEFAULT NULL,
  `termination_reason_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` datetime NOT NULL,
  `started_at` datetime NOT NULL,
  `active_till` datetime DEFAULT NULL,
  `terminated_at` datetime DEFAULT NULL,
  `is_additional_contract` tinyint(3) NOT NULL DEFAULT 1,
  `contract_id` int(11) DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_contract_dealer` (`dealer_id`),
  KEY `fk_contract_company` (`company_id`),
  KEY `fk_contract_type` (`type_id`),
  KEY `fk_contract_created_by` (`created_by`),
  KEY `fk_contract_updated_by` (`updated_by`),
  KEY `fk_contract_creation_reason` (`creation_reason_id`),
  KEY `fk_contract_termination_reason` (`termination_reason_id`),
  CONSTRAINT `fk_contract_company` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  CONSTRAINT `fk_contract_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_contract_creation_reason` FOREIGN KEY (`creation_reason_id`) REFERENCES `contract_creation_reason` (`id`),
  CONSTRAINT `fk_contract_dealer` FOREIGN KEY (`dealer_id`) REFERENCES `dealer` (`id`),
  CONSTRAINT `fk_contract_termination_reason` FOREIGN KEY (`termination_reason_id`) REFERENCES `contract_termination_reason` (`id`),
  CONSTRAINT `fk_contract_type` FOREIGN KEY (`type_id`) REFERENCES `contract_type` (`id`),
  CONSTRAINT `fk_contract_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `contract_creation_reason` */

DROP TABLE IF EXISTS `contract_creation_reason`;

CREATE TABLE `contract_creation_reason` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(3) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `contract_document` */

DROP TABLE IF EXISTS `contract_document`;

CREATE TABLE `contract_document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_id` int(11) NOT NULL,
  `upload_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `document_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_contract_document_contract` (`contract_id`),
  KEY `fk_contract_document_upload` (`upload_id`),
  KEY `fk_contract_document_user` (`user_id`),
  KEY `fk_contract_document_type_id` (`document_type_id`),
  CONSTRAINT `fk_contract_document_type_id` FOREIGN KEY (`document_type_id`) REFERENCES `document_type` (`id`),
  CONSTRAINT `fk_contract_document_upload` FOREIGN KEY (`upload_id`) REFERENCES `upload` (`id`),
  CONSTRAINT `fk_contract_document_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `contract_status_change` */

DROP TABLE IF EXISTS `contract_status_change`;

CREATE TABLE `contract_status_change` (
  `contract_id` int(11) NOT NULL,
  `status` smallint(6) NOT NULL,
  `date` datetime NOT NULL,
  `comment` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  KEY `fk_contract_status_change_user` (`user_id`),
  KEY `fk_contract_status_change_contract` (`contract_id`),
  CONSTRAINT `fk_contract_status_change_contract` FOREIGN KEY (`contract_id`) REFERENCES `contract` (`id`),
  CONSTRAINT `fk_contract_status_change_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `contract_subject` */

DROP TABLE IF EXISTS `contract_subject`;

CREATE TABLE `contract_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `incadea` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `closed_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_contract_subject_contract` (`contract_id`),
  KEY `fk_contract_subject_activity` (`activity_id`),
  KEY `fk_contract_subject_created_by` (`created_by`),
  KEY `fk_contract_subject_updated_by` (`updated_by`),
  CONSTRAINT `fk_contract_subject_activity` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`),
  CONSTRAINT `fk_contract_subject_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_contract_subject_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `contract_termination_reason` */

DROP TABLE IF EXISTS `contract_termination_reason`;

CREATE TABLE `contract_termination_reason` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(3) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `contract_type` */

DROP TABLE IF EXISTS `contract_type`;

CREATE TABLE `contract_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_contract_type_created_by` (`created_by`),
  KEY `fk_contract_type_updated_by` (`updated_by`),
  CONSTRAINT `fk_contract_type_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_contract_type_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `corporate_client` */

DROP TABLE IF EXISTS `corporate_client`;

CREATE TABLE `corporate_client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `holding_id` int(11) DEFAULT NULL,
  `subholding_id` int(11) DEFAULT NULL,
  `is_main_holding_company` int(11) NOT NULL DEFAULT 0,
  `is_main_subholding_company` int(11) NOT NULL DEFAULT 0,
  `reason_of_relation_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `company_status_id` int(11) DEFAULT NULL,
  `okved_id` int(11) DEFAULT NULL,
  `okopf_id` int(11) DEFAULT NULL,
  `okfc_id` int(11) DEFAULT NULL,
  `enterprise_type_id` int(11) DEFAULT NULL,
  `creation_type` int(11) NOT NULL DEFAULT 0,
  `is_liquidated` int(11) NOT NULL DEFAULT 0,
  `name` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `full_name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `inn` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `kpp` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ogrn` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lng` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manager` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manager_post` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `okpo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(2500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `risk` int(11) DEFAULT NULL,
  `company_found_date` date DEFAULT NULL,
  `counterpart_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `folder_id` int(11) DEFAULT NULL,
  `dealer_id` int(11) DEFAULT NULL,
  `contact_type` tinyint(3) NOT NULL DEFAULT 0,
  `data_status` tinyint(3) NOT NULL DEFAULT 1,
  `data_source` tinyint(3) NOT NULL DEFAULT 1,
  `ocato` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_reason_of_relation_id` (`reason_of_relation_id`),
  KEY `fk_company_status_id` (`company_status_id`),
  KEY `fk_okved_id` (`okved_id`),
  KEY `fk_enterprise_type_id` (`enterprise_type_id`),
  KEY `fk_created_by` (`created_by`),
  KEY `fk_updated_by` (`updated_by`),
  KEY `corporate_client_inn_idx` (`inn`,`kpp`),
  KEY `fk_corporate_client_folder_id` (`folder_id`),
  KEY `fk_corporate_client_dealer` (`dealer_id`),
  CONSTRAINT `fk_company_status_id` FOREIGN KEY (`company_status_id`) REFERENCES `company_status` (`id`),
  CONSTRAINT `fk_corporate_client_dealer` FOREIGN KEY (`dealer_id`) REFERENCES `dealer` (`id`),
  CONSTRAINT `fk_corporate_client_folder_id` FOREIGN KEY (`folder_id`) REFERENCES `corporate_client_folder` (`id`),
  CONSTRAINT `fk_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_enterprise_type_id` FOREIGN KEY (`enterprise_type_id`) REFERENCES `enterprise_type` (`id`),
  CONSTRAINT `fk_okved_id` FOREIGN KEY (`okved_id`) REFERENCES `okved` (`id`),
  CONSTRAINT `fk_reason_of_relation_id` FOREIGN KEY (`reason_of_relation_id`) REFERENCES `reason_of_relation` (`id`),
  CONSTRAINT `fk_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17718 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `corporate_client_business_model` */

DROP TABLE IF EXISTS `corporate_client_business_model`;

CREATE TABLE `corporate_client_business_model` (
  `corporate_client_id` int(11) NOT NULL,
  `type` tinyint(3) NOT NULL,
  `age` tinyint(3) DEFAULT NULL,
  `mileage` int(11) DEFAULT NULL,
  `company_type` tinyint(3) DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `purchase_outsourcing` tinyint(3) DEFAULT 0,
  `purchase_outsourcing_comment` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service_outsourcing` tinyint(3) DEFAULT 0,
  `service_outsourcing_comment` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transport_work_type_id` int(11) DEFAULT NULL,
  `recalculate_queue_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`corporate_client_id`),
  KEY `fk_corporate_client_business_model_created_by` (`created_by`),
  KEY `fk_corporate_client_business_model_updated_by` (`updated_by`),
  KEY `fk_corporate_client_business_model_transport_work_type_id` (`transport_work_type_id`),
  CONSTRAINT `fk_corporate_client_business_model_corporate_client_id` FOREIGN KEY (`corporate_client_id`) REFERENCES `corporate_client` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_corporate_client_business_model_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_corporate_client_business_model_transport_work_type_id` FOREIGN KEY (`transport_work_type_id`) REFERENCES `transport_work_types` (`id`),
  CONSTRAINT `fk_corporate_client_business_model_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `corporate_client_business_model_purchase_criteria` */

DROP TABLE IF EXISTS `corporate_client_business_model_purchase_criteria`;

CREATE TABLE `corporate_client_business_model_purchase_criteria` (
  `business_model_id` int(11) NOT NULL DEFAULT 0,
  `decision_criteria_id` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`business_model_id`,`decision_criteria_id`),
  KEY `fk-cc_bm_purchase_criteria-decision_criteria_id` (`decision_criteria_id`),
  CONSTRAINT `fk-cc_bm_purchase_criteria-business_model_id` FOREIGN KEY (`business_model_id`) REFERENCES `corporate_client_business_model` (`corporate_client_id`) ON DELETE CASCADE,
  CONSTRAINT `fk-cc_bm_purchase_criteria-decision_criteria_id` FOREIGN KEY (`decision_criteria_id`) REFERENCES `decision_criteria` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `corporate_client_business_model_purchase_mechanism` */

DROP TABLE IF EXISTS `corporate_client_business_model_purchase_mechanism`;

CREATE TABLE `corporate_client_business_model_purchase_mechanism` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_mechanism_id` int(11) NOT NULL,
  `corporate_client_business_model_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cc_bm_pm_purchase_mechanism_id` (`purchase_mechanism_id`),
  KEY `cc_bm_pm_corporate_client_id` (`corporate_client_business_model_id`),
  CONSTRAINT `cc_bm_pm_corporate_client_id` FOREIGN KEY (`corporate_client_business_model_id`) REFERENCES `corporate_client_business_model` (`corporate_client_id`),
  CONSTRAINT `cc_bm_pm_purchase_mechanism_id` FOREIGN KEY (`purchase_mechanism_id`) REFERENCES `purchase_mechanism` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `corporate_client_business_model_update_criteria` */

DROP TABLE IF EXISTS `corporate_client_business_model_update_criteria`;

CREATE TABLE `corporate_client_business_model_update_criteria` (
  `business_model_id` int(11) NOT NULL DEFAULT 0,
  `decision_criteria_id` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`business_model_id`,`decision_criteria_id`),
  KEY `fk-cc_bm_update_criteria-decision_criteria_id` (`decision_criteria_id`),
  CONSTRAINT `fk-cc_bm_update_criteria-business_model_id` FOREIGN KEY (`business_model_id`) REFERENCES `corporate_client_business_model` (`corporate_client_id`) ON DELETE CASCADE,
  CONSTRAINT `fk-cc_bm_update_criteria-decision_criteria_id` FOREIGN KEY (`decision_criteria_id`) REFERENCES `decision_criteria` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `corporate_client_change_log` */

DROP TABLE IF EXISTS `corporate_client_change_log`;

CREATE TABLE `corporate_client_change_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `corporate_client_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `section` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33002 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `corporate_client_contact` */

DROP TABLE IF EXISTS `corporate_client_contact`;

CREATE TABLE `corporate_client_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `corporate_client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_in_company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `area_of_responsibility` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `work_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `marital_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `focus_of_interest` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_corporate_client_id` (`corporate_client_id`),
  CONSTRAINT `fk_corporate_client_id` FOREIGN KEY (`corporate_client_id`) REFERENCES `corporate_client` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `corporate_client_folder` */

DROP TABLE IF EXISTS `corporate_client_folder`;

CREATE TABLE `corporate_client_folder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `corporate_client_folder_client` */

DROP TABLE IF EXISTS `corporate_client_folder_client`;

CREATE TABLE `corporate_client_folder_client` (
  `folder_id` int(11) NOT NULL,
  `corporate_client_id` int(11) NOT NULL,
  PRIMARY KEY (`folder_id`,`corporate_client_id`),
  KEY `fk-cc_fc-corporate_client_id` (`corporate_client_id`),
  CONSTRAINT `fk-cc_fc-corporate_client_id` FOREIGN KEY (`corporate_client_id`) REFERENCES `corporate_client` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk-cc_fc-folder_id` FOREIGN KEY (`folder_id`) REFERENCES `corporate_client_folder` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `corporate_client_import_schema` */

DROP TABLE IF EXISTS `corporate_client_import_schema`;

CREATE TABLE `corporate_client_import_schema` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rules` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_corporate_client_import_schema_created_by` (`created_by`),
  CONSTRAINT `fk_corporate_client_import_schema_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `corporate_client_outsourcing` */

DROP TABLE IF EXISTS `corporate_client_outsourcing`;

CREATE TABLE `corporate_client_outsourcing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `corporate_client_id` int(11) NOT NULL,
  `outsourcing_corporate_client_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `outsourcing_type` int(11) DEFAULT NULL,
  `relation_type` int(11) DEFAULT NULL,
  `partnership_type` int(11) DEFAULT NULL,
  `start_year` int(11) DEFAULT NULL,
  `our_client` int(11) NOT NULL DEFAULT 0,
  `comment` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_outsourcing_corporate_client_id` (`outsourcing_corporate_client_id`),
  KEY `cco_corporate_client_id_idx` (`corporate_client_id`),
  CONSTRAINT `fk_cco_corporate_client_id` FOREIGN KEY (`corporate_client_id`) REFERENCES `corporate_client` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_outsourcing_corporate_client_id` FOREIGN KEY (`outsourcing_corporate_client_id`) REFERENCES `corporate_client` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `corporate_client_parks` */

DROP TABLE IF EXISTS `corporate_client_parks`;

CREATE TABLE `corporate_client_parks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `car_id` int(11) NOT NULL,
  `corporate_client_id` int(11) NOT NULL,
  `registration_type` int(11) NOT NULL,
  `registration_date` datetime NOT NULL,
  `registration_removal_date` datetime DEFAULT NULL,
  `registration_removal_comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `corporate_client_parks_car_id` (`car_id`),
  KEY `corporate_client_parks_user_id` (`user_id`),
  KEY `ix_corporate_client_parks_active_by_client` (`corporate_client_id`,`registration_removal_date`),
  CONSTRAINT `corporate_client_parks_car_id` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`),
  CONSTRAINT `corporate_client_parks_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_corporate_client_parks_client_id` FOREIGN KEY (`corporate_client_id`) REFERENCES `corporate_client` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=792922 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `corporate_client_purchases` */

DROP TABLE IF EXISTS `corporate_client_purchases`;

CREATE TABLE `corporate_client_purchases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `corporate_client_id` int(11) NOT NULL,
  `average_purchases_per_year` int(11) DEFAULT NULL,
  `average_purchase_price` int(11) DEFAULT NULL,
  `purchases_in_current_year` int(11) DEFAULT NULL,
  `last_purchase_date` date DEFAULT NULL,
  `won_by_uaz` int(11) DEFAULT NULL,
  `car_contracts` int(11) DEFAULT NULL,
  `car_issues` int(11) DEFAULT NULL,
  `issued_by_previous_year_contracts` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `corporate_client_purchases_corporate_client_id` (`corporate_client_id`),
  CONSTRAINT `corporate_client_purchases_corporate_client_id` FOREIGN KEY (`corporate_client_id`) REFERENCES `corporate_client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `corporate_client_sales_forecast` */

DROP TABLE IF EXISTS `corporate_client_sales_forecast`;

CREATE TABLE `corporate_client_sales_forecast` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `calculation_id` int(11) NOT NULL,
  `serial_model_id` int(11) NOT NULL,
  `section` int(11) NOT NULL,
  `year` smallint(6) NOT NULL,
  `value` int(11) NOT NULL,
  `potential` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_corporate_client_sales_forecast_calculation_id` (`calculation_id`),
  KEY `fk_corporate_client_sales_forecast_serial_model_id` (`serial_model_id`),
  CONSTRAINT `fk_corporate_client_sales_forecast_calculation_id` FOREIGN KEY (`calculation_id`) REFERENCES `corporate_client_sales_forecast_calculation` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_corporate_client_sales_forecast_serial_model_id` FOREIGN KEY (`serial_model_id`) REFERENCES `serial_model` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=85151 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `corporate_client_sales_forecast_calculation` */

DROP TABLE IF EXISTS `corporate_client_sales_forecast_calculation`;

CREATE TABLE `corporate_client_sales_forecast_calculation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `corporate_client_id` int(11) NOT NULL,
  `year` smallint(6) NOT NULL,
  `created_at` datetime NOT NULL,
  `calculated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cc_sales_forecast_calculation_corporate_client_id` (`corporate_client_id`),
  CONSTRAINT `fk_cc_sales_forecast_calculation_corporate_client_id` FOREIGN KEY (`corporate_client_id`) REFERENCES `corporate_client` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1244 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `corporate_client_service_contracts` */

DROP TABLE IF EXISTS `corporate_client_service_contracts`;

CREATE TABLE `corporate_client_service_contracts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `corporate_client_id` int(11) NOT NULL,
  `contract_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contract_date` date NOT NULL,
  `active_till` date DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `model_id` int(11) DEFAULT NULL,
  `service_cost_per_car` decimal(10,2) NOT NULL,
  `cars_amount` int(11) NOT NULL,
  `contract_price` decimal(10,2) NOT NULL,
  `details` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `corporate_client_service_contracts_corporate_client_id` (`corporate_client_id`),
  KEY `corporate_client_service_contracts_model_id` (`model_id`),
  CONSTRAINT `corporate_client_service_contracts_corporate_client_id` FOREIGN KEY (`corporate_client_id`) REFERENCES `corporate_client` (`id`),
  CONSTRAINT `corporate_client_service_contracts_model_id` FOREIGN KEY (`model_id`) REFERENCES `ref_model_class_segment` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `corporate_client_upload_history` */

DROP TABLE IF EXISTS `corporate_client_upload_history`;

CREATE TABLE `corporate_client_upload_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `address_check_status` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `import_status` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `corporate_sales_fact` */

DROP TABLE IF EXISTS `corporate_sales_fact`;

CREATE TABLE `corporate_sales_fact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `corporate_client_id` int(11) NOT NULL,
  `serial_model_id` int(11) NOT NULL,
  `year` smallint(6) NOT NULL,
  `month` tinyint(3) NOT NULL,
  `value` int(11) NOT NULL,
  `source` tinyint(3) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL,
  `market` tinyint(3) DEFAULT NULL,
  `year_sold` int(11) DEFAULT NULL,
  `sale_fact_type` tinyint(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_corporate_sales_fact_corporate_client_id` (`corporate_client_id`),
  KEY `fk_corporate_sales_fact_serial_model_id` (`serial_model_id`),
  CONSTRAINT `fk_corporate_sales_fact_corporate_client_id` FOREIGN KEY (`corporate_client_id`) REFERENCES `corporate_client` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_corporate_sales_fact_serial_model_id` FOREIGN KEY (`serial_model_id`) REFERENCES `serial_model` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6155 DEFAULT CHARSET=utf8;

/*Table structure for table `corporate_sales_forecast` */

DROP TABLE IF EXISTS `corporate_sales_forecast`;

CREATE TABLE `corporate_sales_forecast` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `corporate_client_id` int(11) NOT NULL,
  `serial_model_id` int(11) NOT NULL,
  `year` smallint(6) NOT NULL,
  `month` tinyint(3) NOT NULL,
  `value` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_corporate_sales_forecast_corporate_client_id` (`corporate_client_id`),
  KEY `fk_corporate_sales_forecast_serial_model_id` (`serial_model_id`),
  CONSTRAINT `fk_corporate_sales_forecast_corporate_client_id` FOREIGN KEY (`corporate_client_id`) REFERENCES `corporate_client` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_corporate_sales_forecast_serial_model_id` FOREIGN KEY (`serial_model_id`) REFERENCES `serial_model` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6022 DEFAULT CHARSET=utf8;

/*Table structure for table `corporate_sales_forecast_by_quarter` */

DROP TABLE IF EXISTS `corporate_sales_forecast_by_quarter`;

CREATE TABLE `corporate_sales_forecast_by_quarter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `corporate_client_id` int(11) NOT NULL,
  `serial_model_id` int(11) NOT NULL,
  `year` smallint(6) NOT NULL,
  `quarter` tinyint(3) NOT NULL,
  `value` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_corporate_sales_forecast_by_quarter_corporate_client_id` (`corporate_client_id`),
  KEY `fk_corporate_sales_forecast_by_quarter_serial_model_id` (`serial_model_id`),
  CONSTRAINT `fk_corporate_sales_forecast_by_quarter_corporate_client_id` FOREIGN KEY (`corporate_client_id`) REFERENCES `corporate_client` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_corporate_sales_forecast_by_quarter_serial_model_id` FOREIGN KEY (`serial_model_id`) REFERENCES `serial_model` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1415 DEFAULT CHARSET=utf8;

/*Table structure for table `corporate_sales_forecast_by_year` */

DROP TABLE IF EXISTS `corporate_sales_forecast_by_year`;

CREATE TABLE `corporate_sales_forecast_by_year` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `corporate_client_id` int(11) NOT NULL,
  `serial_model_id` int(11) NOT NULL,
  `year` smallint(6) NOT NULL,
  `value` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_corporate_sales_forecast_by_year_corporate_client_id` (`corporate_client_id`),
  KEY `fk_corporate_sales_forecast_by_year_serial_model_id` (`serial_model_id`),
  CONSTRAINT `fk_corporate_sales_forecast_by_year_corporate_client_id` FOREIGN KEY (`corporate_client_id`) REFERENCES `corporate_client` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_corporate_sales_forecast_by_year_serial_model_id` FOREIGN KEY (`serial_model_id`) REFERENCES `serial_model` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=895 DEFAULT CHARSET=utf8;

/*Table structure for table `corporate_sales_plan` */

DROP TABLE IF EXISTS `corporate_sales_plan`;

CREATE TABLE `corporate_sales_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `corporate_client_id` int(11) NOT NULL,
  `serial_model_id` int(11) NOT NULL,
  `year` smallint(6) NOT NULL,
  `month` tinyint(3) NOT NULL,
  `value` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_corporate_sales_plan_corporate_client_id` (`corporate_client_id`),
  KEY `fk_corporate_sales_plan_serial_model_id` (`serial_model_id`),
  KEY `fk_corporate_sales_plan_created_by` (`created_by`),
  CONSTRAINT `fk_corporate_sales_plan_corporate_client_id` FOREIGN KEY (`corporate_client_id`) REFERENCES `corporate_client` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_corporate_sales_plan_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_corporate_sales_plan_serial_model_id` FOREIGN KEY (`serial_model_id`) REFERENCES `serial_model` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=162 DEFAULT CHARSET=utf8;

/*Table structure for table `country_mapping` */

DROP TABLE IF EXISTS `country_mapping`;

CREATE TABLE `country_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `gibdd_upload_history_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `dadata_query_cache` */

DROP TABLE IF EXISTS `dadata_query_cache`;

CREATE TABLE `dadata_query_cache` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `raw_query` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `correct_address` tinyint(1) NOT NULL,
  `lat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lng` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1334 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `db_for_lvc` */

DROP TABLE IF EXISTS `db_for_lvc`;

CREATE TABLE `db_for_lvc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_db_for_lvc_created_by` (`created_by`),
  KEY `fk_db_for_lvc_updated_by` (`updated_by`),
  CONSTRAINT `fk_db_for_lvc_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_db_for_lvc_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `dc_concept` */

DROP TABLE IF EXISTS `dc_concept`;

CREATE TABLE `dc_concept` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `dc_placing_format` */

DROP TABLE IF EXISTS `dc_placing_format`;

CREATE TABLE `dc_placing_format` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dc_placing_format_created_by` (`created_by`),
  KEY `fk_dc_placing_format_updated_by` (`updated_by`),
  CONSTRAINT `fk_dc_placing_format_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_dc_placing_format_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `dc_status` */

DROP TABLE IF EXISTS `dc_status`;

CREATE TABLE `dc_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dc_status_created_by` (`created_by`),
  KEY `fk_dc_status_updated_by` (`updated_by`),
  CONSTRAINT `fk_dc_status_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_dc_status_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `dealer` */

DROP TABLE IF EXISTS `dealer`;

CREATE TABLE `dealer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `salon_id` int(11) NOT NULL,
  `bdm_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `english_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `rop` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `database` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `connectionstring` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `inactive` tinyint(1) NOT NULL,
  `only_report` int(4) NOT NULL,
  `report_login` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `report_password_hash` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `report_first_period_start` date DEFAULT NULL,
  `rop_contacts` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `start_reporting_at` date DEFAULT NULL,
  `inactive_since` date DEFAULT NULL,
  `inactive_since_service` date DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service_center_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_cars_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_cars_address_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_spare_parts_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `legal_form` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kpp` varchar(9) COLLATE utf8_unicode_ci DEFAULT NULL,
  `settlement_account` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bik` varchar(9) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ogrn` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inn` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `juridical_zip_code` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `juridical_region_id` int(11) DEFAULT NULL,
  `juridical_city_id` int(11) DEFAULT NULL,
  `juridical_street` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `juridical_house` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `juridical_building` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contract_number` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contract_signing_date` date DEFAULT NULL,
  `zip_code` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sm_id` int(11) DEFAULT NULL,
  `srv_id` int(11) DEFAULT NULL,
  `crm_id` int(11) DEFAULT NULL,
  `dealer_group_id` int(11) DEFAULT NULL,
  `dsf_enabled` tinyint(1) NOT NULL DEFAULT 0,
  `holding_id` int(11) DEFAULT NULL,
  `marketing_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `register_company_at` date DEFAULT NULL,
  `register_dealer_at` date DEFAULT NULL,
  `is_subaru_center` tinyint(1) NOT NULL,
  `lms_support` tinyint(1) NOT NULL,
  `contract_type` int(11) DEFAULT NULL,
  `contract_finish_at` int(11) DEFAULT NULL,
  `employees` int(11) DEFAULT NULL,
  `land_area` double DEFAULT NULL,
  `building_area` double DEFAULT NULL,
  `authorized_capital` double DEFAULT NULL,
  `brand_list` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `correspondent_account` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `okpo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `statistic_type` enum('manual','api','crm') COLLATE utf8_unicode_ci DEFAULT NULL,
  `work_time` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city_id` int(11) DEFAULT NULL,
  `service_legal_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service_marketing_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service_deactivated_at` date DEFAULT NULL,
  `dealer_type` smallint(6) DEFAULT NULL,
  `marketing_manager_id` int(11) DEFAULT NULL,
  `marketing_agency_id` int(11) DEFAULT NULL,
  `responsible_person_id` int(11) DEFAULT NULL,
  `distributor_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `country_id` int(11) NOT NULL,
  `dc_status_id` int(11) DEFAULT NULL,
  `dc_placing_format_id` int(11) DEFAULT NULL,
  `dc_concept_id` int(11) DEFAULT NULL,
  `logo_id` int(11) DEFAULT NULL,
  `holding_website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dealers_distributor` (`distributor_id`),
  KEY `fk_dealer_dealer_group_id` (`dealer_group_id`),
  KEY `fk_dealer_holding_id` (`holding_id`),
  KEY `fk_dealer_user_id` (`user_id`),
  KEY `fk_dealer_marketing_manager` (`marketing_manager_id`),
  KEY `fk_dealer_marketing_agency` (`marketing_agency_id`),
  KEY `fk_dealer_responsible_person` (`responsible_person_id`),
  KEY `fk_dealer_dc_status_id` (`dc_status_id`),
  KEY `fk_dealer_dc_placing_format_id` (`dc_placing_format_id`),
  KEY `fk_dealer_dc_concept_id` (`dc_concept_id`),
  KEY `dealer_logo_idx` (`logo_id`),
  KEY `xUserManager` (`bdm_id`,`sm_id`,`srv_id`,`marketing_manager_id`),
  KEY `xdcStatusId` (`dc_status_id`),
  CONSTRAINT `dealer_logo_idx` FOREIGN KEY (`logo_id`) REFERENCES `upload` (`id`),
  CONSTRAINT `fk_dealer_dc_concept_id` FOREIGN KEY (`dc_concept_id`) REFERENCES `dc_concept` (`id`),
  CONSTRAINT `fk_dealer_dc_placing_format_id` FOREIGN KEY (`dc_placing_format_id`) REFERENCES `dc_placing_format` (`id`),
  CONSTRAINT `fk_dealer_dc_status_id` FOREIGN KEY (`dc_status_id`) REFERENCES `dc_status` (`id`),
  CONSTRAINT `fk_dealer_dealer_group_id` FOREIGN KEY (`dealer_group_id`) REFERENCES `dealer_group` (`id`),
  CONSTRAINT `fk_dealer_holding_id` FOREIGN KEY (`holding_id`) REFERENCES `holding` (`id`),
  CONSTRAINT `fk_dealer_marketing_agency` FOREIGN KEY (`marketing_agency_id`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_dealer_marketing_manager` FOREIGN KEY (`marketing_manager_id`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_dealer_responsible_person` FOREIGN KEY (`responsible_person_id`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_dealer_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_dealers_distributor` FOREIGN KEY (`distributor_id`) REFERENCES `distributor` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=441 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `dealer_activity` */

DROP TABLE IF EXISTS `dealer_activity`;

CREATE TABLE `dealer_activity` (
  `dealer_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  UNIQUE KEY `uk_dealer_activity_dealer_id_activity_id` (`dealer_id`,`activity_id`),
  KEY `fk_dealer_activity_user` (`user_id`),
  KEY `fk_dealer_activity_activity` (`activity_id`),
  CONSTRAINT `fk_dealer_activity_activity` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`),
  CONSTRAINT `fk_dealer_activity_dealer` FOREIGN KEY (`dealer_id`) REFERENCES `dealer` (`id`),
  CONSTRAINT `fk_dealer_activity_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `dealer_activity_info` */

DROP TABLE IF EXISTS `dealer_activity_info`;

CREATE TABLE `dealer_activity_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dealer_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `actual_from_date` date DEFAULT NULL,
  `actual_to_date` date DEFAULT NULL,
  `work_time` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body_repair_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body_repair_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` float DEFAULT NULL,
  `lng` float DEFAULT NULL,
  `has_body_repair` tinyint(3) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `history_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_dealer_activity_info_dealer_id_activity_id` (`dealer_id`,`activity_id`),
  KEY `fk_dealer_activity_info_activity` (`activity_id`),
  KEY `xdate` (`actual_from_date`,`actual_to_date`),
  KEY `fk_dealer_activity_info_history_id` (`history_id`),
  CONSTRAINT `fk_dealer_activity_info_activity` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`),
  CONSTRAINT `fk_dealer_activity_info_dealer` FOREIGN KEY (`dealer_id`) REFERENCES `dealer` (`id`),
  CONSTRAINT `fk_dealer_activity_info_history_id` FOREIGN KEY (`history_id`) REFERENCES `dealer_activity_info_history` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `dealer_activity_info_history` */

DROP TABLE IF EXISTS `dealer_activity_info_history`;

CREATE TABLE `dealer_activity_info_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dealer_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `actual_from_date` date DEFAULT NULL,
  `actual_to_date` date DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dealer_activity_info_h_activity` (`activity_id`),
  KEY `fk_dealer_activity_info_h_dealer` (`dealer_id`),
  CONSTRAINT `fk_dealer_activity_info_h_activity` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`),
  CONSTRAINT `fk_dealer_activity_info_h_dealer` FOREIGN KEY (`dealer_id`) REFERENCES `dealer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `dealer_change_history` */

DROP TABLE IF EXISTS `dealer_change_history`;

CREATE TABLE `dealer_change_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dealer_id` int(11) NOT NULL,
  `comment` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dealer_change_history_dealer` (`dealer_id`),
  KEY `fk_dealer_change_history_user` (`user_id`),
  CONSTRAINT `fk_dealer_change_history_dealer` FOREIGN KEY (`dealer_id`) REFERENCES `dealer` (`id`),
  CONSTRAINT `fk_dealer_change_history_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2129 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `dealer_comment` */

DROP TABLE IF EXISTS `dealer_comment`;

CREATE TABLE `dealer_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dealer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `text` text DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dealer_comment_user` (`user_id`),
  KEY `fk_dealer_comment_dealers` (`dealer_id`),
  CONSTRAINT `fk_dealer_comment_dealers` FOREIGN KEY (`dealer_id`) REFERENCES `dealer` (`id`),
  CONSTRAINT `fk_dealer_comment_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `dealer_company` */

DROP TABLE IF EXISTS `dealer_company`;

CREATE TABLE `dealer_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dealer_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dealer_company_dealer` (`dealer_id`),
  KEY `fk_dealer_company_company` (`company_id`),
  KEY `fk_dealer_company_created_by` (`created_by`),
  CONSTRAINT `fk_dealer_company_company` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  CONSTRAINT `fk_dealer_company_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_dealer_company_dealer` FOREIGN KEY (`dealer_id`) REFERENCES `dealer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `dealer_contact` */

DROP TABLE IF EXISTS `dealer_contact`;

CREATE TABLE `dealer_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_type_id` int(11) NOT NULL,
  `dealer_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dealer_contact_created_by` (`created_by`),
  KEY `fk_dealer_contact_updated_by` (`updated_by`),
  KEY `fk_dealer_contact_dealer_id` (`dealer_id`),
  KEY `fk_dealer_contact_type_id` (`contact_type_id`),
  CONSTRAINT `fk_dealer_contact_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_dealer_contact_dealer_id` FOREIGN KEY (`dealer_id`) REFERENCES `dealer` (`id`),
  CONSTRAINT `fk_dealer_contact_type_id` FOREIGN KEY (`contact_type_id`) REFERENCES `dealer_contact_type` (`id`),
  CONSTRAINT `fk_dealer_contact_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `dealer_contact_category` */

DROP TABLE IF EXISTS `dealer_contact_category`;

CREATE TABLE `dealer_contact_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `is_required` tinyint(1) NOT NULL DEFAULT 0,
  `is_multiple` tinyint(1) NOT NULL DEFAULT 0,
  `status` smallint(6) NOT NULL,
  `order` int(11) NOT NULL,
  `contact_group_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dealer_contact_category_created_by` (`created_by`),
  KEY `fk_dealer_contact_category_updated_by` (`updated_by`),
  KEY `fk_dealer_contact_category_group_id` (`contact_group_id`),
  CONSTRAINT `fk_dealer_contact_category_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_dealer_contact_category_group_id` FOREIGN KEY (`contact_group_id`) REFERENCES `dealer_contact_group` (`id`),
  CONSTRAINT `fk_dealer_contact_category_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `dealer_contact_group` */

DROP TABLE IF EXISTS `dealer_contact_group`;

CREATE TABLE `dealer_contact_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL,
  `order` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dealer_contact_group_created_by` (`created_by`),
  KEY `fk_dealer_contact_group_updated_by` (`updated_by`),
  CONSTRAINT `fk_dealer_contact_group_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_dealer_contact_group_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `dealer_contact_type` */

DROP TABLE IF EXISTS `dealer_contact_type`;

CREATE TABLE `dealer_contact_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `status` smallint(6) NOT NULL,
  `order` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dealer_contact_type_created_by` (`created_by`),
  KEY `fk_dealer_contact_type_updated_by` (`updated_by`),
  CONSTRAINT `fk_dealer_contact_type_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_dealer_contact_type_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `dealer_corporate_sale` */

DROP TABLE IF EXISTS `dealer_corporate_sale`;

CREATE TABLE `dealer_corporate_sale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dealer_id` int(11) NOT NULL,
  `upload_id` int(11) NOT NULL,
  `contract_number` varchar(255) DEFAULT NULL,
  `contract_date` date DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dealer_corporate_sale-dealer-fk` (`dealer_id`),
  KEY `dealer_corporate_sale-upload-fk` (`upload_id`),
  KEY `dealer_corporate_sale-user-fk` (`user_id`),
  CONSTRAINT `dealer_corporate_sale-dealer-fk` FOREIGN KEY (`dealer_id`) REFERENCES `dealer` (`id`),
  CONSTRAINT `dealer_corporate_sale-upload-fk` FOREIGN KEY (`upload_id`) REFERENCES `upload` (`id`),
  CONSTRAINT `dealer_corporate_sale-user-fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `dealer_design_project` */

DROP TABLE IF EXISTS `dealer_design_project`;

CREATE TABLE `dealer_design_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dealer_id` int(11) NOT NULL,
  `agreement_date` date DEFAULT NULL,
  `implementation_date` date DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dealer_design_project_dealer_id` (`dealer_id`),
  CONSTRAINT `fk_dealer_design_project_dealer_id` FOREIGN KEY (`dealer_id`) REFERENCES `dealer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=441 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `dealer_document` */

DROP TABLE IF EXISTS `dealer_document`;

CREATE TABLE `dealer_document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dealer_id` int(11) NOT NULL,
  `upload_id` int(11) NOT NULL,
  `document_type_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dealer_document_dealer` (`dealer_id`),
  KEY `fk_dealer_document_upload` (`upload_id`),
  KEY `fk_dealer_document_user` (`user_id`),
  KEY `fk_dealer_document_document_type` (`document_type_id`),
  CONSTRAINT `fk_dealer_document_dealer` FOREIGN KEY (`dealer_id`) REFERENCES `dealer` (`id`),
  CONSTRAINT `fk_dealer_document_document_type` FOREIGN KEY (`document_type_id`) REFERENCES `document_type` (`id`),
  CONSTRAINT `fk_dealer_document_upload` FOREIGN KEY (`upload_id`) REFERENCES `upload` (`id`),
  CONSTRAINT `fk_dealer_document_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `dealer_group` */

DROP TABLE IF EXISTS `dealer_group`;

CREATE TABLE `dealer_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dealer_group_user` (`user_id`),
  CONSTRAINT `fk_dealer_group_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `dealer_media` */

DROP TABLE IF EXISTS `dealer_media`;

CREATE TABLE `dealer_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `upload_id` int(11) NOT NULL,
  `type` smallint(6) NOT NULL,
  `media_type_id` int(11) NOT NULL,
  `dealer_id` int(11) NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `comment` varchar(255) DEFAULT '',
  `preview_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dealer_media_created_by` (`created_by`),
  KEY `fk_dealer_media_updated_by` (`updated_by`),
  KEY `fk_dealer_media_dealer_id` (`dealer_id`),
  KEY `fk_dealer_media_upload_id` (`upload_id`),
  KEY `fk_dealer_media_media_type_id` (`media_type_id`),
  KEY `dealer_media_preview_fk` (`preview_id`),
  CONSTRAINT `dealer_media_preview_fk` FOREIGN KEY (`preview_id`) REFERENCES `upload` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_dealer_media_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_dealer_media_dealer_id` FOREIGN KEY (`dealer_id`) REFERENCES `dealer` (`id`),
  CONSTRAINT `fk_dealer_media_media_type_id` FOREIGN KEY (`media_type_id`) REFERENCES `media_type` (`id`),
  CONSTRAINT `fk_dealer_media_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_dealer_media_upload_id` FOREIGN KEY (`upload_id`) REFERENCES `upload` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=867 DEFAULT CHARSET=utf8;

/*Table structure for table `dealer_media_tag` */

DROP TABLE IF EXISTS `dealer_media_tag`;

CREATE TABLE `dealer_media_tag` (
  `dealer_media_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  UNIQUE KEY `uk_dealer_media_tag_media_id_tag_id` (`dealer_media_id`,`tag_id`),
  KEY `fk_dealer_media_tag_user` (`user_id`),
  KEY `fk_dealer_media_tag_tag` (`tag_id`),
  CONSTRAINT `fk_dealer_media_tag_dealer` FOREIGN KEY (`dealer_media_id`) REFERENCES `dealer_media` (`id`),
  CONSTRAINT `fk_dealer_media_tag_tag` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`),
  CONSTRAINT `fk_dealer_media_tag_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `dealer_parking` */

DROP TABLE IF EXISTS `dealer_parking`;

CREATE TABLE `dealer_parking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dealer_id` int(11) NOT NULL,
  `customer_dc` int(11) NOT NULL DEFAULT 0,
  `customer_service` int(11) NOT NULL DEFAULT 0,
  `test_drive_rc` int(11) NOT NULL DEFAULT 0,
  `test_drive_lcv` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dealer_parking_dealer_id` (`dealer_id`),
  CONSTRAINT `fk_dealer_parking_dealer_id` FOREIGN KEY (`dealer_id`) REFERENCES `dealer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=441 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `dealer_poll` */

DROP TABLE IF EXISTS `dealer_poll`;

CREATE TABLE `dealer_poll` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poll_id` int(11) NOT NULL,
  `dealer_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `total_earned_points` decimal(19,4) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `responsible` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active_from` date DEFAULT NULL,
  `active_to` date DEFAULT NULL,
  `is_taken_to_work` tinyint(1) DEFAULT 0,
  `is_blocked` tinyint(1) DEFAULT 0,
  `block_at` datetime DEFAULT NULL,
  `is_monitoring` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Добавлен на монитринг',
  PRIMARY KEY (`id`),
  KEY `index_poll_id` (`poll_id`),
  KEY `fk_dealer_poll_dealer` (`dealer_id`),
  KEY `fk_dealer_poll_user` (`user_id`),
  KEY `xIsMonitoring` (`is_monitoring`),
  CONSTRAINT `fk_dealer_poll_dealer` FOREIGN KEY (`dealer_id`) REFERENCES `dealer` (`id`),
  CONSTRAINT `fk_dealer_poll_poll` FOREIGN KEY (`poll_id`) REFERENCES `poll` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_dealer_poll_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=484 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `dealer_poll_tag` */

DROP TABLE IF EXISTS `dealer_poll_tag`;

CREATE TABLE `dealer_poll_tag` (
  `dealer_poll_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  UNIQUE KEY `uk_task_tag_dealer_poll_id_tag_id` (`dealer_poll_id`,`tag_id`),
  KEY `fk_dealer_poll_tag_user` (`user_id`),
  CONSTRAINT `fk_dealer_poll_tag_dealer_poll` FOREIGN KEY (`dealer_poll_id`) REFERENCES `dealer_poll` (`id`),
  CONSTRAINT `fk_dealer_poll_tag_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `dealer_service` */

DROP TABLE IF EXISTS `dealer_service`;

CREATE TABLE `dealer_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dealer_id` int(11) NOT NULL,
  `space` float NOT NULL DEFAULT 0,
  `gate_height` float NOT NULL DEFAULT 0,
  `space_height` float NOT NULL DEFAULT 0,
  `observation_pit` int(11) NOT NULL DEFAULT 0,
  `car_lifts_lcv` int(11) NOT NULL DEFAULT 0,
  `stations` int(11) NOT NULL DEFAULT 0,
  `uaz_stations` int(11) NOT NULL DEFAULT 0,
  `car_wash_stations` int(11) NOT NULL DEFAULT 0,
  `installation_area` int(11) NOT NULL DEFAULT 0,
  `cart_lifts` int(11) NOT NULL DEFAULT 0,
  `cart_lifts_uaz` int(11) NOT NULL DEFAULT 0,
  `has_wheel_alignment_stand` int(11) NOT NULL DEFAULT 0,
  `has_breaks_stand` int(11) NOT NULL DEFAULT 0,
  `has_diagnostic_equipment` int(11) NOT NULL DEFAULT 0,
  `has_special_tools` int(11) NOT NULL DEFAULT 0,
  `mkc_workshop_space` float NOT NULL DEFAULT 0,
  `mkc_prepare_station` int(11) NOT NULL DEFAULT 0,
  `spray_booth` int(11) NOT NULL DEFAULT 0,
  `spare_parts_warehouse_space` float NOT NULL DEFAULT 0,
  `uaz_spare_parts_warehouse_space` float NOT NULL DEFAULT 0,
  `has_address_storage` int(11) NOT NULL DEFAULT 0,
  `replacement_cars` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dealer_service_dealer_id` (`dealer_id`),
  CONSTRAINT `fk_dealer_service_dealer_id` FOREIGN KEY (`dealer_id`) REFERENCES `dealer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=441 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `dealer_showroom` */

DROP TABLE IF EXISTS `dealer_showroom`;

CREATE TABLE `dealer_showroom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dealer_id` int(11) NOT NULL,
  `floor` int(11) NOT NULL DEFAULT 1,
  `space` float NOT NULL DEFAULT 0,
  `car_place_rc` int(11) NOT NULL DEFAULT 0,
  `car_place_lcv` int(11) NOT NULL DEFAULT 0,
  `space_height` float NOT NULL DEFAULT 0,
  `gate_height` float NOT NULL DEFAULT 0,
  `warehouse_space` float NOT NULL DEFAULT 0,
  `warehouse_car_place` float NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dealer_showroom_dealer_id` (`dealer_id`),
  CONSTRAINT `fk_dealer_showroom_dealer_id` FOREIGN KEY (`dealer_id`) REFERENCES `dealer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=441 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `dealer_staff` */

DROP TABLE IF EXISTS `dealer_staff`;

CREATE TABLE `dealer_staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dealer_id` int(11) NOT NULL,
  `total` int(11) NOT NULL DEFAULT 0,
  `administrative` int(11) NOT NULL DEFAULT 0,
  `marketing` int(11) NOT NULL DEFAULT 0,
  `sales` int(11) NOT NULL DEFAULT 0,
  `service` int(11) NOT NULL DEFAULT 0,
  `sales_head` int(11) NOT NULL DEFAULT 0,
  `sales_consultant` int(11) NOT NULL DEFAULT 0,
  `corp_sales_consultant` int(11) NOT NULL DEFAULT 0,
  `productive` int(11) NOT NULL DEFAULT 0,
  `workshop_master` int(11) NOT NULL DEFAULT 0,
  `mechanic` int(11) NOT NULL DEFAULT 0,
  `aggregator` int(11) NOT NULL DEFAULT 0,
  `diagnostician` int(11) NOT NULL DEFAULT 0,
  `non_productive` int(11) NOT NULL DEFAULT 0,
  `spare_parts_sales` int(11) NOT NULL DEFAULT 0,
  `master_receptionist` int(11) NOT NULL DEFAULT 0,
  `warranty_engineer` int(11) NOT NULL DEFAULT 0,
  `rop_training_percent` int(11) NOT NULL DEFAULT 0,
  `sales_training_percent` int(11) NOT NULL DEFAULT 0,
  `corp_pk_training_percent` int(11) NOT NULL DEFAULT 0,
  `head_service_training_percent` int(11) NOT NULL DEFAULT 0,
  `mechanic_training_percent` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dealer_staff_dealer_id` (`dealer_id`),
  CONSTRAINT `fk_dealer_staff_dealer_id` FOREIGN KEY (`dealer_id`) REFERENCES `dealer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=441 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `dealer_survey` */

DROP TABLE IF EXISTS `dealer_survey`;

CREATE TABLE `dealer_survey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dealer_poll_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_id` int(11) DEFAULT NULL,
  `any_answer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kpi_answer` tinyint(1) DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `in_action_plan` tinyint(1) DEFAULT 0,
  `is_approved` tinyint(1) DEFAULT NULL,
  `point` decimal(19,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx` (`dealer_poll_id`,`question_id`),
  KEY `index_answer_id` (`answer_id`),
  KEY `index_question_id` (`question_id`),
  KEY `index_dealer_poll_id` (`dealer_poll_id`),
  CONSTRAINT `fk_dealer_survey_dealer_poll` FOREIGN KEY (`dealer_poll_id`) REFERENCES `dealer_poll` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_dealer_survey_poll_answer` FOREIGN KEY (`answer_id`) REFERENCES `poll_answer` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_dealer_survey_poll_question` FOREIGN KEY (`question_id`) REFERENCES `poll_question` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `dealer_survey_attachment` */

DROP TABLE IF EXISTS `dealer_survey_attachment`;

CREATE TABLE `dealer_survey_attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dealer_survey_id` int(11) NOT NULL,
  `upload_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dealer_survey_attachment_dealer_survey` (`dealer_survey_id`),
  KEY `fk_dealer_survey_attachment_upload` (`upload_id`),
  KEY `fk_dealer_survey_attachment_user` (`user_id`),
  CONSTRAINT `fk_dealer_survey_attachment_dealer_survey` FOREIGN KEY (`dealer_survey_id`) REFERENCES `dealer_survey` (`id`),
  CONSTRAINT `fk_dealer_survey_attachment_upload` FOREIGN KEY (`upload_id`) REFERENCES `upload` (`id`),
  CONSTRAINT `fk_dealer_survey_attachment_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `dealer_tag` */

DROP TABLE IF EXISTS `dealer_tag`;

CREATE TABLE `dealer_tag` (
  `dealer_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  UNIQUE KEY `uk_dealer_tag_dealer_id_tag_id` (`dealer_id`,`tag_id`),
  KEY `fk_dealer_tag_user` (`user_id`),
  KEY `fk_dealer_tag_tag` (`tag_id`),
  CONSTRAINT `fk_dealer_tag_dealer` FOREIGN KEY (`dealer_id`) REFERENCES `dealer` (`id`),
  CONSTRAINT `fk_dealer_tag_tag` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`),
  CONSTRAINT `fk_dealer_tag_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `dealer_tag_directory` */

DROP TABLE IF EXISTS `dealer_tag_directory`;

CREATE TABLE `dealer_tag_directory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `ord` int(11) NOT NULL,
  `type` smallint(6) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dealer_tag_directory_user` (`user_id`),
  CONSTRAINT `fk_dealer_tag_directory_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `decision_criteria` */

DROP TABLE IF EXISTS `decision_criteria`;

CREATE TABLE `decision_criteria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(3) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_decision_criteria_created_by` (`created_by`),
  KEY `fk_decision_criteria_updated_by` (`updated_by`),
  CONSTRAINT `fk_decision_criteria_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_decision_criteria_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `department` */

DROP TABLE IF EXISTS `department`;

CREATE TABLE `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `status` smallint(6) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `fk_department_created_by` (`created_by`),
  KEY `fk_department_updated_by` (`updated_by`),
  CONSTRAINT `fk_department_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_department_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `design_project_document` */

DROP TABLE IF EXISTS `design_project_document`;

CREATE TABLE `design_project_document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `upload_id` int(11) NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 0,
  `design_project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_design_project_document_project` (`design_project_id`),
  KEY `fk_design_project_document_upload` (`upload_id`),
  KEY `fk_design_project_document_user` (`user_id`),
  CONSTRAINT `fk_design_project_document_project` FOREIGN KEY (`design_project_id`) REFERENCES `dealer_design_project` (`id`),
  CONSTRAINT `fk_design_project_document_upload` FOREIGN KEY (`upload_id`) REFERENCES `upload` (`id`),
  CONSTRAINT `fk_design_project_document_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `distributor` */

DROP TABLE IF EXISTS `distributor`;

CREATE TABLE `distributor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `marketing_name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_distributor_user` (`user_id`),
  CONSTRAINT `fk_distributor_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `distributor_contact` */

DROP TABLE IF EXISTS `distributor_contact`;

CREATE TABLE `distributor_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `corporate_client_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_distributor_contact_user` (`user_id`),
  KEY `fk_distributor_contact_corporate_client` (`created_by`),
  CONSTRAINT `fk_distributor_contact_corporate_client` FOREIGN KEY (`created_by`) REFERENCES `corporate_client` (`id`),
  CONSTRAINT `fk_distributor_contact_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_distributor_contact_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `document_type` */

DROP TABLE IF EXISTS `document_type`;

CREATE TABLE `document_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `fk_document_type_user` (`user_id`),
  CONSTRAINT `fk_document_type_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `engine_type` */

DROP TABLE IF EXISTS `engine_type`;

CREATE TABLE `engine_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_engine_type_created_by` (`created_by`),
  KEY `fk_engine_type_updated_by` (`updated_by`),
  CONSTRAINT `fk_engine_type_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_engine_type_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `enterprise_type` */

DROP TABLE IF EXISTS `enterprise_type`;

CREATE TABLE `enterprise_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `entity_type` */

DROP TABLE IF EXISTS `entity_type`;

CREATE TABLE `entity_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_entity_type_created_by` (`created_by`),
  KEY `fk_entity_type_updated_by` (`updated_by`),
  CONSTRAINT `fk_entity_type_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_entity_type_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `export_task` */

DROP TABLE IF EXISTS `export_task`;

CREATE TABLE `export_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `upload_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `done_at` datetime DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `method` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `error_message` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `gibdd_import_schema` */

DROP TABLE IF EXISTS `gibdd_import_schema`;

CREATE TABLE `gibdd_import_schema` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rules` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gibdd_import_schema_created_by` (`created_by`),
  CONSTRAINT `fk_gibdd_import_schema_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `gibdd_import_state` */

DROP TABLE IF EXISTS `gibdd_import_state`;

CREATE TABLE `gibdd_import_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `process_status` tinyint(3) DEFAULT NULL,
  `header_in_first_row` tinyint(3) NOT NULL DEFAULT 0,
  `car_with_same_registration_date_exists` tinyint(3) DEFAULT NULL,
  `existing_car_error_year` int(11) DEFAULT NULL,
  `existing_car_error_month` int(11) DEFAULT NULL,
  `success_rows` int(11) DEFAULT NULL,
  `error_rows` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `columns` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_rows` int(11) DEFAULT NULL,
  `last_error` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `period_from` date DEFAULT NULL,
  `period_to` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `gibdd_upload_error` */

DROP TABLE IF EXISTS `gibdd_upload_error`;

CREATE TABLE `gibdd_upload_error` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `history_id` int(11) NOT NULL,
  `vin` varchar(255) DEFAULT NULL,
  `engine_number` varchar(255) DEFAULT NULL,
  `body_number` varchar(255) DEFAULT NULL,
  `date_registered` date DEFAULT NULL,
  `registration_type` varchar(255) DEFAULT NULL,
  `category` tinyint(3) DEFAULT NULL,
  `text` text DEFAULT NULL,
  `trace` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gibdd_upload_error_history_id` (`history_id`),
  CONSTRAINT `fk_gibdd_upload_error_history_id` FOREIGN KEY (`history_id`) REFERENCES `gibdd_upload_history` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1641 DEFAULT CHARSET=utf8;

/*Table structure for table `gibdd_upload_history` */

DROP TABLE IF EXISTS `gibdd_upload_history`;

CREATE TABLE `gibdd_upload_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `period_from` date DEFAULT NULL,
  `period_to` date DEFAULT NULL,
  `import_status` int(11) NOT NULL DEFAULT 0,
  `imported_success` int(11) NOT NULL DEFAULT 0,
  `imported_errors` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `registration_place_check` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `import_data` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_error` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `history_change` */

DROP TABLE IF EXISTS `history_change`;

CREATE TABLE `history_change` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_name` varchar(255) NOT NULL,
  `model_id` int(11) NOT NULL,
  `attribute` varchar(255) NOT NULL,
  `old_value` text DEFAULT NULL,
  `new_value` text DEFAULT NULL,
  `action` int(11) DEFAULT NULL,
  `change_at` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `comment` text DEFAULT NULL,
  `custom_name_action` varchar(255) DEFAULT NULL COMMENT 'Custom action name',
  `action_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_history_change_model_name` (`model_name`),
  KEY `idx_history_change_model_id` (`model_id`),
  KEY `idx_history_change_attribute` (`attribute`),
  KEY `idx_history_change_user_id` (`user_id`),
  KEY `idx_history_change_action` (`action`),
  KEY `idx_history_change_change_at` (`change_at`),
  KEY `xactionUserId` (`action_user_id`),
  CONSTRAINT `fk_history_change_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1537 DEFAULT CHARSET=utf8;

/*Table structure for table `holding` */

DROP TABLE IF EXISTS `holding`;

CREATE TABLE `holding` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brand_list` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_holding_created_by` (`created_by`),
  KEY `fk_holding_updated_by` (`updated_by`),
  CONSTRAINT `fk_holding_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_holding_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=623 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `holding_company` */

DROP TABLE IF EXISTS `holding_company`;

CREATE TABLE `holding_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_holding_company_parent` (`parent_id`),
  KEY `holding_company_inn_idx` (`inn`),
  CONSTRAINT `fk_holding_company_parent` FOREIGN KEY (`parent_id`) REFERENCES `holding_company` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=866 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `holding_mapping` */

DROP TABLE IF EXISTS `holding_mapping`;

CREATE TABLE `holding_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `holding_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `competitor_upload_history_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `holding_mapping_holding_idx` (`holding_id`),
  KEY `holding_mapping_update_idx` (`competitor_upload_history_id`),
  CONSTRAINT `holding_mapping_holding_idx` FOREIGN KEY (`holding_id`) REFERENCES `holding` (`id`),
  CONSTRAINT `holding_mapping_update_idx` FOREIGN KEY (`competitor_upload_history_id`) REFERENCES `competitor_upload_history` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=597 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `media_type` */

DROP TABLE IF EXISTS `media_type`;

CREATE TABLE `media_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `max_photos` int(11) NOT NULL,
  `max_videos` int(11) NOT NULL,
  `status` smallint(6) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_media_type_created_by` (`created_by`),
  KEY `fk_media_type_updated_by` (`updated_by`),
  CONSTRAINT `fk_media_type_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_media_type_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `migration` */

DROP TABLE IF EXISTS `migration`;

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `model_class` */

DROP TABLE IF EXISTS `model_class`;

CREATE TABLE `model_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `model_mapping` */

DROP TABLE IF EXISTS `model_mapping`;

CREATE TABLE `model_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `aeb_region_upload_history_id` int(11) DEFAULT NULL,
  `gibdd_upload_history_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `model_mapping_upload_idx` (`aeb_region_upload_history_id`),
  CONSTRAINT `model_mapping_upload_idx` FOREIGN KEY (`aeb_region_upload_history_id`) REFERENCES `aeb_region_upload_history` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2666 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `model_mapping_notification` */

DROP TABLE IF EXISTS `model_mapping_notification`;

CREATE TABLE `model_mapping_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `fk_model_mapping_notification_model_id` (`model_id`),
  CONSTRAINT `fk_model_mapping_notification_model_id` FOREIGN KEY (`model_id`) REFERENCES `ref_model_class_segment` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `model_segment` */

DROP TABLE IF EXISTS `model_segment`;

CREATE TABLE `model_segment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `okved` */

DROP TABLE IF EXISTS `okved`;

CREATE TABLE `okved` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2134 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `outdated_park_update_rule` */

DROP TABLE IF EXISTS `outdated_park_update_rule`;

CREATE TABLE `outdated_park_update_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `corporate_client_id` int(11) NOT NULL,
  `year1` int(11) NOT NULL,
  `year2` int(11) NOT NULL,
  `year3` int(11) NOT NULL,
  `year4` int(11) NOT NULL,
  `year5` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `brand_type` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `outdated_park_update_rule_corporate_client_id` (`corporate_client_id`),
  CONSTRAINT `outdated_park_update_rule_corporate_client_id` FOREIGN KEY (`corporate_client_id`) REFERENCES `corporate_client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `park_import_schema` */

DROP TABLE IF EXISTS `park_import_schema`;

CREATE TABLE `park_import_schema` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rules` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_park_import_schema_created_by` (`created_by`),
  CONSTRAINT `fk_park_import_schema_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `park_replace` */

DROP TABLE IF EXISTS `park_replace`;

CREATE TABLE `park_replace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NOT NULL,
  `to_model_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `corporate_client_id` int(11) NOT NULL,
  `percent` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_park_replace_model_id_to_model_id` (`model_id`,`to_model_id`,`corporate_client_id`),
  UNIQUE KEY `uk_park_replace_model_id` (`model_id`,`corporate_client_id`),
  KEY `fk_park_replace_to_model_id` (`to_model_id`),
  KEY `fk_park_replace_created_by` (`created_by`),
  KEY `park_replace_cc_id` (`corporate_client_id`),
  CONSTRAINT `fk_park_replace_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_park_replace_to_model_id` FOREIGN KEY (`to_model_id`) REFERENCES `ref_model_class_segment` (`id`) ON DELETE CASCADE,
  CONSTRAINT `park_replace_cc_id` FOREIGN KEY (`corporate_client_id`) REFERENCES `corporate_client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1798 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `park_upload_history` */

DROP TABLE IF EXISTS `park_upload_history`;

CREATE TABLE `park_upload_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `import_status` int(11) NOT NULL DEFAULT 0,
  `imported_success` int(11) NOT NULL DEFAULT 0,
  `imported_errors` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `import_data` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `registration_place_check` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_error` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `parks` */

DROP TABLE IF EXISTS `parks`;

CREATE TABLE `parks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  `registration_place_id` int(11) NOT NULL,
  `registration_date` date DEFAULT NULL,
  `registration_type` int(11) NOT NULL,
  `ownership_type` int(11) NOT NULL,
  `inn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_created_at` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_ocved_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ocved_code_transcript` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ocved_group` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ogrn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_parks_car_id` (`car_id`),
  KEY `fk_parks_registration_place_id` (`registration_place_id`),
  KEY `fk_parks_user_id` (`user_id`),
  CONSTRAINT `fk_parks_car_id` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`),
  CONSTRAINT `fk_parks_registration_place_id` FOREIGN KEY (`registration_place_id`) REFERENCES `registration_places` (`id`),
  CONSTRAINT `fk_parks_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52220217 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `poll` */

DROP TABLE IF EXISTS `poll`;

CREATE TABLE `poll` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL DEFAULT 0,
  `max_available_points` decimal(19,4) DEFAULT NULL,
  `actionPlanResponsibleRole` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT 0,
  `can_be_filled_by_dealer` tinyint(1) DEFAULT NULL,
  `workflow_id` int(11) DEFAULT NULL,
  `activity_direction_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_poll_created_by` (`created_by`),
  KEY `fk_poll_updated_by` (`updated_by`),
  KEY `fk_poll_workflow_id` (`workflow_id`),
  KEY `fk_poll_activity_direction` (`activity_direction_id`),
  CONSTRAINT `fk_poll_activity_direction` FOREIGN KEY (`activity_direction_id`) REFERENCES `activity_direction` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_poll_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_poll_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_poll_workflow_id` FOREIGN KEY (`workflow_id`) REFERENCES `workflow` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `poll_answer` */

DROP TABLE IF EXISTS `poll_answer`;

CREATE TABLE `poll_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `point` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_question_id` (`question_id`),
  CONSTRAINT `fk_poll_answer_poll_question` FOREIGN KEY (`question_id`) REFERENCES `poll_question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `poll_block` */

DROP TABLE IF EXISTS `poll_block`;

CREATE TABLE `poll_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poll_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_poll_id` (`poll_id`),
  CONSTRAINT `fk_poll_block_poll` FOREIGN KEY (`poll_id`) REFERENCES `poll` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `poll_block_relation` */

DROP TABLE IF EXISTS `poll_block_relation`;

CREATE TABLE `poll_block_relation` (
  `poll_id_parent` int(11) NOT NULL,
  `poll_id_child` int(11) NOT NULL,
  PRIMARY KEY (`poll_id_parent`,`poll_id_child`),
  UNIQUE KEY `index_poll_id_child` (`poll_id_child`),
  KEY `index_poll_id_parent` (`poll_id_parent`),
  CONSTRAINT `fk_poll_block_relation_poll_block_child` FOREIGN KEY (`poll_id_child`) REFERENCES `poll_block` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_poll_block_relation_poll_block_parent` FOREIGN KEY (`poll_id_parent`) REFERENCES `poll_block` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `poll_evaluation_criterion` */

DROP TABLE IF EXISTS `poll_evaluation_criterion`;

CREATE TABLE `poll_evaluation_criterion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `poll_id` int(11) NOT NULL,
  `point_from` int(11) NOT NULL,
  `point_to` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx` (`code`,`poll_id`),
  KEY `fk_poll_evaluation_criterion_created_by` (`created_by`),
  KEY `fk_poll_evaluation_criterion_updated_by` (`updated_by`),
  KEY `fk_poll_evaluation_criterion_poll_id` (`poll_id`),
  CONSTRAINT `fk_poll_evaluation_criterion_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_poll_evaluation_criterion_poll_id` FOREIGN KEY (`poll_id`) REFERENCES `poll` (`id`),
  CONSTRAINT `fk_poll_evaluation_criterion_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `poll_question` */

DROP TABLE IF EXISTS `poll_question`;

CREATE TABLE `poll_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `block_id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `point` decimal(19,4) DEFAULT NULL,
  `json_data` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_block_id` (`block_id`),
  CONSTRAINT `fk_poll_question_poll_block` FOREIGN KEY (`block_id`) REFERENCES `poll_block` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `poll_role` */

DROP TABLE IF EXISTS `poll_role`;

CREATE TABLE `poll_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poll_id` int(11) NOT NULL,
  `type` smallint(6) DEFAULT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_available_for_dealer` tinyint(1) NOT NULL DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_poll_role_created_by` (`created_by`),
  KEY `fk_poll_role_poll_id` (`poll_id`),
  CONSTRAINT `fk_poll_role_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_poll_role_poll_id` FOREIGN KEY (`poll_id`) REFERENCES `poll` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `poll_settings` */

DROP TABLE IF EXISTS `poll_settings`;

CREATE TABLE `poll_settings` (
  `poll_id` int(11) NOT NULL,
  `type` smallint(6) DEFAULT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  KEY `fk_poll_settings_created_by` (`created_by`),
  KEY `fk_poll_settings_poll_id` (`poll_id`),
  CONSTRAINT `fk_poll_settings_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_poll_settings_poll_id` FOREIGN KEY (`poll_id`) REFERENCES `poll` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `poll_tag` */

DROP TABLE IF EXISTS `poll_tag`;

CREATE TABLE `poll_tag` (
  `poll_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  UNIQUE KEY `uk_task_tag_poll_id_tag_id` (`poll_id`,`tag_id`),
  KEY `fk_poll_tag_user` (`user_id`),
  CONSTRAINT `fk_poll_tag_poll` FOREIGN KEY (`poll_id`) REFERENCES `poll` (`id`),
  CONSTRAINT `fk_poll_tag_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `position` */

DROP TABLE IF EXISTS `position`;

CREATE TABLE `position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `is_dealer` tinyint(1) NOT NULL,
  `code` enum('BDM','SM','SRV','MANAGER','ROP','ACCEPTOR','BDM_MARKETING','MARKETING') COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_position_created_by` (`created_by`),
  KEY `fk_position_updated_by` (`updated_by`),
  CONSTRAINT `fk_position_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_position_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `purchase_mechanism` */

DROP TABLE IF EXISTS `purchase_mechanism`;

CREATE TABLE `purchase_mechanism` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `queue` */

DROP TABLE IF EXISTS `queue`;

CREATE TABLE `queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `job` blob NOT NULL,
  `pushed_at` int(11) NOT NULL,
  `ttr` int(11) NOT NULL,
  `delay` int(11) NOT NULL DEFAULT 0,
  `priority` int(11) unsigned NOT NULL DEFAULT 1024,
  `reserved_at` int(11) DEFAULT NULL,
  `attempt` int(11) DEFAULT NULL,
  `done_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `channel` (`channel`),
  KEY `reserved_at` (`reserved_at`),
  KEY `priority` (`priority`)
) ENGINE=InnoDB AUTO_INCREMENT=485 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `reason_of_relation` */

DROP TABLE IF EXISTS `reason_of_relation`;

CREATE TABLE `reason_of_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `ref_model_class_segment` */

DROP TABLE IF EXISTS `ref_model_class_segment`;

CREATE TABLE `ref_model_class_segment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) DEFAULT NULL,
  `model_class_id_type` int(11) DEFAULT NULL,
  `model_class_id` int(11) DEFAULT NULL,
  `model_segment_id_type` int(11) DEFAULT NULL,
  `model_segment_id` int(11) DEFAULT NULL,
  `serial_model_id` int(11) DEFAULT NULL,
  `name` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `brand_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ref_model_class_segment_model_id` (`model_id`),
  KEY `idx_ref_model_class_segment_model_class_id_type` (`model_class_id_type`),
  KEY `idx_ref_model_class_segment_model_segment_id_type` (`model_segment_id_type`),
  KEY `fk_ref_model_class_segment_model_class_id` (`model_class_id`),
  KEY `fk_ref_model_class_segment_model_segment_id` (`model_segment_id`),
  KEY `fk_ref_model_class_segment_serial_model_id` (`serial_model_id`),
  CONSTRAINT `fk_ref_model_class_segment_model_class_id` FOREIGN KEY (`model_class_id`) REFERENCES `model_class` (`id`),
  CONSTRAINT `fk_ref_model_class_segment_model_segment_id` FOREIGN KEY (`model_segment_id`) REFERENCES `model_segment` (`id`),
  CONSTRAINT `fk_ref_model_class_segment_serial_model_id` FOREIGN KEY (`serial_model_id`) REFERENCES `serial_model` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5979 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `region_export` */

DROP TABLE IF EXISTS `region_export`;

CREATE TABLE `region_export` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_region_export_created_by` (`created_by`),
  KEY `fk_region_export_updated_by` (`updated_by`),
  CONSTRAINT `fk_region_export_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_region_export_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `region_mapping` */

DROP TABLE IF EXISTS `region_mapping`;

CREATE TABLE `region_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `region_id` int(11) NOT NULL,
  `name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `aeb_region_upload_history_id` int(11) DEFAULT NULL,
  `gibdd_upload_history_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `region_mapping_upload_idx` (`aeb_region_upload_history_id`),
  CONSTRAINT `region_mapping_upload_idx` FOREIGN KEY (`aeb_region_upload_history_id`) REFERENCES `aeb_region_upload_history` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `registration_places` */

DROP TABLE IF EXISTS `registration_places`;

CREATE TABLE `registration_places` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `federal_district_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `region_area` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `area_center` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region_center` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `area_in_region_center` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` decimal(10,8) DEFAULT NULL,
  `lng` decimal(11,8) DEFAULT NULL,
  `population` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locality` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ocato` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `octmo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `postal_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `registration_places_region_idx` (`region_id`),
  KEY `idx_federal_district_id` (`federal_district_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1482 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `relation_dealers_contracts` */

DROP TABLE IF EXISTS `relation_dealers_contracts`;

CREATE TABLE `relation_dealers_contracts` (
  `dealer_id` int(11) NOT NULL,
  `contract_id` int(11) NOT NULL,
  PRIMARY KEY (`dealer_id`,`contract_id`),
  KEY `fk_relation_dealers_contracts_contract_id_contract_id` (`contract_id`),
  CONSTRAINT `fk_relation_dealers_contracts_dealer_id_dealer_id` FOREIGN KEY (`dealer_id`) REFERENCES `dealer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `serial_model` */

DROP TABLE IF EXISTS `serial_model`;

CREATE TABLE `serial_model` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ref_brand_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `all_models_included` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_serial_model_created_by` (`created_by`),
  KEY `fk_serial_model_updated_by` (`updated_by`),
  CONSTRAINT `fk_serial_model_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_serial_model_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `session` */

DROP TABLE IF EXISTS `session`;

CREATE TABLE `session` (
  `id` char(40) NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` blob DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `sold_vehicle` */

DROP TABLE IF EXISTS `sold_vehicle`;

CREATE TABLE `sold_vehicle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_brand_id` int(11) NOT NULL,
  `ref_model_id` int(11) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `vin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sale_at` date DEFAULT NULL,
  `dealer_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `market` tinyint(3) DEFAULT NULL,
  `counterpart_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inn` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kpp` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `process_at` datetime DEFAULT NULL,
  `shipped_at` date DEFAULT NULL,
  `sale_at_added` tinyint(1) DEFAULT 0,
  `car_registration_id` int(11) DEFAULT NULL,
  `contractor_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mpv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mpv_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `options_text` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `production_year` smallint(6) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `vin` (`vin`),
  KEY `fk_sold_vehicle_model` (`model_id`),
  KEY `fk_sold_vehicle_car_registration` (`car_registration_id`),
  CONSTRAINT `fk_sold_vehicle_car_registration` FOREIGN KEY (`car_registration_id`) REFERENCES `car_registration` (`id`),
  CONSTRAINT `fk_sold_vehicle_model` FOREIGN KEY (`model_id`) REFERENCES `ref_model_class_segment` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=236505 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `steering_wheel_type` */

DROP TABLE IF EXISTS `steering_wheel_type`;

CREATE TABLE `steering_wheel_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_steering_wheel_type_created_by` (`created_by`),
  KEY `fk_steering_wheel_type_updated_by` (`updated_by`),
  CONSTRAINT `fk_steering_wheel_type_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_steering_wheel_type_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `system_event` */

DROP TABLE IF EXISTS `system_event`;

CREATE TABLE `system_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_name` varchar(300) NOT NULL COMMENT 'Event Name',
  `type_id` int(11) NOT NULL COMMENT 'Type Id',
  `handler` varchar(255) DEFAULT NULL COMMENT 'Handler',
  `params` varchar(1000) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1 COMMENT 'Active',
  PRIMARY KEY (`id`),
  KEY `fk_system_event_user` (`user_id`),
  KEY `x_type_event` (`type_id`),
  KEY `x_active` (`active`),
  CONSTRAINT `fk_system_event_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `x_type_event` FOREIGN KEY (`type_id`) REFERENCES `system_event_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

/*Table structure for table `system_event_informer` */

DROP TABLE IF EXISTS `system_event_informer`;

CREATE TABLE `system_event_informer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `system_event_id` int(11) NOT NULL COMMENT 'System event',
  `user_id` int(11) DEFAULT NULL COMMENT 'User',
  `type_id` int(11) DEFAULT NULL COMMENT 'Type System Event',
  `message` varchar(1000) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Message',
  `is_read` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Is Read Message',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `x_user` (`user_id`),
  KEY `x_is_read` (`is_read`),
  KEY `fk_system_event` (`system_event_id`),
  KEY `fk_type_event` (`type_id`),
  CONSTRAINT `fk_system_event` FOREIGN KEY (`system_event_id`) REFERENCES `system_event` (`id`),
  CONSTRAINT `fk_type_event` FOREIGN KEY (`type_id`) REFERENCES `system_event_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `system_event_mail_setting` */

DROP TABLE IF EXISTS `system_event_mail_setting`;

CREATE TABLE `system_event_mail_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `system_event_id` int(11) NOT NULL COMMENT 'System Event Id',
  `type_handle` int(11) DEFAULT NULL COMMENT 'Type Handler',
  `event_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Event Name',
  `handler` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Handler event',
  `workflow` int(11) DEFAULT NULL COMMENT 'Workflow',
  `stage` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`stage`)),
  `recipients` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'Recipients' CHECK (json_valid(`recipients`)),
  `role` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'User Role' CHECK (json_valid(`role`)),
  `list_email` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'List email' CHECK (json_valid(`list_email`)),
  `subject_mail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Subject',
  `text_mail` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Email message',
  `text_for_informer` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Message for informer',
  `personal_user_by_workflow` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Personal User By Workflow',
  `is_send_notify_before_change` tinyint(1) DEFAULT 0 COMMENT 'Is send notify before change',
  `count_days_to_blocked` int(11) DEFAULT NULL COMMENT 'Count Days To Blocked',
  PRIMARY KEY (`id`),
  KEY `x_type_handle` (`type_handle`),
  KEY `x_handler` (`handler`),
  KEY `x_fk_workflow` (`workflow`),
  KEY `x_fk_sys_evt_id` (`system_event_id`),
  CONSTRAINT `x_fk_sys_evt_id` FOREIGN KEY (`system_event_id`) REFERENCES `system_event` (`id`),
  CONSTRAINT `x_fk_type_handle` FOREIGN KEY (`type_handle`) REFERENCES `system_event_type` (`id`),
  CONSTRAINT `x_fk_workflow` FOREIGN KEY (`workflow`) REFERENCES `workflow` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `system_event_type` */

DROP TABLE IF EXISTS `system_event_type`;

CREATE TABLE `system_event_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_event` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'email' COMMENT 'Type event (email, sms)',
  `entity` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'List Model Class Name' CHECK (json_valid(`entity`)),
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Title',
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Description',
  `active` tinyint(1) DEFAULT 1 COMMENT 'Active',
  `created_at` datetime NOT NULL COMMENT 'Created At',
  `updated_at` datetime NOT NULL COMMENT 'Updated At',
  `created_by` int(11) DEFAULT NULL COMMENT 'Created By',
  `updated_by` int(11) DEFAULT NULL COMMENT 'Updated By',
  `template_format` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Template Format',
  PRIMARY KEY (`id`),
  KEY `x_active` (`active`),
  KEY `x_type_evt` (`type_event`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `system_evt_model_placeholder` */

DROP TABLE IF EXISTS `system_evt_model_placeholder`;

CREATE TABLE `system_evt_model_placeholder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entity` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Model Class Name',
  `placeholder` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Placeholder',
  `relation_value` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Relation Model Value',
  `description` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Description',
  `created_at` datetime NOT NULL COMMENT 'Created At',
  `updated_at` datetime DEFAULT NULL COMMENT 'Updated At',
  `created_by` int(11) DEFAULT NULL COMMENT 'Created By',
  `updated_by` int(11) DEFAULT NULL COMMENT 'Updated By',
  PRIMARY KEY (`id`),
  KEY `xentity` (`entity`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `system_evt_type_settg_form` */

DROP TABLE IF EXISTS `system_evt_type_settg_form`;

CREATE TABLE `system_evt_type_settg_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_id` int(11) NOT NULL COMMENT 'System Event Type',
  `field_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Title',
  `widget` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Widget Class Name',
  `data_field` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'Data For Field' CHECK (json_valid(`data_field`)),
  `type_init_data` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'default' COMMENT 'Type Initialize Data Field: callable, data',
  `is_required` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Is Required',
  `is_multiple` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Is Multiple',
  `default_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Default value',
  `created_at` datetime NOT NULL COMMENT 'Created At',
  `updated_at` datetime DEFAULT NULL COMMENT 'Updated At',
  `created_by` int(11) DEFAULT NULL COMMENT 'Created By',
  `updated_by` int(11) DEFAULT NULL COMMENT 'Updated By',
  PRIMARY KEY (`id`),
  KEY `x_setting` (`setting_id`),
  CONSTRAINT `x_setting` FOREIGN KEY (`setting_id`) REFERENCES `system_evt_type_setting` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `system_evt_type_setting` */

DROP TABLE IF EXISTS `system_evt_type_setting`;

CREATE TABLE `system_evt_type_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL COMMENT 'Type Id',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Title',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Slug',
  `description` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Description',
  `system_description` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'System Description',
  `is_flag` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Is flag',
  `created_at` datetime NOT NULL COMMENT 'Created At',
  `updated_at` datetime NOT NULL COMMENT 'Updated At',
  `created_by` int(11) DEFAULT NULL COMMENT 'Created By',
  `updated_by` int(11) DEFAULT NULL COMMENT 'Updated By',
  `sort` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Sort Field',
  PRIMARY KEY (`id`),
  KEY `x_is_flag` (`is_flag`),
  KEY `x_type` (`type_id`),
  KEY `x_sort` (`sort`),
  CONSTRAINT `x_type` FOREIGN KEY (`type_id`) REFERENCES `system_event_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `tag` */

DROP TABLE IF EXISTS `tag`;

CREATE TABLE `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_group_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tag_group_created_by` (`created_by`),
  KEY `fk_tag_group_updated_by` (`updated_by`),
  CONSTRAINT `fk_tag_group_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_tag_group_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `task` */

DROP TABLE IF EXISTS `task`;

CREATE TABLE `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dealer_id` int(11) DEFAULT NULL,
  `author_id` int(11) NOT NULL,
  `controller_id` int(11) DEFAULT NULL,
  `responsible_person_id` int(11) DEFAULT NULL,
  `dealer_poll_id` int(11) DEFAULT NULL,
  `action_plan_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `stage` int(11) NOT NULL DEFAULT 0,
  `stage_system` tinyint(1) NOT NULL DEFAULT 1,
  `is_blocked` tinyint(1) NOT NULL,
  `entity_id` int(11) DEFAULT NULL,
  `sub_entity_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `name` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `descr` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `deadline` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `stage_appointment_at` datetime DEFAULT NULL,
  `closed_at` datetime DEFAULT NULL,
  `workflow_id` int(11) NOT NULL,
  `approver_id` int(11) DEFAULT NULL,
  `inspector_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_task_user_author` (`author_id`),
  KEY `fk_task_user_responsible_person` (`responsible_person_id`),
  KEY `fk_task_workflow` (`workflow_id`),
  KEY `fk_task_user_approver` (`approver_id`),
  KEY `fk_task_inspector_id` (`inspector_id`),
  KEY `xstage` (`stage`),
  CONSTRAINT `fk_task_inspector_id` FOREIGN KEY (`inspector_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_task_user_approver` FOREIGN KEY (`approver_id`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_task_user_author` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_task_user_responsible_person` FOREIGN KEY (`responsible_person_id`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_task_workflow` FOREIGN KEY (`workflow_id`) REFERENCES `workflow` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=817 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `task_attachment` */

DROP TABLE IF EXISTS `task_attachment`;

CREATE TABLE `task_attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `upload_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `log_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_task_attachment_task` (`task_id`),
  KEY `fk_task_attachment_upload` (`upload_id`),
  KEY `fk_task_attachment_user` (`user_id`),
  KEY `fk_task_attachment_log` (`log_id`),
  CONSTRAINT `fk_task_attachment_log` FOREIGN KEY (`log_id`) REFERENCES `task_log` (`id`),
  CONSTRAINT `fk_task_attachment_task` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`),
  CONSTRAINT `fk_task_attachment_upload` FOREIGN KEY (`upload_id`) REFERENCES `upload` (`id`),
  CONSTRAINT `fk_task_attachment_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `task_feature` */

DROP TABLE IF EXISTS `task_feature`;

CREATE TABLE `task_feature` (
  `task_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`task_id`,`user_id`),
  KEY `fk_task_feature_user_id` (`user_id`),
  CONSTRAINT `fk_task_feature_task_id` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`),
  CONSTRAINT `fk_task_feature_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `task_log` */

DROP TABLE IF EXISTS `task_log`;

CREATE TABLE `task_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `old_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `action_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_task_log_user` (`user_id`),
  KEY `fk_task_log_task` (`task_id`),
  KEY `xactionUserId` (`action_user_id`),
  CONSTRAINT `fk_task_log_task` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`),
  CONSTRAINT `fk_task_log_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1625 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `task_observer` */

DROP TABLE IF EXISTS `task_observer`;

CREATE TABLE `task_observer` (
  `task_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`task_id`,`user_id`),
  KEY `fk_task_observer_user` (`user_id`),
  CONSTRAINT `fk_task_observer_task` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`),
  CONSTRAINT `fk_task_observer_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `task_stage` */

DROP TABLE IF EXISTS `task_stage`;

CREATE TABLE `task_stage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `workflow_id` int(11) NOT NULL,
  `duration_interval_begins` int(11) NOT NULL DEFAULT 0,
  `duration` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_task_stage_user` (`user_id`),
  KEY `fk_task_stage_workflow` (`workflow_id`),
  CONSTRAINT `fk_task_stage_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_task_stage_workflow` FOREIGN KEY (`workflow_id`) REFERENCES `workflow` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `task_stage_rule` */

DROP TABLE IF EXISTS `task_stage_rule`;

CREATE TABLE `task_stage_rule` (
  `workflow_id` int(11) NOT NULL,
  `stage_id` int(11) NOT NULL,
  `stage_system` tinyint(1) NOT NULL DEFAULT 0,
  `author_can` tinyint(1) DEFAULT NULL,
  `responsible_person_can` tinyint(1) DEFAULT NULL,
  `controller_can` tinyint(1) DEFAULT NULL,
  `observer_can` tinyint(1) DEFAULT NULL,
  `required_report_types` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `validation_rules` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `approver_can` tinyint(1) DEFAULT NULL,
  `answerable_can` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`workflow_id`,`stage_id`,`stage_system`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `task_stage_rule_availability` */

DROP TABLE IF EXISTS `task_stage_rule_availability`;

CREATE TABLE `task_stage_rule_availability` (
  `workflow_id` int(11) NOT NULL,
  `stage_id` int(11) NOT NULL,
  `stage_system` tinyint(1) NOT NULL DEFAULT 0,
  `available_stage_id` int(11) NOT NULL,
  `available_stage_system` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`workflow_id`,`stage_id`,`stage_system`,`available_stage_id`,`available_stage_system`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `task_tag` */

DROP TABLE IF EXISTS `task_tag`;

CREATE TABLE `task_tag` (
  `task_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  UNIQUE KEY `uk_task_tag_task_id_tag_id` (`task_id`,`tag_id`),
  KEY `fk_task_tag_user` (`user_id`),
  CONSTRAINT `fk_task_tag_task` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`),
  CONSTRAINT `fk_task_tag_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `temporary_aeb_region_data` */

DROP TABLE IF EXISTS `temporary_aeb_region_data`;

CREATE TABLE `temporary_aeb_region_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) NOT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `segment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `federal_district` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `year` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `month1` int(11) DEFAULT NULL,
  `month2` int(11) DEFAULT NULL,
  `month3` int(11) DEFAULT NULL,
  `month4` int(11) DEFAULT NULL,
  `month5` int(11) DEFAULT NULL,
  `month6` int(11) DEFAULT NULL,
  `month7` int(11) DEFAULT NULL,
  `month8` int(11) DEFAULT NULL,
  `month9` int(11) DEFAULT NULL,
  `month10` int(11) DEFAULT NULL,
  `month11` int(11) DEFAULT NULL,
  `month12` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `temporary_competitors_data` */

DROP TABLE IF EXISTS `temporary_competitors_data`;

CREATE TABLE `temporary_competitors_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dealer_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `external_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rds_dealer_id` int(11) DEFAULT NULL,
  `vehicle_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `holding` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `holding_website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lng` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dealer_website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dealer_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sale` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spare_parts` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brand` int(11) NOT NULL,
  `distributor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_before_move` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `skipped` smallint(6) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_temporary_competitors_data_rds_dealer_id` (`rds_dealer_id`),
  CONSTRAINT `fk_temporary_competitors_data_rds_dealer_id` FOREIGN KEY (`rds_dealer_id`) REFERENCES `competitor_brand` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=157940 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `temporary_corporate_client_table` */

DROP TABLE IF EXISTS `temporary_corporate_client_table`;

CREATE TABLE `temporary_corporate_client_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rds_corporate_client_id` int(11) DEFAULT NULL,
  `holding_id` int(11) DEFAULT NULL,
  `subholding_id` int(11) DEFAULT NULL,
  `company_size_id` int(11) DEFAULT NULL,
  `relation_reason_id` int(11) DEFAULT NULL,
  `okved_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `full_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `holding_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `holding_inn` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `subholding_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subholding_inn` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `affiliated_name` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `affiliated_inn` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `ogrn` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `relation_reason` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lng` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `okato` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `okpo` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kpp` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `risk` int(11) DEFAULT NULL,
  `company_size` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `okved` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `okved_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manager` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manager_post` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(2500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `registration_date` date DEFAULT NULL,
  `company_found_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16554 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `temporary_gibdd_data` */

DROP TABLE IF EXISTS `temporary_gibdd_data`;

CREATE TABLE `temporary_gibdd_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `car_id` int(11) DEFAULT NULL,
  `vehicle_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `db_for_lvc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brand` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model_id` int(11) DEFAULT NULL,
  `model_modification` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `federal_district` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `region` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `region_id` int(11) DEFAULT NULL,
  `region_area` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `area_center` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region_center` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `area_in_region_center` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `registration_place_id` int(11) DEFAULT NULL,
  `population` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locality` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ocato` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `octmo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `year_manufactured` int(11) DEFAULT NULL,
  `date_registered` date NOT NULL,
  `year_registered` int(11) DEFAULT NULL,
  `month_registered` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `day_registered` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` int(11) DEFAULT 1,
  `condition` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `registration_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ownership_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `steering_wheel_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `engine_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `engine_power` int(11) DEFAULT NULL,
  `engine_capacity` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `curb_weight` int(11) DEFAULT NULL,
  `dry_weight` int(11) DEFAULT NULL,
  `weight_in_first_range` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight_in_second_range` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cmk` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cmk_detailing` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class_sollers` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gvw_range` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chassis_configuration` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bus_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bus_purpose` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bus_length` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `engine_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chassis_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brand_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brand_country_id` int(11) DEFAULT NULL,
  `manufactured_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manufactured_country_id` int(11) DEFAULT NULL,
  `inn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `car_modification_id` int(11) DEFAULT NULL,
  `registration_place_checked` smallint(6) NOT NULL DEFAULT 0,
  `manufacturer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `true_weight` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `okved` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `okved_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activity_name` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_temporary_gibdd_data_registration_place_id` (`registration_place_id`),
  KEY `fk_temporary_gibdd_data_user_id` (`user_id`),
  KEY `temporary_gibdd_data_brand_idx` (`brand_id`),
  KEY `temporary_gibdd_data_model` (`model`),
  KEY `temporary_gibdd_data_manufactured_country` (`manufactured_country`),
  KEY `temporary_gibdd_data_brand_country` (`brand_country`),
  KEY `temporary_gibdd_data_region` (`region`),
  KEY `temporary_gibdd_data_brand` (`brand`),
  KEY `temporary_gibdd_data_group` (`brand`,`model`,`year_registered`,`month_registered`),
  KEY `temporary_gibdd_data_update_idx` (`region_id`,`region_center`,`region_area`,`area_center`,`locality`),
  KEY `temporary_gibdd_data_modification_idx` (`model_id`,`model_modification`),
  KEY `temporary_gibdd_data_engine_type_idx` (`engine_type`),
  KEY `temporary_gibdd_data_steering_wheel_type_idx` (`steering_wheel_type`),
  KEY `temporary_gibdd_data_body_type_idx` (`body_type`),
  KEY `temporary_gibdd_data_vehicle_type_idx` (`vehicle_type`),
  KEY `temporary_gibdd_data_chassis_configuration_idx` (`chassis_configuration`),
  KEY `temporary_gibdd_data_okved_idx` (`okved`),
  CONSTRAINT `fk_temporary_gibdd_data_registration_place_id` FOREIGN KEY (`registration_place_id`) REFERENCES `registration_places` (`id`),
  CONSTRAINT `fk_temporary_gibdd_data_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=694639 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `temporary_gibdd_import_errors` */

DROP TABLE IF EXISTS `temporary_gibdd_import_errors`;

CREATE TABLE `temporary_gibdd_import_errors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rowNumber` int(11) DEFAULT NULL,
  `errorText` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_0` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_4` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_5` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_6` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_7` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_8` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_9` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_10` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_11` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_12` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_13` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_14` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_15` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_16` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_17` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_18` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_19` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_20` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_21` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_22` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_23` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_24` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_25` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_26` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_27` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_28` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_29` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_30` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_31` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_32` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_33` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_34` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_35` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_36` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_37` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_38` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_39` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_40` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_41` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_42` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_43` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_44` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_45` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_46` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_47` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_48` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_49` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_50` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_51` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `column_52` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `temporary_gibdd_import_errors_rowNumber_index` (`rowNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `temporary_park_data` */

DROP TABLE IF EXISTS `temporary_park_data`;

CREATE TABLE `temporary_park_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `car_id` int(11) DEFAULT NULL,
  `vehicle_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vehicle_type_id` int(11) DEFAULT NULL,
  `db_for_lvc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `db_for_lvc_id` int(11) DEFAULT NULL,
  `brand` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model_id` int(11) DEFAULT NULL,
  `model_modification` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `federal_district` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `region` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `region_id` int(11) DEFAULT NULL,
  `region_area` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `area_center` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region_center` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `area_in_region_center` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `registration_place_id` int(11) DEFAULT NULL,
  `population` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locality` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ocato` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `octmo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `year_manufactured` int(11) DEFAULT NULL,
  `date_registered` datetime NOT NULL,
  `year_registered` int(11) DEFAULT NULL,
  `month_registered` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `day_registered` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` int(11) DEFAULT 1,
  `registration_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ownership_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body_type_id` int(11) DEFAULT NULL,
  `steering_wheel_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `steering_wheel_type_id` int(11) DEFAULT NULL,
  `engine_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `engine_type_id` int(11) DEFAULT NULL,
  `engine_power` int(11) DEFAULT NULL,
  `engine_capacity` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `curb_weight` int(11) DEFAULT NULL,
  `dry_weight` int(11) DEFAULT NULL,
  `weight_in_first_range` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight_in_second_range` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cmk` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cmk_detailing` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cmk_detailing_id` int(11) DEFAULT NULL,
  `class_sollers` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gvw_range` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chassis_configuration` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chassis_configuration_id` int(11) DEFAULT NULL,
  `bus_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bus_purpose` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bus_purpose_id` int(11) DEFAULT NULL,
  `bus_length` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `engine_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chassis_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brand_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brand_country_id` int(11) DEFAULT NULL,
  `manufactured_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manufactured_country_id` int(11) DEFAULT NULL,
  `inn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `user_id` int(11) NOT NULL,
  `car_modification_id` int(11) DEFAULT NULL,
  `registration_place_checked` smallint(6) NOT NULL DEFAULT 0,
  `manufacturer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `true_weight` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gvw_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gvw_segment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_segment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chassis_manufactured_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_created_at` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_ocved_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ocved_code_transcript` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ocved_group` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ogrn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ocpo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `handled` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_temporary_park_data_registration_place_id` (`registration_place_id`),
  KEY `fk_temporary_park_data_db_for_lvc_id` (`db_for_lvc_id`),
  KEY `fk_temporary_park_data_cmk_detailing_id` (`cmk_detailing_id`),
  KEY `fk_temporary_park_data_chassis_configuration_id` (`chassis_configuration_id`),
  KEY `fk_temporary_park_data_bus_purpose_id` (`bus_purpose_id`),
  KEY `fk_temporary_park_data_vehicle_type_id` (`vehicle_type_id`),
  KEY `fk_temporary_park_data_body_type_id` (`body_type_id`),
  KEY `fk_temporary_park_data_steering_wheel_type_id` (`steering_wheel_type_id`),
  KEY `fk_temporary_park_data_engine_type_id` (`engine_type_id`),
  KEY `fk_temporary_park_data_user_id` (`user_id`),
  KEY `temporary_park_data_brand_idx` (`brand_id`),
  KEY `temporary_park_data_model_idx` (`model_id`),
  KEY `temporary_park_data_model` (`model`),
  KEY `temporary_park_data_region_idx` (`region_id`),
  KEY `temporary_park_data_manufactured_country_idx` (`manufactured_country_id`),
  KEY `temporary_park_data_brand_country_idx` (`brand_country_id`),
  KEY `temporary_park_data_chassis_manufactured_country` (`chassis_manufactured_country`),
  KEY `temporary_park_data_region_center` (`region_center`),
  KEY `temporary_park_data_region_area` (`region_area`),
  KEY `temporary_park_data_area_center` (`area_center`),
  KEY `temporary_park_data_locality` (`locality`),
  KEY `temporary_park_data_registration_place_id` (`registration_place_id`),
  KEY `temporary_park_data_vin` (`vin`),
  KEY `temporary_park_data_body_number` (`body_number`),
  KEY `temporary_park_data_engine_number` (`engine_number`),
  CONSTRAINT `fk_temporary_park_data_body_type_id` FOREIGN KEY (`body_type_id`) REFERENCES `body_type` (`id`),
  CONSTRAINT `fk_temporary_park_data_bus_purpose_id` FOREIGN KEY (`bus_purpose_id`) REFERENCES `bus_purpose` (`id`),
  CONSTRAINT `fk_temporary_park_data_chassis_configuration_id` FOREIGN KEY (`chassis_configuration_id`) REFERENCES `chassis_configuration` (`id`),
  CONSTRAINT `fk_temporary_park_data_cmk_detailing_id` FOREIGN KEY (`cmk_detailing_id`) REFERENCES `cmk_detailing` (`id`),
  CONSTRAINT `fk_temporary_park_data_db_for_lvc_id` FOREIGN KEY (`db_for_lvc_id`) REFERENCES `db_for_lvc` (`id`),
  CONSTRAINT `fk_temporary_park_data_engine_type_id` FOREIGN KEY (`engine_type_id`) REFERENCES `engine_type` (`id`),
  CONSTRAINT `fk_temporary_park_data_steering_wheel_type_id` FOREIGN KEY (`steering_wheel_type_id`) REFERENCES `steering_wheel_type` (`id`),
  CONSTRAINT `fk_temporary_park_data_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_temporary_park_data_vehicle_type_id` FOREIGN KEY (`vehicle_type_id`) REFERENCES `vehicle_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50064243 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `transport_work_types` */

DROP TABLE IF EXISTS `transport_work_types`;

CREATE TABLE `transport_work_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT 1,
  `created_at` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `upload` */

DROP TABLE IF EXISTS `upload`;

CREATE TABLE `upload` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` smallint(6) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_upload_user` (`user_id`),
  CONSTRAINT `fk_upload_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2156 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `upload_meta` */

DROP TABLE IF EXISTS `upload_meta`;

CREATE TABLE `upload_meta` (
  `upload_id` int(11) NOT NULL,
  `type` int(3) NOT NULL,
  `mime` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `extension` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_duration` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_preview` int(11) DEFAULT NULL,
  UNIQUE KEY `upload_id` (`upload_id`),
  KEY `fk_upload_meta_video_preview_upload_id` (`video_preview`),
  CONSTRAINT `fk_upload_meta_upload_id_upload_id` FOREIGN KEY (`upload_id`) REFERENCES `upload` (`id`),
  CONSTRAINT `fk_upload_meta_video_preview_upload_id` FOREIGN KEY (`video_preview`) REFERENCES `upload` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `common_user_id` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `access_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dealer_id` int(11) DEFAULT NULL,
  `holding_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `boss_id` int(11) DEFAULT NULL,
  `work_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `employment_at` date DEFAULT NULL,
  `birthday_at` date DEFAULT NULL,
  `status` smallint(6) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `position_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `common_user_id` (`common_user_id`),
  KEY `fk_user_position_id` (`position_id`),
  CONSTRAINT `fk_user_position_id` FOREIGN KEY (`position_id`) REFERENCES `position` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `user_change_log` */

DROP TABLE IF EXISTS `user_change_log`;

CREATE TABLE `user_change_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data_change` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `key` int(11) NOT NULL,
  `initiator` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `xuser` (`user_id`),
  KEY `xaction` (`action`),
  KEY `xcreatedAt` (`created_at`),
  KEY `xkey` (`key`),
  KEY `xinitiator` (`initiator`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `user_distributor` */

DROP TABLE IF EXISTS `user_distributor`;

CREATE TABLE `user_distributor` (
  `user_id` int(11) NOT NULL,
  `distributor_id` int(11) NOT NULL,
  UNIQUE KEY `idx` (`user_id`,`distributor_id`),
  KEY `fk_user_distributor_distributor` (`distributor_id`),
  CONSTRAINT `fk_user_distributor_distributor` FOREIGN KEY (`distributor_id`) REFERENCES `distributor` (`id`),
  CONSTRAINT `fk_user_distributor_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `vehicle_type` */

DROP TABLE IF EXISTS `vehicle_type`;

CREATE TABLE `vehicle_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_vehicle_type_created_by` (`created_by`),
  KEY `fk_vehicle_type_updated_by` (`updated_by`),
  CONSTRAINT `fk_vehicle_type_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_vehicle_type_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `workflow` */

DROP TABLE IF EXISTS `workflow`;

CREATE TABLE `workflow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) DEFAULT NULL,
  `first_stage_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_stage_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
