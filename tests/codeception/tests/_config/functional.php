<?php
return yii\helpers\ArrayHelper::merge(
    require(__DIR__.'/../../config/common.php'),
    require(__DIR__.'/../../config/web.php'),
    require(__DIR__.'/../../config/tests/_local_test.php'),
    [
        'components' => [
            'request' => [ // for publication assets with run tests
                'scriptFile' => dirname(dirname(__DIR__)).'/web/index-test.php',
                // disabled checker csrf token with run tests
                'enableCsrfValidation' => false,
            ],
        ],
    ]
);
