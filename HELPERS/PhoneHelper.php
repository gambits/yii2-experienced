<?php

namespace app\helpers;

abstract class PhoneHelper
{
    public static function normalize($phone)
    {
        $phone = static::correctPhoneNumberPrefix($phone);
        $phone = static::correctPhoneNumberLength($phone);

        return $phone;
    }

    protected static function correctPhoneNumberPrefix($phone)
    {
        $phone = preg_replace('/\D/', '', $phone);
        if (!$phone) {
            return null;
        } elseif (strlen($phone) == 10 && $phone[0] != 7) {
            return '+7' . $phone;
        } elseif (strlen($phone) >= 11 && $phone[0] == '8') {
            return '+7' . substr($phone, 1);
        } elseif (strlen($phone) >= 12 && substr($phone, 0, 2) == '78') {
            return '+7' . substr($phone, 2);
        } else {
            return '+' . $phone;
        }
    }

    protected static function correctPhoneNumberLength($phone)
    {
        if (substr($phone, 0, 2) == '+7') {
            if (strlen($phone) >= 12) {
                return substr($phone, 0, 12);
            }
        } elseif (substr($phone, 0, 4) == '+375') {
            if (strlen($phone) >= 13) {
                return substr($phone, 0, 13);
            }
        }

        return null;
    }
}
