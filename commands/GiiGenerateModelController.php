<?php


namespace app\commands;


use yii\console\Controller;

/**
 * Generate all models
 * Class GiiGenerateModelController
 */
class GiiGenerateModelController extends Controller
{
    public function actionIndex()
    {
        $cmd = 'php /var/www/yii2-gcrm-dev/yii gii/model --tableName={tableName} --modelClass={modelClass} --ns="app\models" --generateQuery=1 --queryClass={queryClass} --queryNs="app\models\query"';
        foreach (\Yii::$app->db->getSchema()->getTableNames() as $tableName) {
            echo '$tableName: ' . $tableName . PHP_EOL;
            $className = $this->getClassNameWithTableName($tableName);
            echo '$className: ' . $className . PHP_EOL;
            exec(
                strtr(
                    $cmd,
                    [
                        '{modelClass}' => $className,
                        '{queryClass}' => $className . 'Query',
                        '{tableName}' => $tableName,
                    ]
                ),
                $outs
            );

            var_dump($outs, '$outs');
        }
    }

    private function getClassNameWithTableName($tableName)
    {
        $dataTable = explode('_', $tableName);
        $className = '';
        foreach ($dataTable as $item) {
            $className .= ucfirst($item);
        }

        return $className;
    }
}