<?php

namespace app\actions;

use Yii;
use yii\base\Action;
use yii\di\Instance;

class CreateAction extends Action
{
    public $modelClass;
    public $scenario;
    /** @var array */
    public $dependencies = [];
    public $view = '_form';

    /**
     * @return \yii\web\Response
     * @throws \Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        /** @var \yii\db\ActiveRecord $model */
        $model = Instance::ensure($this->modelClass);

        if (is_callable($this->scenario)) {
            $scenario = call_user_func($this->scenario);
        } else {
            $scenario = $this->scenario;
        }
        if (!empty($scenario)) {
            $model->setScenario($scenario);
        }

        if ($this->dependencies) {
            $intersect = array_intersect_key(Yii::$app->getRequest()->get(), array_flip($this->dependencies));
            if (count($intersect) !== count($this->dependencies)) {
                throw new \Exception('Указаны не все обязательные параметры');
            }

            $model->setAttributes($intersect);
        }

        if ($model->load(Yii::$app->getRequest()->post()) && $model->save()) {
            return $this->controller->asJson(['model' => $model]);
        }

        return $this->controller->asJson(
            ['form' => $this->controller->renderPartial($this->view, ['model' => $model])]
        );
    }
}
