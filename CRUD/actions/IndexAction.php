<?php

namespace app\actions;

use Yii;
use yii\base\Action;
use yii\widgets\ActiveForm;

class IndexAction extends Action
{
    public $modelClass;
    public $scenario;
    public $view = 'index';
    public $beforeSearch;

    public function run()
    {
        /** @var \yii\db\ActiveRecord $searchModel */
        $searchModel = new $this->modelClass();
        if (is_callable($this->scenario)) {
            $scenario = call_user_func($this->scenario);
        } else {
            $scenario = $this->scenario;
        }
        if (!empty($scenario)) {
            $searchModel->setScenario($scenario);
        }

        if (Yii::$app->request->isAjax && !Yii::$app->request->isPjax && $searchModel->load(Yii::$app->request->queryParams)) {
            return $this->controller->asJson(ActiveForm::validate($searchModel));
        }

        if (is_callable($this->beforeSearch)) {
            call_user_func($this->beforeSearch, $searchModel);
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->controller->render($this->view, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
