<?php

namespace app\actions;

use yii\base\Action;
use yii\di\Instance;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class DeleteAction extends Action
{
    public $modelClass;

    /**
     * @param array $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function run(array $id)
    {
        /** @var \yii\db\ActiveRecord $model */
        $modelClass = Instance::ensure($this->modelClass);
        $model = $modelClass::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException();
        }

        return $this->controller->asJson(['success' => (int)$model->delete(), 'errors' => Html::errorSummary($model)]);
    }
}
