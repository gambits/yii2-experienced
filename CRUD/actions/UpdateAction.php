<?php

namespace app\actions;

use Yii;
use yii\base\Action;
use yii\di\Instance;
use yii\web\NotFoundHttpException;

class UpdateAction extends Action
{
    public $modelClass;
    public $view = '_form';
    public $scenario;

    public function run(array $id)
    {
        /** @var \yii\db\ActiveRecord $model */
        $modelClass = Instance::ensure($this->modelClass);
        $model = $modelClass::findOne($id);
        if (is_callable($this->scenario)) {
            $scenario = call_user_func($this->scenario, $model);
        } else {
            $scenario = $this->scenario;
        }
        if (!$model) {
            throw new NotFoundHttpException();
        }
        if ($scenario) {
            $model->setScenario($scenario);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->controller->asJson(['model' => $model]);
        }

        return $this->controller->asJson(
            ['form' => $this->controller->renderPartial($this->view, ['model' => $model])]
        );
    }
}
